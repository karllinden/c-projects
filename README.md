# C Projects

A collection of miscellaneous basic projects written in C.
The projects are organized as subprojects to simplify development and building.
The subprojects are organized under the `subprojects/` directory.

## Installation

The subprojects can be built and install with the following commands from the
root of this repository:

```bash
meson build
ninja -C build
sudo ninja -C build install
```
