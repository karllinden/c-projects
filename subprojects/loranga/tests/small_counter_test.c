/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Tests that use an 8-bit counter (which is not pseudo random at all), but
 * allows testing that Loranga calls the generate function multiple times if the
 * max to lra_generate_uint is larger than what the implementation can produce.
 */

#include <limits.h>

#include <check-simpler.h>
#include <dima/system.h>

#include <loranga/advanced.h>

struct small_counter {
    uint8_t next;
};

static int small_counter_init(void *state);
static void small_counter_seed(void *state, const unsigned char *seed);
static uint32_t small_counter_generate(void *state);

static const struct lra_implementation small_counter_impl = {
        "counter",
        sizeof(struct small_counter),
        sizeof(uint8_t),
        UINT8_MAX,
        LRA_REENTRANT,
        small_counter_init,
        lra_no_deinit,
        small_counter_seed,
        small_counter_generate,
};

static lra_generator_t *generator;

static int small_counter_init(void *state) {
    struct small_counter *counter = state;
    counter->next = 0;
    return 0;
}

static void small_counter_seed(void *state, const unsigned char *seed) {
    struct small_counter *counter = state;
    memcpy(&counter->next, seed, sizeof(counter->next));
}

static uint32_t small_counter_generate(void *state) {
    struct small_counter *counter = state;
    return counter->next++;
}

void set_up(void) {
    lra_config_t *config = lra_new_config(dima_system_instance());
    ck_assert_ptr_ne(NULL, config);
    lra_set_implementation(config, &small_counter_impl);

    struct lra_error error;
    generator = lra_new_unseeded_generator(config, &error);
    ck_assert_ptr_ne(NULL, generator);

    lra_destroy_config(config);
}

void tear_down(void) {
    lra_destroy_generator(generator);
    generator = NULL;
}

static void seed_with_next(uint8_t next) {
    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);
    lra_mix_bytes(seeder, &next, sizeof(next));
    lra_seed(seeder);
}

static void assert_next(unsigned max, unsigned expected_next) {
    ck_assert_uint_eq(expected_next, lra_generate_uint(generator, max));
}

START_TEST(generate_with_initial_seed) {
    assert_next(UINT16_MAX, 1 << 8 | 0);
    assert_next(UINT16_MAX, 3 << 8 | 2);
    assert_next(UINT16_MAX, 5 << 8 | 4);
    assert_next(UINT16_MAX, 7 << 8 | 6);
    assert_next(UINT16_MAX, 9 << 8 | 8);
}
END_TEST

START_TEST(generate_with_even_wrap_around) {
    seed_with_next(253);
    assert_next(UINT16_MAX, 254 << 8 | 253);
    assert_next(UINT16_MAX, 0 << 8 | 255);
    assert_next(UINT16_MAX, 2 << 8 | 1);
}
END_TEST

START_TEST(generate_three_bytes) {
    seed_with_next(252);
    assert_next(0xFFFFFF, 254 << 16 | 253 << 8 | 252);
    assert_next(0xFFFFFF, 1 << 16 | 0 << 8 | 255);
    assert_next(0xFFFFFF, 4 << 16 | 3 << 8 | 2);
}
END_TEST

START_TEST(generate_without_modulo_bias) {
    assert_next(0x3FFFF, 2 << 16 | 1 << 8 | 0);
    assert_next(0x3FFFF, 1 << 16 | 4 << 8 | 3);
    assert_next(0x3FFFF, 0 << 16 | 7 << 8 | 6);
    assert_next(0x3FFFF, 3 << 16 | 10 << 8 | 9);
}
END_TEST

START_TEST(generate_with_modulo_bias_11_1) {
    /* 256 = 3 (mod 11), so 255, 254 and 253 must be discarded */
    seed_with_next(245);
    assert_next(0xAFFFF, 5 << 16 | 246 << 8 | 245);
    assert_next(0xAFFFF, 8 << 16 | 249 << 8 | 248);
    assert_next(0xAFFFF, 0 << 16 | 252 << 8 | 251);
    assert_next(0xAFFFF, 3 << 16 | 2 << 8 | 1);
}
END_TEST

START_TEST(generate_with_modulo_bias_11_2) {
    /* 256 = 3 (mod 11), so 255, 254 and 253 must be discarded */
    seed_with_next(246);
    assert_next(0xAFFFF, 6 << 16 | 247 << 8 | 246);
    assert_next(0xAFFFF, 9 << 16 | 250 << 8 | 249);
    assert_next(0xAFFFF, 0 << 16 | 253 << 8 | 252);
    assert_next(0xAFFFF, 3 << 16 | 2 << 8 | 1);
}
END_TEST

START_TEST(generate_with_modulo_bias_11_3) {
    /* 256 = 3 (mod 11), so 255, 254 and 253 must be discarded */
    seed_with_next(247);
    assert_next(0xAFFFF, 7 << 16 | 248 << 8 | 247);
    assert_next(0xAFFFF, 10 << 16 | 251 << 8 | 250);
    assert_next(0xAFFFF, 0 << 16 | 254 << 8 | 253);
    assert_next(0xAFFFF, 3 << 16 | 2 << 8 | 1);
}
END_TEST

START_TEST(generate_avoids_overflow) {
    /* 277 = 1 * 256 + 21 */
    /* The generator must produce the uniform distribution [0,277] by generating
     * x from [0,255] and then y from [0,1] to produce y * 256 + x. However, in
     * doing so, it must avoid pairs (x,y) that cause a too large result. */
    seed_with_next(_i);
    ck_assert_int_le(lra_generate_uint(generator, 277), 277);
}
END_TEST

void add_tests(void) {
    ADD_TEST(generate_with_initial_seed);
    ADD_TEST(generate_with_even_wrap_around);
    ADD_TEST(generate_three_bytes);
    ADD_TEST(generate_without_modulo_bias);
    ADD_TEST(generate_with_modulo_bias_11_1);
    ADD_TEST(generate_with_modulo_bias_11_2);
    ADD_TEST(generate_with_modulo_bias_11_3);
    ADD_LOOP_TEST(generate_avoids_overflow, 0, 256);
}
