/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Tests that use a 32-bit counter (which is not pseudo random at all), but
 * allows testing that Loranga passes the correct state to the implementation
 * and that the generated numbers are unbiased when using a smaller bound.
 */

#include <limits.h>

#include <check-simpler.h>
#include <dima/system.h>

#include <loranga/advanced.h>

struct large_counter {
    uint32_t next;
};

static int large_counter_init(void *state);
static void large_counter_seed(void *state, const unsigned char *seed);
static uint32_t large_counter_generate(void *state);

static const struct lra_implementation large_counter_impl = {
        "counter",
        sizeof(struct large_counter),
        sizeof(uint32_t),
        UINT32_MAX,
        LRA_REENTRANT,
        large_counter_init,
        lra_no_deinit,
        large_counter_seed,
        large_counter_generate,
};

static lra_generator_t *generator;

static int large_counter_init(void *state) {
    struct large_counter *counter = state;
    counter->next = 0;
    return 0;
}

static void large_counter_seed(void *state, const unsigned char *seed) {
    struct large_counter *counter = state;
    memcpy(&counter->next, seed, sizeof(counter->next));
}

static uint32_t large_counter_generate(void *state) {
    struct large_counter *counter = state;
    return counter->next++;
}

void set_up(void) {
    lra_config_t *config = lra_new_config(dima_system_instance());
    ck_assert_ptr_ne(NULL, config);
    lra_set_implementation(config, &large_counter_impl);

    struct lra_error error;
    generator = lra_new_unseeded_generator(config, &error);
    ck_assert_ptr_ne(NULL, generator);

    lra_destroy_config(config);
}

void tear_down(void) {
    lra_destroy_generator(generator);
    generator = NULL;
}

static void seed_with_next(uint32_t next) {
    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);
    lra_mix_bytes(seeder, &next, sizeof(next));
    lra_seed(seeder);
}

static void assert_next(unsigned max, unsigned expected_next) {
    ck_assert_uint_eq(expected_next, lra_generate_uint(generator, max));
}

START_TEST(generate_with_initial_seed) {
    assert_next(UINT_MAX, 0);
    assert_next(UINT_MAX, 1);
    assert_next(UINT_MAX, 2);
    assert_next(UINT_MAX, 3);
    assert_next(UINT_MAX, 4);
    assert_next(UINT_MAX, 5);
}
END_TEST

START_TEST(generate_with_custom_seed) {
    seed_with_next(1055);
    assert_next(UINT_MAX, 1055);
    assert_next(UINT_MAX, 1056);
    assert_next(UINT_MAX, 1057);
    assert_next(UINT_MAX, 1058);
    assert_next(UINT_MAX, 1059);
    assert_next(UINT_MAX, 1060);
}
END_TEST

START_TEST(smaller_max_wraps_around) {
    seed_with_next(98);
    assert_next(100, 98);
    assert_next(100, 99);
    assert_next(100, 100);
    assert_next(100, 0);
    assert_next(100, 1);
    assert_next(100, 2);
}
END_TEST

START_TEST(modulo_bias_2) {
    /* 2^32 = 0 (mod 2), so no number needs to be discarded */
    seed_with_next(UINT32_MAX - 1);
    assert_next(1, 0); /* (2^32 - 1) - 1 = 0 (mod 2) */
    assert_next(1, 1); /*  2^32 - 1      = 1 (mod 2) */
    assert_next(1, 0); /* 0              = 0 (mod 2) */
    assert_next(1, 1); /* 1              = 1 (mod 2) */
}
END_TEST

START_TEST(modulo_bias_3) {
    /* 2^32 = 1 (mod 3), so one number must be discarded */
    seed_with_next(UINT32_MAX - 3);
    assert_next(2, 0); /* (2^32 - 1) - 3 = 0 (mod 3) */
    assert_next(2, 1); /* (2^32 - 1) - 2 = 1 (mod 3) */
    assert_next(2, 2); /* (2^32 - 1) - 1 = 2 (mod 3) */
    /*              */ /*  2^32 - 1      = 0 (mod 3) */
    assert_next(2, 0); /* 0              = 0 (mod 3) */
    assert_next(2, 1); /* 1              = 1 (mod 3) */
    assert_next(2, 2); /* 2              = 2 (mod 3) */
}
END_TEST

START_TEST(modulo_bias_6) {
    /* 2^32 = 4 (mod 6), so four numbers must be discarded */
    seed_with_next(UINT32_MAX - 9);
    assert_next(5, 0); /* (2^32 - 1) - 9 = 0 (mod 6) */
    assert_next(5, 1); /* (2^32 - 1) - 8 = 1 (mod 6) */
    assert_next(5, 2); /* (2^32 - 1) - 7 = 2 (mod 6) */
    assert_next(5, 3); /* (2^32 - 1) - 6 = 3 (mod 6) */
    assert_next(5, 4); /* (2^32 - 1) - 5 = 4 (mod 6) */
    assert_next(5, 5); /* (2^32 - 1) - 4 = 5 (mod 6) */
    /*              */ /* (2^32 - 1) - 3 = 0 (mod 6) */
    /*              */ /* (2^32 - 1) - 2 = 1 (mod 6) */
    /*              */ /* (2^32 - 1) - 1 = 2 (mod 6) */
    /*              */ /*  2^32 - 1      = 3 (mod 6) */
    assert_next(5, 0); /* 0              = 0 (mod 6) */
    assert_next(5, 1); /* 1              = 1 (mod 6) */
    assert_next(5, 2); /* 2              = 2 (mod 6) */
    assert_next(5, 3); /* 0              = 3 (mod 6) */
    assert_next(5, 4); /* 1              = 4 (mod 6) */
    assert_next(5, 5); /* 2              = 5 (mod 6) */
}
END_TEST

void add_tests(void) {
    ADD_TEST(generate_with_initial_seed);
    ADD_TEST(generate_with_custom_seed);
    ADD_TEST(smaller_max_wraps_around);
    ADD_TEST(modulo_bias_2);
    ADD_TEST(modulo_bias_3);
    ADD_TEST(modulo_bias_6);
}
