/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#include "fake_getenv.h"

const char *fake_implementation_value = NULL;
const char *fake_seed_value = NULL;

const char *fake_getenv(const char *name) {
    if (strcmp(name, "LORANGA_IMPLEMENTATION") == 0) {
        return fake_implementation_value;
    } else if (strcmp(name, "LORANGA_SEED") == 0) {
        return fake_seed_value;
    } else {
        return NULL;
    }
}
