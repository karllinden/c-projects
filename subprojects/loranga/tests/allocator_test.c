/*
 * Copyright (C) 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>

#include <dima/countdown.h>
#include <dima/exiting_on_failure.h>
#include <dima/system.h>

#include <loranga/allocator.h>
#include <loranga/simple.h>

static lra_generator_t *non_reentrant_generator;
static lra_generator_t *reentrant_generator;

static lra_allocator_t *non_reentrant_allocator;
static lra_allocator_t *reentrant_allocator;

static dima_t *dima_under_test;

START_TEST(does_not_exit_on_failure) {
    ck_assert_int_eq(0, dima_exits_on_failure(dima_under_test));
}
END_TEST

START_TEST(does_not_exit_on_failure_even_if_next_does) {
    dima_t *exiting = dima_new_exiting_on_failure(dima_system_instance(), 28);
    lra_allocator_t *r = lra_new_allocator(exiting, non_reentrant_generator);
    ck_assert_int_eq(0, dima_exits_on_failure(lra_allocator_to_dima(r)));
    lra_unref_allocator(r);
    dima_unref(exiting);
}
END_TEST

START_TEST(non_reentrant_allocator_is_not_thread_safe) {
    dima_t *d = lra_allocator_to_dima(non_reentrant_allocator);
    ck_assert_int_eq(0, dima_is_thread_safe(d));
}
END_TEST

START_TEST(reentrant_allocator_is_not_thread_safe) {
    dima_t *d = lra_allocator_to_dima(reentrant_allocator);
    ck_assert_int_eq(0, dima_is_thread_safe(d));
}
END_TEST

START_TEST(non_reentrant_allocator_is_thread_hostile) {
    dima_t *d = lra_allocator_to_dima(non_reentrant_allocator);
    ck_assert_int_ne(0, dima_is_thread_hostile(d));
}
END_TEST

START_TEST(reentrant_allocator_is_not_thread_hostile) {
    dima_t *d = lra_allocator_to_dima(reentrant_allocator);
    ck_assert_int_eq(0, dima_is_thread_hostile(d));
}
END_TEST

START_TEST(new_random_returns_null_if_allocation_fails) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_t *next = dima_from_countdown(countdown);
    lra_allocator_t *a = lra_new_allocator(next, non_reentrant_generator);
    ck_assert_ptr_eq(NULL, a);
    dima_unref_countdown(countdown);
}
END_TEST

START_TEST(allocator_owns_next_dima) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);
    lra_allocator_t *a = lra_new_allocator(dima_from_countdown(countdown),
                                           non_reentrant_generator);
    dima_unref_countdown(countdown);

    dima_t *dima = lra_allocator_to_dima(a);
    void *ptr = dima_alloc(dima, 70);
    ck_assert_ptr_ne(NULL, ptr);
    dima_free(dima, ptr);

    lra_unref_allocator(a);
}
END_TEST

START_TEST(panics_if_failure_percentage_is_invalid) {
    lra_set_allocator_failure_percentage(non_reentrant_allocator, 101);
}
END_TEST

static int count_failures(void) {
    int result = 0;
    for (int i = 0; i < 100; ++i) {
        void *ptr = dima_alloc(dima_under_test, 16);
        result += (ptr == NULL);
        dima_free(dima_under_test, ptr);
    }
    return result;
}

START_TEST(never_fails_if_failure_percentage_is_0) {
    int failure_count = count_failures();
    ck_assert_int_eq(0, failure_count);
}
END_TEST

START_TEST(always_fails_if_failure_percentage_is_100) {
    lra_set_allocator_failure_percentage(non_reentrant_allocator, 100);
    int failure_count = count_failures();
    ck_assert_int_eq(100, failure_count);
}
END_TEST

START_TEST(has_ok_failure_count_if_failure_percentage_is_50) {
    lra_set_allocator_failure_percentage(non_reentrant_allocator, 50);
    int failure_count = count_failures();

    /* The probability for failure_count <= 7 || failure_count >= 93 is
     * approximately 2.72 * 10^-20, and that probability is so vanishingly
     * small that we deem it impossible. */
    ck_assert_int_gt(failure_count, 7);
    ck_assert_int_lt(failure_count, 93);
}
END_TEST

void set_up(void) {
    struct lra_error error;
    lra_clear_error(&error);

    non_reentrant_generator = lra_new_testing_generator(0, &error);
    ck_assert_ptr_ne(NULL, non_reentrant_generator);

    reentrant_generator = lra_new_testing_generator(LRA_REENTRANT, &error);
    ck_assert_ptr_ne(NULL, reentrant_generator);

    dima_t *next = dima_system_instance();
    non_reentrant_allocator = lra_new_allocator(next, non_reentrant_generator);
    reentrant_allocator = lra_new_allocator(next, reentrant_generator);

    dima_under_test = lra_allocator_to_dima(non_reentrant_allocator);
}

void tear_down(void) {
    dima_under_test = NULL;

    lra_unref_allocator(reentrant_allocator);
    reentrant_allocator = NULL;

    lra_unref_allocator(non_reentrant_allocator);
    non_reentrant_allocator = NULL;

    lra_destroy_generator(reentrant_generator);
    reentrant_generator = NULL;

    lra_destroy_generator(non_reentrant_generator);
    non_reentrant_generator = NULL;
}

void add_tests(void) {
    ADD_TEST(does_not_exit_on_failure);
    ADD_TEST(does_not_exit_on_failure_even_if_next_does);
    ADD_TEST(non_reentrant_allocator_is_not_thread_safe);
    ADD_TEST(reentrant_allocator_is_not_thread_safe);
    ADD_TEST(non_reentrant_allocator_is_thread_hostile);
    ADD_TEST(reentrant_allocator_is_not_thread_hostile);
    ADD_TEST(new_random_returns_null_if_allocation_fails);
    ADD_TEST(allocator_owns_next_dima);
    ADD_ABORT_TEST(panics_if_failure_percentage_is_invalid);
    ADD_TEST(never_fails_if_failure_percentage_is_0);
    ADD_TEST(always_fails_if_failure_percentage_is_100);
    ADD_TEST(has_ok_failure_count_if_failure_percentage_is_50);
}
