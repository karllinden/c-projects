/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <limits.h>
#include <stdlib.h>

#include <check-simpler.h>
#include <dima/countdown.h>
#include <dima/system.h>

#include <loranga/advanced.h>

#include "fake_getenv.h"
#include "fake_impl.h"

static dima_countdown_t *countdown;
static dima_t *allocator;
static lra_config_t *config;

static struct lra_error error;

void set_up(void) {
    countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);

    allocator = dima_from_countdown(countdown);

    config = lra_new_config(allocator);
    lra_set_getenv(config, fake_getenv);

    lra_clear_error(&error);
    set_up_fake_impl();
}

void tear_down(void) {
    lra_destroy_config(config);
    config = NULL;
    allocator = NULL;
    dima_unref_countdown(countdown);
    countdown = NULL;
}

static lra_generator_t *new_generator(void) {
    return lra_new_unseeded_generator(config, &error);
}

static lra_generator_t *new_fake_generator(void) {
    lra_set_implementation(config, &fake_impl);
    return new_generator();
}

START_TEST(new_config_with_failing_allocator_fails) {
    dima_enable_countdown(countdown);
    lra_config_t *c = lra_new_config(allocator);
    ck_assert_ptr_eq(NULL, c);
}
END_TEST

START_TEST(new_config_with_allocator_failing_after_one_succeeds) {
    dima_enable_countdown(countdown);
    dima_start_countdown(countdown, 1);
    lra_config_t *c = lra_new_config(allocator);
    ck_assert_ptr_ne(NULL, c);
    lra_destroy_config(c);
}
END_TEST

START_TEST(destroy_null_config_works) {
    lra_destroy_config(NULL);
}
END_TEST

START_TEST(config_owns_allocator) {
    lra_config_t *c = lra_new_config(allocator);
    ck_assert_ptr_ne(NULL, c);

    tear_down();
    lra_destroy_config(c);
}
END_TEST

START_TEST(new_generator_with_failing_allocator_fails) {
    dima_enable_countdown(countdown);

    lra_generator_t *generator = new_generator();
    ck_assert_ptr_eq(NULL, generator);
    ck_assert_int_eq(LRA_NO_MEMORY, error.code);
    ck_assert_ptr_ne(NULL, error.implementation_name);
}
END_TEST

START_TEST(new_generator_with_ok_arguments_succeeds) {
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert_int_eq(LRA_OK, error.code);
    lra_destroy_generator(generator);
}
END_TEST

static lra_generator_t *new_reentrant_generator(void) {
    lra_set_flags(config, LRA_REENTRANT);
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert(lra_generator_is_reentrant(generator));
    return generator;
}

START_TEST(new_reentrant_generator_returns_reentrant_generator) {
    lra_generator_t *generator = new_reentrant_generator();
    /* new_reentrant_generator does the assertions */
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(new_generator_with_implementation_name_succeeds) {
    lra_set_implementation_name(config, ARBITRARY_IMPL_NAME);
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_str_eq(ARBITRARY_IMPL_NAME, lra_generator_name(generator));

    lra_destroy_generator(generator);
}
END_TEST

START_TEST(new_generator_by_unknown_name_returns_null) {
    lra_set_implementation_name(config, "foobar");
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_eq(NULL, generator);
    ck_assert_int_eq(LRA_NOT_IMPLEMENTED, error.code);
}
END_TEST

START_TEST(new_generator_with_inconsistent_flags_returns_null) {
    lra_set_flags(config, LRA_REENTRANT);
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_eq(NULL, generator);
    ck_assert_int_eq(LRA_NOT_IMPLEMENTED, error.code);
}
END_TEST

START_TEST(new_generator_with_inconsistent_name_returns_null) {
    lra_set_implementation_name(config, ARBITRARY_IMPL_NAME);
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_eq(NULL, generator);
    ck_assert_int_eq(LRA_NOT_IMPLEMENTED, error.code);
}
END_TEST

START_TEST(new_generator_with_null_error_panics) {
    lra_new_unseeded_generator(config, NULL);
}
END_TEST

START_TEST(set_invalid_flags_panics) {
    lra_set_flags(config, 0x80);
}
END_TEST

START_TEST(set_null_implementation_name_panics) {
    lra_set_implementation_name(config, NULL);
}
END_TEST

START_TEST(set_null_implementation_panics) {
    lra_set_implementation(config, NULL);
}
END_TEST

START_TEST(destroy_null_generator_works) {
    lra_destroy_generator(NULL);
}
END_TEST

START_TEST(generator_owns_allocator) {
    lra_generator_t *generator = new_generator();
    tear_down();
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(new_generator_with_failing_init_returns_null) {
    fake_init_code = 1;
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_eq(NULL, generator);
    ck_assert_int_eq(LRA_INIT_FAILED, error.code);
    ck_assert_str_eq("fake", error.implementation_name);
    ck_assert_int_eq(1, error.init_failure_code);
}
END_TEST

START_TEST(new_generator_with_failing_init_has_correct_error) {
    fake_impl.name = "other";
    fake_init_code = 2;
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_eq(NULL, generator);
    ck_assert_int_eq(LRA_INIT_FAILED, error.code);
    ck_assert_str_eq("other", error.implementation_name);
    ck_assert_int_eq(2, error.init_failure_code);
}
END_TEST

START_TEST(generator_with_implementation_has_correct_name) {
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert_str_eq("fake", lra_generator_name(generator));
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(generator_is_reentrant_if_implementation_has_flag) {
    fake_impl.flags = LRA_REENTRANT;
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert(lra_generator_is_reentrant(generator));
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(generator_is_not_reentrant_if_implementation_lacks_flag) {
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert(!lra_generator_is_reentrant(generator));
    lra_destroy_generator(generator);
}
END_TEST

static uint32_t return_eleven(ecb_unused void *state) {
    return 11;
}

START_TEST(panics_if_generate_returns_too_large) {
    fake_impl.max = 10;
    fake_impl.generate = return_eleven;
    lra_generator_t *generator = new_fake_generator();
    lra_generate_uint(generator, 10);
}
END_TEST

static void assert_generates_result(unsigned expected_result) {
    fake_result = expected_result;
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_ne(NULL, generator);
    unsigned actual_result = lra_generate_uint(generator, UINT_MAX);
    ck_assert_int_eq(expected_result, actual_result);
    lra_destroy_generator(generator);
}

START_TEST(generate_when_result_is_24) {
    assert_generates_result(24);
}
END_TEST

START_TEST(generate_when_result_is_35) {
    assert_generates_result(35);
}
END_TEST

START_TEST(generate_returns_max_or_less) {
    fake_result = 128;
    lra_generator_t *generator = new_fake_generator();

    unsigned result = lra_generate_uint(generator, 100);
    ck_assert_int_eq(27, result);

    lra_destroy_generator(generator);
}
END_TEST

START_TEST(new_generator_copies_implementation) {
    lra_generator_t *generator = new_fake_generator();
    fake_impl.generate = NULL;

    /* This segfaults if the implementation is not copied into the generator. */
    lra_generate_uint(generator, UINT_MAX);

    lra_destroy_generator(generator);
}
END_TEST

START_TEST(destroy_generator_calls_deinit_with_state) {
    ck_assert_ptr_eq(NULL, fake_init_state);
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_ne(NULL, fake_init_state);
    lra_destroy_generator(generator);
    ck_assert_ptr_eq(NULL, fake_init_state);
}
END_TEST

START_TEST(set_null_getenv_panics) {
    lra_set_getenv(config, NULL);
}
END_TEST

START_TEST(new_generator_respects_environment_variable) {
    fake_implementation_value = ARBITRARY_IMPL_NAME;
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);
    ck_assert_str_eq(ARBITRARY_IMPL_NAME, lra_generator_name(generator));
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(standard_getenv_works) {
    lra_set_getenv(config, lra_standard_getenv);
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(no_getenv_works) {
    lra_set_getenv(config, lra_no_getenv);
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(set_implementation_with_null_name_panics) {
    fake_impl.name = NULL;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_0_seed_size_panics) {
    fake_impl.seed_size = 0;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_0_max_panics) {
    fake_impl.max = 0;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_invalid_flags_in_impl_panics) {
    fake_impl.flags = 0x0400;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_null_init_panics) {
    fake_impl.init = NULL;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_null_deinit_panics) {
    fake_impl.deinit = NULL;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_null_seed_panics) {
    fake_impl.seed = NULL;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(set_implementation_with_null_generate_panics) {
    fake_impl.generate = NULL;
    lra_set_implementation(config, &fake_impl);
}
END_TEST

START_TEST(new_seeder_with_failing_allocator_returns_null) {
    lra_generator_t *generator = new_fake_generator();
    ck_assert_ptr_ne(NULL, generator);

    dima_enable_countdown(countdown);
    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_eq(NULL, seeder);

    lra_destroy_generator(generator);
}
END_TEST

START_TEST(destroy_generator_with_seeder_panics) {
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);

    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);

    lra_destroy_generator(generator);
}
END_TEST

START_TEST(create_multiple_seeders_panics) {
    lra_generator_t *generator = new_generator();
    ck_assert_ptr_ne(NULL, generator);

    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);

    lra_new_seeder(generator);
}
END_TEST

static void mix_bytes(const unsigned char *bytes, size_t size) {
    lra_generator_t *generator = new_fake_generator();

    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);

    lra_mix_bytes(seeder, bytes, size);
    lra_seed(seeder);
    lra_destroy_generator(generator);
}

static const unsigned char four_bytes[4] = {0xDE, 0xAD, 0xBE, 0xEF};

static void mix_4_bytes(void) {
    mix_bytes(four_bytes, 4);
}

START_TEST(mix_4_bytes_into_8_byte_seed) {
    fake_impl.seed_size = 8;
    mix_4_bytes();
    ck_assert_int_eq(four_bytes[0], fake_seed_bytes[0]);
    ck_assert_int_eq(four_bytes[1], fake_seed_bytes[1]);
    ck_assert_int_eq(four_bytes[2], fake_seed_bytes[2]);
    ck_assert_int_eq(four_bytes[3], fake_seed_bytes[3]);
    ck_assert_int_eq(0, fake_seed_bytes[4]);
    ck_assert_int_eq(0, fake_seed_bytes[5]);
    ck_assert_int_eq(0, fake_seed_bytes[6]);
    ck_assert_int_eq(0, fake_seed_bytes[7]);
}
END_TEST

static const unsigned char eight_bytes[8]
        = {0xCA, 0xFE, 0xBA, 0xBE, 0xFE, 0xED, 0xC0, 0xDE};

static void mix_8_bytes(void) {
    mix_bytes(eight_bytes, 8);
}

static void assert_8_bytes(void) {
    ck_assert_int_eq(eight_bytes[0], fake_seed_bytes[0]);
    ck_assert_int_eq(eight_bytes[1], fake_seed_bytes[1]);
    ck_assert_int_eq(eight_bytes[2], fake_seed_bytes[2]);
    ck_assert_int_eq(eight_bytes[3], fake_seed_bytes[3]);
    ck_assert_int_eq(eight_bytes[4], fake_seed_bytes[4]);
    ck_assert_int_eq(eight_bytes[5], fake_seed_bytes[5]);
    ck_assert_int_eq(eight_bytes[6], fake_seed_bytes[6]);
    ck_assert_int_eq(eight_bytes[7], fake_seed_bytes[7]);
}

START_TEST(mix_8_bytes_into_8_byte_seed) {
    fake_impl.seed_size = 8;
    mix_8_bytes();
    assert_8_bytes();
}
END_TEST

START_TEST(mix_8_bytes_into_4_byte_seed) {
    fake_impl.seed_size = 4;
    mix_8_bytes();
    ck_assert_int_eq(eight_bytes[0] ^ eight_bytes[4], fake_seed_bytes[0]);
    ck_assert_int_eq(eight_bytes[1] ^ eight_bytes[5], fake_seed_bytes[1]);
    ck_assert_int_eq(eight_bytes[2] ^ eight_bytes[6], fake_seed_bytes[2]);
    ck_assert_int_eq(eight_bytes[3] ^ eight_bytes[7], fake_seed_bytes[3]);
    ck_assert_int_eq(0, fake_seed_bytes[4]);
    ck_assert_int_eq(0, fake_seed_bytes[5]);
    ck_assert_int_eq(0, fake_seed_bytes[6]);
    ck_assert_int_eq(0, fake_seed_bytes[7]);
}
END_TEST

START_TEST(mix_1024_bytes_into_128_byte_seed) {
    fake_impl.seed_size = FAKE_SEED_BYTES_SIZE;
    unsigned char bytes[1024];
    memset(bytes, 0, 1024);
    mix_bytes(bytes, 1024);
}
END_TEST

START_TEST(mix_2_bytes_4_times_is_same_as_mix_8_bytes) {
    fake_impl.seed_size = 8;

    lra_generator_t *generator = new_fake_generator();

    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);

    lra_mix_bytes(seeder, eight_bytes + 0, 2);
    lra_mix_bytes(seeder, eight_bytes + 2, 2);
    lra_mix_bytes(seeder, eight_bytes + 4, 2);
    lra_mix_bytes(seeder, eight_bytes + 6, 2);
    lra_seed(seeder);
    lra_destroy_generator(generator);

    assert_8_bytes();
}
END_TEST

START_TEST(mix_non_existing_file_fails) {
    lra_generator_t *generator = new_generator();
    lra_seeder_t *seeder = lra_new_seeder(generator);

    struct lra_mix_result result;
    lra_mix_file(seeder, "/this/path/does/not/exist", 0, &result);

    ck_assert_int_eq(LRA_MIX_RESULT_ERROR, result.type);
    ck_assert_uint_eq(0, result.bytes_read);

    lra_seed(seeder);
    lra_destroy_generator(generator);
}
END_TEST

#if HAVE_FMEMOPEN
static void mix_stream(FILE *stream,
                       size_t size,
                       struct lra_mix_result *result) {
    lra_generator_t *generator = new_fake_generator();
    lra_seeder_t *seeder = lra_new_seeder(generator);

    lra_mix_stream(seeder, stream, size, result);

    fclose(stream);

    lra_seed(seeder);
    lra_destroy_generator(generator);
}

START_TEST(mix_unreadable_stream_fails) {
    char buf[8];
    FILE *stream = fmemopen(buf, 8, "w");

    struct lra_mix_result result;
    mix_stream(stream, 1, &result);

    ck_assert_int_eq(LRA_MIX_RESULT_ERROR, result.type);
    ck_assert_uint_eq(0, result.bytes_read);
}
END_TEST

static void mix_eight_byte_stream(size_t size, struct lra_mix_result *result) {
    fake_impl.seed_size = 8;
    FILE *stream = fmemopen((void *)eight_bytes, 8, "r");
    mix_stream(stream, size, result);
}

START_TEST(mix_stream_with_0_size_mixes_seed_size) {
    struct lra_mix_result result;
    mix_eight_byte_stream(0, &result);

    ck_assert_int_eq(LRA_MIX_RESULT_OK, result.type);
    ck_assert_uint_eq(8, result.bytes_read);

    assert_8_bytes();
}
END_TEST

START_TEST(mix_stream_with_fixed_size_mixes_that_size) {
    struct lra_mix_result result;
    mix_eight_byte_stream(4, &result);

    ck_assert_int_eq(LRA_MIX_RESULT_OK, result.type);
    ck_assert_uint_eq(4, result.bytes_read);

    ck_assert_int_eq(eight_bytes[0], fake_seed_bytes[0]);
    ck_assert_int_eq(eight_bytes[1], fake_seed_bytes[1]);
    ck_assert_int_eq(eight_bytes[2], fake_seed_bytes[2]);
    ck_assert_int_eq(eight_bytes[3], fake_seed_bytes[3]);
    ck_assert_int_eq(0, fake_seed_bytes[4]);
    ck_assert_int_eq(0, fake_seed_bytes[5]);
    ck_assert_int_eq(0, fake_seed_bytes[6]);
    ck_assert_int_eq(0, fake_seed_bytes[7]);
}
END_TEST

START_TEST(mix_stream_with_too_large_size_stops_at_eof) {
    struct lra_mix_result result;
    mix_eight_byte_stream(SIZE_MAX, &result);

    ck_assert_int_eq(LRA_MIX_RESULT_EOF, result.type);
    ck_assert_uint_eq(8, result.bytes_read);

    assert_8_bytes();
}
END_TEST
#endif /* HAVE_FMEMOPEN */

static void assert_fork_with_failing_allocator_fails(unsigned n_allocs) {
    lra_generator_t *g = new_reentrant_generator();

    dima_start_countdown(countdown, n_allocs);
    dima_enable_countdown(countdown);
    lra_generator_t *f = lra_fork_generator(g, &error);
    ck_assert_int_eq(LRA_NO_MEMORY, error.code);
    ck_assert_str_eq(lra_generator_name(g), error.implementation_name);
    ck_assert_ptr_eq(NULL, f);

    lra_destroy_generator(g);
}

START_TEST(fork_generator_with_failing_allocator_fails) {
    assert_fork_with_failing_allocator_fails(0);
}
END_TEST

START_TEST(fork_generator_with_allocator_failing_after_one_fails) {
    assert_fork_with_failing_allocator_fails(1);
}
END_TEST

START_TEST(fork_generator_with_allocator_failing_after_two_succeeds) {
    lra_generator_t *g = new_reentrant_generator();

    dima_start_countdown(countdown, 2);
    dima_enable_countdown(countdown);
    lra_generator_t *f = lra_fork_generator(g, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, f);

    lra_destroy_generator(f);
    lra_destroy_generator(g);
}
END_TEST

START_TEST(fork_generator_with_null_error_panics) {
    lra_generator_t *g = new_generator();
    ck_assert_ptr_ne(NULL, g);
    lra_fork_generator(g, NULL);
}
END_TEST

void add_tests(void) {
    ADD_TEST(new_config_with_failing_allocator_fails);
    ADD_TEST(new_config_with_allocator_failing_after_one_succeeds);
    ADD_TEST(destroy_null_config_works);
    ADD_TEST(config_owns_allocator);

    ADD_TEST(new_generator_with_failing_allocator_fails);
    ADD_TEST(new_generator_with_ok_arguments_succeeds);
    ADD_TEST(new_reentrant_generator_returns_reentrant_generator);
    ADD_TEST(new_generator_with_implementation_name_succeeds);
    ADD_TEST(new_generator_by_unknown_name_returns_null);
    ADD_TEST(new_generator_with_inconsistent_flags_returns_null);
    ADD_TEST(new_generator_with_inconsistent_name_returns_null);
    ADD_ABORT_TEST(new_generator_with_null_error_panics);
    ADD_ABORT_TEST(set_invalid_flags_panics);
    ADD_ABORT_TEST(set_null_implementation_name_panics);
    ADD_ABORT_TEST(set_null_implementation_panics);
    ADD_TEST(destroy_null_generator_works);
    ADD_TEST(generator_owns_allocator);
    ADD_TEST(new_generator_with_failing_init_returns_null);
    ADD_TEST(new_generator_with_failing_init_has_correct_error);
    ADD_TEST(generator_with_implementation_has_correct_name);
    ADD_TEST(generator_is_reentrant_if_implementation_has_flag);
    ADD_TEST(generator_is_not_reentrant_if_implementation_lacks_flag);
    ADD_ABORT_TEST(panics_if_generate_returns_too_large);
    ADD_TEST(generate_when_result_is_24);
    ADD_TEST(generate_when_result_is_35);
    ADD_TEST(generate_returns_max_or_less);
    ADD_TEST(new_generator_copies_implementation);
    ADD_TEST(destroy_generator_calls_deinit_with_state);

    ADD_ABORT_TEST(set_null_getenv_panics);
    ADD_TEST(new_generator_respects_environment_variable);
    ADD_TEST(standard_getenv_works);
    ADD_TEST(no_getenv_works);

    ADD_ABORT_TEST(set_implementation_with_null_name_panics);
    ADD_ABORT_TEST(set_implementation_with_0_seed_size_panics);
    ADD_ABORT_TEST(set_implementation_with_0_max_panics);
    ADD_ABORT_TEST(set_implementation_with_invalid_flags_in_impl_panics);
    ADD_ABORT_TEST(set_implementation_with_null_init_panics);
    ADD_ABORT_TEST(set_implementation_with_null_deinit_panics);
    ADD_ABORT_TEST(set_implementation_with_null_seed_panics);
    ADD_ABORT_TEST(set_implementation_with_null_generate_panics);

    ADD_TEST(new_seeder_with_failing_allocator_returns_null);
    ADD_ABORT_TEST(destroy_generator_with_seeder_panics);
    ADD_ABORT_TEST(create_multiple_seeders_panics);
    ADD_TEST(mix_4_bytes_into_8_byte_seed);
    ADD_TEST(mix_8_bytes_into_8_byte_seed);
    ADD_TEST(mix_8_bytes_into_4_byte_seed);
    ADD_TEST(mix_1024_bytes_into_128_byte_seed);
    ADD_TEST(mix_2_bytes_4_times_is_same_as_mix_8_bytes);

    ADD_TEST(mix_non_existing_file_fails);
#if HAVE_FMEMOPEN
    ADD_TEST(mix_unreadable_stream_fails);
    ADD_TEST(mix_stream_with_0_size_mixes_seed_size);
    ADD_TEST(mix_stream_with_fixed_size_mixes_that_size);
    ADD_TEST(mix_stream_with_too_large_size_stops_at_eof);
#endif /* HAVE_FMEMOPEN */

    ADD_TEST(fork_generator_with_failing_allocator_fails);
    ADD_TEST(fork_generator_with_allocator_failing_after_one_fails);
    ADD_TEST(fork_generator_with_allocator_failing_after_two_succeeds);
    ADD_ABORT_TEST(fork_generator_with_null_error_panics);
}
