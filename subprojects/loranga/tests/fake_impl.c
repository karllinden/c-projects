/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check.h>

#include "fake_impl.h"

int fake_init_code;
void *fake_init_state = NULL;
uint32_t fake_result;
unsigned char fake_seed_bytes[FAKE_SEED_BYTES_SIZE];
struct lra_implementation fake_impl;

static int fake_init(void *state) {
    fake_init_state = state;
    return fake_init_code;
}

static void fake_deinit(void *state) {
    ck_assert_ptr_eq(fake_init_state, state);
    fake_init_state = NULL;
}

static void fake_seed(ecb_unused void *state,
                      ecb_unused const unsigned char *seed) {
    memcpy(fake_seed_bytes, seed, fake_impl.seed_size);
}

static uint32_t fake_generate(ecb_unused void *state) {
    return fake_result;
}

void set_up_fake_impl(void) {
    fake_init_code = 0;
    fake_init_state = NULL;
    fake_result = 0;
    memset(fake_seed_bytes, 0, FAKE_SEED_BYTES_SIZE);

    fake_impl.name = "fake";
    fake_impl.state_size = 0;
    fake_impl.seed_size = 8;
    fake_impl.max = UINT32_MAX;
    fake_impl.flags = 0;
    fake_impl.init = fake_init;
    fake_impl.deinit = fake_deinit;
    fake_impl.seed = fake_seed;
    fake_impl.generate = fake_generate;
}
