/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include <check-simpler.h>

#include <loranga/simple.h>

#include "sequence.h"

static struct lra_error error;

void set_up(void) {
    lra_clear_error(&error);
}

void tear_down(void) {
    /* empty */
}

START_TEST(new_application_generator_with_null_error_panics) {
    lra_new_application_generator(0, NULL);
}
END_TEST

START_TEST(new_testing_generator_with_null_error_panics) {
    lra_new_testing_generator(0, NULL);
}
END_TEST

START_TEST(new_application_generator_with_invalid_flags_panics) {
    lra_new_application_generator(0x04, &error);
}
END_TEST

START_TEST(new_testing_generator_with_invalid_flags_panics) {
    lra_new_application_generator(0x04, &error);
}
END_TEST

typedef lra_generator_t *new_generator_fn(lra_flags_t flags,
                                          struct lra_error *error);

static void assert_returns_non_null(new_generator_fn *new_generator) {
    lra_generator_t *generator = new_generator(0, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, generator);

    lra_destroy_generator(generator);
}

START_TEST(new_application_generator_returns_non_null) {
    assert_returns_non_null(lra_new_application_generator);
}
END_TEST

START_TEST(new_testing_generator_returns_non_null) {
    assert_returns_non_null(lra_new_testing_generator);
}
END_TEST

static void generate_sequence(new_generator_fn *new_generator,
                              struct sequence *seq) {
    lra_generator_t *generator = new_generator(LRA_REENTRANT, &error);
    ck_assert_ptr_ne(NULL, generator);
    ck_assert(lra_generator_is_reentrant(generator));

    sequence_generate(seq, generator, 100000);
    lra_destroy_generator(generator);
}

static bool generates_same_sequence(new_generator_fn *new_generator) {
    struct sequence seq1;
    struct sequence seq2;
    generate_sequence(new_generator, &seq1);
    generate_sequence(new_generator, &seq2);
    return sequence_equal(&seq1, &seq2);
}

START_TEST(different_application_generators_produce_different_sequences) {
    ck_assert(!generates_same_sequence(lra_new_application_generator));
}
END_TEST

START_TEST(different_testing_generators_produce_different_sequences) {
    ck_assert(!generates_same_sequence(lra_new_application_generator));
}
END_TEST

#if HAVE_SETENV
static bool respects_fixed_seed(new_generator_fn *new_generator,
                                const char *seed) {
    setenv("LORANGA_SEED", seed, 1);
    return generates_same_sequence(new_generator);
}

#define MAX_SEED_LEN 64
static bool respects_seed(new_generator_fn *new_generator) {
    char seed[MAX_SEED_LEN + 1];
    memset(seed, '\0', MAX_SEED_LEN + 1);
    for (size_t i = 0; i < MAX_SEED_LEN; ++i) {
        seed[i] = 'a';
        if (respects_fixed_seed(new_generator, seed)) {
            return true;
        }
    }
    return false;
}

START_TEST(new_application_generator_does_not_respect_seed) {
    ck_assert(!respects_seed(lra_new_application_generator));
}
END_TEST

START_TEST(new_testing_generator_respects_seed) {
    ck_assert(respects_seed(lra_new_testing_generator));
}
END_TEST
#endif /* HAVE_SETENV */

void add_tests(void) {
    ADD_ABORT_TEST(new_application_generator_with_null_error_panics);
    ADD_ABORT_TEST(new_testing_generator_with_null_error_panics);
    ADD_ABORT_TEST(new_application_generator_with_invalid_flags_panics);
    ADD_ABORT_TEST(new_testing_generator_with_invalid_flags_panics);
    ADD_TEST(new_application_generator_returns_non_null);
    ADD_TEST(new_testing_generator_returns_non_null);
    ADD_TEST(different_application_generators_produce_different_sequences);
    ADD_TEST(different_testing_generators_produce_different_sequences);
#if HAVE_SETENV
    ADD_TEST(new_application_generator_does_not_respect_seed);
    ADD_TEST(new_testing_generator_respects_seed);
#endif /* HAVE_SETENV */
}
