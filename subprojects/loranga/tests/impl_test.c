/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include <check-simpler.h>
#include <dima/system.h>

#include <loranga/advanced.h>
#include <loranga/simple.h>

#include "fake_getenv.h"
#include "sequence.h"

#define SEED_SIZE 16

static const unsigned char seed1[SEED_SIZE] = {
        0xDE,
        0xAD,
        0xBE,
        0xEF,
        0xBA,
        0xAA,
        0xAA,
        0xAD,
        0xBE,
        0xEF,
        0xBA,
        0xBE,
        0xCA,
        0xFE,
        0xD0,
        0x0D,
};

static const unsigned char seed2[SEED_SIZE] = {
        0xCA,
        0xFE,
        0xBA,
        0xBE,
        0xB1,
        0x05,
        0xF0,
        0x0D,
        0x0D,
        0x15,
        0xEA,
        0x5E,
        0xFE,
        0xED,
        0xC0,
        0xDE,
};

static lra_config_t *config;
static struct lra_error error;
static lra_generator_t *generator;

void set_up(void) {
    config = lra_new_config(dima_system_instance());
    ck_assert_ptr_ne(NULL, config);
    lra_set_implementation_name(config, IMPL_NAME);

    lra_clear_error(&error);
    generator = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, generator);
}

void tear_down(void) {
    lra_destroy_generator(generator);
    generator = NULL;
    lra_destroy_config(config);
    config = NULL;
}

static void seed_generator(const unsigned char *seed) {
    lra_seeder_t *seeder = lra_new_seeder(generator);
    lra_mix_bytes(seeder, seed, SEED_SIZE);
    lra_seed(seeder);
}

static void generate_sequence(const unsigned char *seed,
                              unsigned max,
                              struct sequence *seq) {
    seed_generator(seed);
    sequence_generate(seq, generator, max);
}

START_TEST(generator_has_correct_name) {
    ck_assert_str_eq(IMPL_NAME, lra_generator_name(generator));
}
END_TEST

START_TEST(forked_generator_has_correct_name) {
    if (!lra_generator_is_reentrant(generator)) {
        return;
    }

    lra_generator_t *fork = lra_fork_generator(generator, &error);
    ck_assert_ptr_ne(NULL, fork);
    ck_assert_str_eq(IMPL_NAME, lra_generator_name(fork));
    ck_assert(lra_generator_is_reentrant(fork));
    lra_destroy_generator(fork);
}
END_TEST

START_TEST(fork_non_reentrant_generator_panics) {
    if (lra_generator_is_reentrant(generator)) {
        abort();
    }

    lra_fork_generator(generator, &error);
}
END_TEST

START_TEST(cannot_be_loaded_as_reentrant_if_non_reentrant) {
    if (lra_generator_is_reentrant(generator)) {
        return;
    }

    lra_set_flags(config, LRA_REENTRANT);
    lra_generator_t *g = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_NOT_IMPLEMENTED, error.code);
    ck_assert_ptr_eq(NULL, g);
}
END_TEST

START_TEST(cannot_be_loaded_from_env_as_reentrant_if_non_reentrant) {
    if (lra_generator_is_reentrant(generator)) {
        return;
    }

    fake_implementation_value = IMPL_NAME;
    lra_config_t *c = lra_new_config(dima_system_instance());
    lra_set_getenv(c, fake_getenv);
    lra_set_flags(c, LRA_REENTRANT);

    lra_generator_t *g = lra_new_unseeded_generator(c, &error);
    ck_assert_int_eq(LRA_NOT_IMPLEMENTED, error.code);
    ck_assert_ptr_eq(NULL, g);

    lra_destroy_config(c);
}
END_TEST

START_TEST(can_be_loaded_from_fake_env) {
    fake_implementation_value = IMPL_NAME;
    lra_config_t *c = lra_new_config(dima_system_instance());
    lra_set_getenv(c, fake_getenv);

    lra_generator_t *g = lra_new_unseeded_generator(c, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, g);

    lra_destroy_generator(g);
    lra_destroy_config(c);
}
END_TEST

#if HAVE_SETENV
typedef lra_generator_t *new_generator_fn(lra_flags_t flags,
                                          struct lra_error *error);

static void assert_respects_real_env(new_generator_fn *new_generator) {
    setenv("LORANGA_IMPLEMENTATION", IMPL_NAME, 1);
    lra_generator_t *g = new_generator(0, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, g);
    ck_assert_str_eq(IMPL_NAME, lra_generator_name(g));
    lra_destroy_generator(g);
}

START_TEST(new_application_generator_respects_real_env) {
    assert_respects_real_env(lra_new_application_generator);
}
END_TEST

START_TEST(new_testing_generator_respects_real_env) {
    assert_respects_real_env(lra_new_testing_generator);
}
END_TEST
#endif /* HAVE_SETENV */

#if HAVE_OPEN_MEMSTREAM
typedef void act_fn(void);

static void assert_testing_output(act_fn *act) {
    char *buf = NULL;
    size_t size = 0;
    FILE *out = open_memstream(&buf, &size);
    if (out == NULL) {
        perror("open_memstream");
        abort();
    }

    lra_enable_testing(config, out);

    act();

    fclose(out);

    ck_assert_str_eq("export LORANGA_IMPLEMENTATION=" IMPL_NAME "\n", buf);
    free(buf);
}

static void create_generator(void) {
    lra_generator_t *g = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, g);
    lra_destroy_generator(g);
}

START_TEST(new_generator_prints_impl_name_while_testing) {
    assert_testing_output(create_generator);
}
END_TEST

static void create_forked_generator(void) {
    lra_generator_t *g = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, g);

    lra_generator_t *f = lra_fork_generator(g, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, f);

    lra_destroy_generator(f);
    lra_destroy_generator(g);
}

START_TEST(fork_generator_does_not_print_seed_while_testing) {
    if (!lra_generator_is_reentrant(generator)) {
        return;
    }
    assert_testing_output(create_forked_generator);
}
END_TEST
#endif /* HAVE_OPEN_MEMSTREAM */

START_TEST(re_seeded_generator_produces_same_sequence_max) {
    struct sequence seq1;
    struct sequence seq2;
    generate_sequence(seed1, UINT32_MAX, &seq1);
    generate_sequence(seed1, UINT32_MAX, &seq2);
    ck_assert(sequence_equal(&seq1, &seq2));
}
END_TEST

START_TEST(re_seeded_generator_produces_same_sequence_nomax) {
    struct sequence seq1;
    struct sequence seq2;
    generate_sequence(seed2, 1337, &seq1);
    generate_sequence(seed2, 1337, &seq2);
    ck_assert(sequence_equal(&seq1, &seq2));
}
END_TEST

START_TEST(different_seeds_produce_different_sequences_max) {
    struct sequence seq1;
    struct sequence seq2;
    generate_sequence(seed1, UINT32_MAX, &seq1);
    generate_sequence(seed2, UINT32_MAX, &seq2);
    ck_assert(!sequence_equal(&seq1, &seq2));
}
END_TEST

START_TEST(different_seeds_produce_different_sequences_nomax) {
    struct sequence seq1;
    struct sequence seq2;
    generate_sequence(seed2, 756, &seq2);
    generate_sequence(seed1, 756, &seq1);
    ck_assert(!sequence_equal(&seq1, &seq2));
}
END_TEST

START_TEST(forked_generators_produce_different_sequences) {
    if (!lra_generator_is_reentrant(generator)) {
        return;
    }

    lra_generator_t *f1 = lra_fork_generator(generator, &error);
    lra_generator_t *f2 = lra_fork_generator(generator, &error);

    ck_assert_ptr_ne(f1, NULL);
    ck_assert_ptr_ne(f2, NULL);

    struct sequence seq1;
    struct sequence seq2;
    sequence_generate(&seq1, f1, UINT32_MAX);
    sequence_generate(&seq2, f2, UINT32_MAX);
    ck_assert(!sequence_equal(&seq1, &seq2));

    lra_destroy_generator(f1);
    lra_destroy_generator(f2);
}
END_TEST

START_TEST(forked_generator_has_default_entropy_when_not_testing) {
    if (!lra_generator_is_reentrant(generator)) {
        return;
    }

    lra_generator_t *g = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, g);

    /*
     * Although generator and g produce the same sequence, forking each of them
     * should produce generators with different sequences, because default
     * entropy is mixed in non-testing mode.
     */
    lra_generator_t *f1 = lra_fork_generator(generator, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, f1);

    lra_generator_t *f2 = lra_fork_generator(g, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, f2);

    struct sequence seq1;
    struct sequence seq2;
    sequence_generate(&seq1, f1, UINT32_MAX);
    sequence_generate(&seq2, f2, UINT32_MAX);
    ck_assert(!sequence_equal(&seq1, &seq2));

    lra_destroy_generator(f2);
    lra_destroy_generator(f1);
    lra_destroy_generator(g);
}
END_TEST

START_TEST(forked_generator_does_not_have_default_entropy_when_testing) {
    if (!lra_generator_is_reentrant(generator)) {
        return;
    }

    lra_enable_testing(config, stderr);

    lra_generator_t *g1 = lra_new_unseeded_generator(config, &error);
    lra_generator_t *g2 = lra_new_unseeded_generator(config, &error);
    ck_assert_ptr_ne(NULL, g1);
    ck_assert_ptr_ne(NULL, g2);

    /*
     * Because g1 and g2 generate the same sequence and are in testing mode,
     * their forks will also generate the same sequence.
     */
    lra_generator_t *f1 = lra_fork_generator(g1, &error);
    lra_generator_t *f2 = lra_fork_generator(g2, &error);
    ck_assert_ptr_ne(NULL, f1);
    ck_assert_ptr_ne(NULL, f2);

    struct sequence seq1;
    struct sequence seq2;
    sequence_generate(&seq1, f1, UINT32_MAX);
    sequence_generate(&seq2, f2, UINT32_MAX);
    ck_assert(sequence_equal(&seq1, &seq2));

    lra_destroy_generator(f2);
    lra_destroy_generator(f1);
    lra_destroy_generator(g2);
    lra_destroy_generator(g1);
}
END_TEST

void add_tests(void) {
    ADD_TEST(generator_has_correct_name);
    ADD_TEST(forked_generator_has_correct_name);
    ADD_ABORT_TEST(fork_non_reentrant_generator_panics);
    ADD_TEST(cannot_be_loaded_as_reentrant_if_non_reentrant);
    ADD_TEST(cannot_be_loaded_from_env_as_reentrant_if_non_reentrant);
    ADD_TEST(can_be_loaded_from_fake_env);
#if HAVE_SETENV
    ADD_TEST(new_application_generator_respects_real_env);
    ADD_TEST(new_testing_generator_respects_real_env);
#endif /* HAVE_SETENV */
#if HAVE_OPEN_MEMSTREAM
    ADD_TEST(new_generator_prints_impl_name_while_testing);
    ADD_TEST(fork_generator_does_not_print_seed_while_testing);
#endif /* HAVE_OPEN_MEMSTREAM */
    ADD_TEST(re_seeded_generator_produces_same_sequence_max);
    ADD_TEST(re_seeded_generator_produces_same_sequence_nomax);
    ADD_TEST(different_seeds_produce_different_sequences_max);
    ADD_TEST(different_seeds_produce_different_sequences_nomax);
    ADD_TEST(forked_generators_produce_different_sequences);
    ADD_TEST(forked_generator_has_default_entropy_when_not_testing);
    ADD_TEST(forked_generator_does_not_have_default_entropy_when_testing);
}
