/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <limits.h>
#include <time.h>

#include <check-simpler.h>
#include <dima/system.h>

#include <loranga/advanced.h>

#include "sequence.h"

static lra_generator_t *generator;

void set_up(void) {
    lra_config_t *config = lra_new_config(dima_system_instance());
    ck_assert_ptr_ne(NULL, config);

    struct lra_error error;
    lra_clear_error(&error);
    generator = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, generator);

    lra_destroy_config(config);
}

void tear_down(void) {
    lra_destroy_generator(generator);
    generator = NULL;
}

static void mix_default(void) {
    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);
    lra_mix_default(seeder);
    lra_seed(seeder);
}

START_TEST(unseeded_and_default_mixed_generators_produce_different_sequences) {
    struct sequence seq1;
    struct sequence seq2;

    sequence_generate(&seq1, generator, UINT_MAX);
    mix_default();
    sequence_generate(&seq2, generator, UINT_MAX);

    ck_assert(!sequence_equal(&seq1, &seq2));
}
END_TEST

START_TEST(default_mixed_generators_produce_different_sequences) {
    struct sequence seq1;
    struct sequence seq2;

    mix_default();
    sequence_generate(&seq1, generator, UINT_MAX);
    mix_default();
    sequence_generate(&seq2, generator, UINT_MAX);

    ck_assert(!sequence_equal(&seq1, &seq2));
}
END_TEST

void add_tests(void) {
    ADD_TEST(unseeded_and_default_mixed_generators_produce_different_sequences);
    ADD_TEST(default_mixed_generators_produce_different_sequences);
}
