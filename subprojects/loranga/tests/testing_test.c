/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include <check-simpler.h>
#include <dima/system.h>

#include <loranga/advanced.h>

#include "fake_getenv.h"
#include "fake_impl.h"

#define EXPECTED_BUF_SIZE 128

static char *buf;
static size_t size;
static FILE *out;
static lra_config_t *config;

static char expected_buf[EXPECTED_BUF_SIZE];
static size_t expected_buf_len;

void set_up(void) {
    set_up_fake_impl();

    buf = NULL;
    size = 0;

    out = open_memstream(&buf, &size);
    if (out == NULL) {
        perror("open_memstream");
        abort();
    }

    config = lra_new_config(dima_system_instance());
    ck_assert_ptr_ne(NULL, config);
    lra_enable_testing(config, out);
    lra_set_getenv(config, fake_getenv);

    memset(expected_buf, 0, EXPECTED_BUF_SIZE);
    expected_buf_len = 0;
}

void tear_down(void) {
    lra_destroy_config(config);
    config = NULL;
    fclose(out);
    out = NULL;
    free(buf);
    buf = NULL;
}

static lra_generator_t *new_generator(void) {
    struct lra_error error;
    lra_clear_error(&error);
    lra_generator_t *generator = lra_new_unseeded_generator(config, &error);
    ck_assert_int_eq(LRA_OK, error.code);
    ck_assert_ptr_ne(NULL, generator);
    return generator;
}

static lra_generator_t *new_fake_generator(size_t seed_size) {
    fake_impl.seed_size = seed_size;
    lra_set_implementation(config, &fake_impl);
    return new_generator();
}

static void append_expected_buf(const char *s) {
    size_t len = strlen(s);
    ck_assert_int_lt(expected_buf_len + len, EXPECTED_BUF_SIZE);

    memcpy(expected_buf + expected_buf_len, s, len);
    expected_buf_len += len;
    expected_buf[expected_buf_len] = '\0';
}

static void append_expected_buf_char(char ch) {
    char str[2];
    str[0] = ch;
    str[1] = '\0';
    append_expected_buf(str);
}

START_TEST(enable_testing_with_null_out_panics) {
    lra_config_t *config = lra_new_config(dima_system_instance());
    ck_assert_ptr_ne(NULL, config);
    lra_enable_testing(config, NULL);
}
END_TEST

#define MAX_SEED_SIZE 6
struct seed {
    size_t size;
    const char *hexadecimal;
    unsigned char bytes[MAX_SEED_SIZE];
};

static const struct seed seeds[] = {
        {4, "deadbeef", {0xDE, 0xAD, 0xBE, 0xEF, 0, 0}},
        {6, "baadcafebabe", {0xBA, 0xAD, 0xCA, 0xFE, 0xBA, 0xBE}},
        {3, "101ca7", {0x10, 0x1C, 0xA7}},
};
#define N_SEEDS ecb_array_length(seeds)

static void assert_fake_seeded(const struct seed *seed) {
    ck_assert_int_eq(0, memcmp(seed->bytes, fake_seed_bytes, seed->size));

    fflush(out);
    append_expected_buf("export LORANGA_IMPLEMENTATION=fake\n");
    append_expected_buf("export LORANGA_SEED=");
    append_expected_buf(seed->hexadecimal);
    append_expected_buf("\n");
    ck_assert_str_eq(expected_buf, buf);
}

START_TEST(seed_prints_hexadecimal) {
    const struct seed *seed = seeds + _i;
    lra_generator_t *generator = new_fake_generator(seed->size);
    lra_seeder_t *seeder = lra_new_seeder(generator);
    ck_assert_ptr_ne(NULL, seeder);

    lra_mix_bytes(seeder, seed->bytes, seed->size);
    lra_seed(seeder);

    assert_fake_seeded(seed);
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(respects_loranga_environment_variable) {
    const struct seed *seed = seeds + _i;
    lra_generator_t *generator = new_fake_generator(seed->size);
    lra_seeder_t *seeder = lra_new_seeder(generator);
    fake_seed_value = seed->hexadecimal;
    lra_seed(seeder);

    assert_fake_seeded(seed);
    lra_destroy_generator(generator);
}
END_TEST

START_TEST(prints_warning_if_seed_has_length_9_and_8_is_expected) {
    lra_generator_t *generator = new_fake_generator(4);
    lra_seeder_t *seeder = lra_new_seeder(generator);
    fake_seed_value = "a0b1c2d3e";
    lra_seed(seeder);

    fflush(out);
    append_expected_buf("export LORANGA_IMPLEMENTATION=fake\n");
    append_expected_buf(
            "warning: LORANGA_SEED is 9 characters, but should have been 8\n");
    append_expected_buf("export LORANGA_SEED=00000000\n");
    ck_assert_str_eq(expected_buf, buf);

    lra_destroy_generator(generator);
}
END_TEST

START_TEST(prints_warning_if_seed_has_length_5_and_6_is_expected) {
    lra_generator_t *generator = new_fake_generator(3);
    lra_seeder_t *seeder = lra_new_seeder(generator);
    fake_seed_value = "f1e3d";
    lra_seed(seeder);

    fflush(out);
    append_expected_buf("export LORANGA_IMPLEMENTATION=fake\n");
    append_expected_buf(
            "warning: LORANGA_SEED is 5 characters, but should have been 6\n");
    append_expected_buf("export LORANGA_SEED=000000\n");
    ck_assert_str_eq(expected_buf, buf);

    lra_destroy_generator(generator);
}
END_TEST

static char invalid_chars[] = {'/', ':', '`', 'g'};

START_TEST(prints_warning_if_seed_has_invalid_character) {
    char seed[] = {'a', 'b', 'c', 'd', '\0'};
    char invalid_char = invalid_chars[_i];
    seed[_i] = invalid_char;

    lra_generator_t *generator = new_fake_generator(2);
    lra_seeder_t *seeder = lra_new_seeder(generator);
    fake_seed_value = seed;
    lra_seed(seeder);

    fflush(out);
    append_expected_buf("export LORANGA_IMPLEMENTATION=fake\n");
    append_expected_buf("warning: LORANGA_SEED contains invalid character '");
    append_expected_buf_char(invalid_char);
    append_expected_buf("'\n");
    append_expected_buf("export LORANGA_SEED=0000\n");
    ck_assert_str_eq(expected_buf, buf);

    lra_destroy_generator(generator);
}
END_TEST

void add_tests(void) {
    ADD_ABORT_TEST(enable_testing_with_null_out_panics);
    ADD_LOOP_TEST(seed_prints_hexadecimal, 0, N_SEEDS);
    ADD_LOOP_TEST(respects_loranga_environment_variable, 0, N_SEEDS);
    ADD_TEST(prints_warning_if_seed_has_length_9_and_8_is_expected);
    ADD_TEST(prints_warning_if_seed_has_length_5_and_6_is_expected);
    ADD_LOOP_TEST(prints_warning_if_seed_has_invalid_character,
                  0,
                  ecb_array_length(invalid_chars));
}
