/*
 * Copyright (C) 2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>
#include <dima/system.h>

#include <loranga/advanced.h>

#include "sequence.h"

typedef int mix_dev_fn(lra_seeder_t *);

static lra_config_t *config;
static struct lra_error error;

void set_up(void) {
    config = lra_new_config(dima_system_instance());
    lra_clear_error(&error);
}

void tear_down(void) {
    lra_destroy_config(config);
}

static void generate_sequence(mix_dev_fn *mix_dev, struct sequence *seq) {
    lra_generator_t *generator = lra_new_unseeded_generator(config, &error);

    lra_seeder_t *seeder = lra_new_seeder(generator);
    int result = mix_dev(seeder);
    ck_assert_int_eq(0, result);
    lra_seed(seeder);

    sequence_generate(seq, generator, 1000);

    lra_destroy_generator(generator);
}

static void assert_different_sequences(mix_dev_fn *mix_dev) {
    struct sequence seq1;
    struct sequence seq2;
    generate_sequence(mix_dev, &seq1);
    generate_sequence(mix_dev, &seq2);
    ck_assert(!sequence_equal(&seq1, &seq2));
}

START_TEST(dev_random_seeded_generators_produce_different_sequences) {
    assert_different_sequences(lra_mix_dev_random);
}
END_TEST

START_TEST(dev_urandom_seeded_generators_produce_different_sequences) {
    assert_different_sequences(lra_mix_dev_urandom);
}
END_TEST

void add_tests(void) {
    ADD_TEST(dev_random_seeded_generators_produce_different_sequences);
    ADD_TEST(dev_urandom_seeded_generators_produce_different_sequences);
}
