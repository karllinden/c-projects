/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * loranga/implementation.h
 * ------------------------
 * Declarations needed to implement pseudo random number generators.
 */

#ifndef LORANGA_IMPLEMENTATION_H
#define LORANGA_IMPLEMENTATION_H

#include <loranga/flags.h>

/**
 * Structure for implementing new pseudo random number generators.
 */
struct lra_implementation {
    /**
     * The name is used for selecting the implementation (without a compile-time
     * dependency to the struct lra_implementation). Currently only built-in
     * implementations can be selected by their name.
     */
    const char *name;

    /**
     * The size of the state that is needed for producing pseudo random numbers.
     */
    size_t state_size;

    /**
     * The number of bytes to seed the state with.
     */
    size_t seed_size;

    /**
     * The maximum number that this generator can produce.
     */
    uint32_t max;

    /*
     * The flags, ORed together. See loranga/flags.h for bits.
     */
    lra_flags_t flags;

    /**
     * Initializes a state of size state_size.
     *
     * @param state the state to initialize
     * @return zero on success, or a non-zero, implementation specific code on
     *         failure
     */
    int (*init)(void *state);

    /*
     * Deinitializes a successfully initialized state.
     *
     * Loranga zeroes out the generator state after deinitialization, so
     * implementations only need to release resources as part of their
     * deinitialization. Implementations that do not hold resources can use
     * lra_no_deinit.
     */
    void (*deinit)(void *state);

    /**
     * Seeds a successfully initialized state with a seed of size seed_size.
     */
    void (*seed)(void *state, const unsigned char *seed);

    /**
     * Generates a uniformly distributed 32-bit unsigned integer in the range
     * [0,max] using a successfully initialized state.
     */
    uint32_t (*generate)(void *state);
};

/*
 * No-op implementation of init in struct lra_implementation.
 */
int lra_no_init(void *state);

/**
 * No-op implementation of deinit in struct lra_implementation.
 */
void lra_no_deinit(void *state);

#endif /* !LORANGA_IMPLEMENTATION_H */
