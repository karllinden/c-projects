/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * loranga/loranga.h
 * -----------------
 * The main header for the Loranga library. The loranga library reserves the
 * LORANGA_, LRA_ and lra_ prefixes for its definitions and symbols.
 *
 * This header declares the interface for consuming pseudo random number
 * generators, which for most clients is only generating numbers with them. To
 * create a generator, preferably use the simple API defined in
 * loranga/simple.h, or use the advanced API in loranga/advanced.h if the simple
 * API is not sufficient.
 *
 * Loranga generators are not safe for multithreaded use, because synchronizing
 * access to the generator state would create unnecessary contention. Instead,
 * reentrant generators can be forked with lra_fork_generator which creates
 * another generator that can be used from another thread. Non-reentrant
 * generators are thread-hostile and shall not be used concurrently by multiple
 * threads. To create a reentrant generator, specify LRA_REENTRANT in the flags
 * when creating the generator.
 */

#ifndef LORANGA_LORANGA_H
#define LORANGA_LORANGA_H

#include <stdbool.h>

#include <ecb.h>

/**
 * The codes to distinguish between different types of errors.
 */
enum lra_error_code {
    /**
     * Finished successfully.
     */
    LRA_OK,

    /**
     * The impelementation's init function failed.
     */
    LRA_INIT_FAILED,

    /**
     * Failed seeding from /dev/random.
     */
    LRA_DEV_RANDOM_FAILED,

    /**
     * Insufficient memory.
     */
    LRA_NO_MEMORY,

    /**
     * The requested generator is not implemented.
     */
    LRA_NOT_IMPLEMENTED,
};

struct lra_error {
    enum lra_error_code code;

    /**
     * The name of the implementation, if applicable, or NULL otherwise.
     *
     * This is non-NULL if the code is LRA_NO_MEMRORY or LRA_INIT_FAILED.
     */
    const char *implementation_name;

    /**
     * The return code of the implementation's init function if the code is
     * LRA_INIT_FAILED.
     */
    int init_failure_code;
};

typedef struct lra_generator_s lra_generator_t;

/**
 * Generates a uniformly distributed unsigned 32-bit integer in the range
 * [0,max] with the given generator.
 */
uint32_t lra_generate_uint32(lra_generator_t *generator, uint32_t max);

/**
 * Generates a uniformly distributed unsigned integer in the range [0,max] with
 * the given generator.
 */
unsigned lra_generate_uint(lra_generator_t *generator, unsigned max);

/**
 * Returns true if and only if the given generator is reentrant.
 */
bool lra_generator_is_reentrant(const lra_generator_t *generator);

/**
 * Returns the name of the generator.
 */
const char *lra_generator_name(const lra_generator_t *generator);

/**
 * Destroys the given generator.
 *
 * If the generator to destroy is NULL, this function does nothing. Destroying a
 * generator while seeding it causes undefined behavior.
 */
void lra_destroy_generator(lra_generator_t *generator);

/**
 * Clears the error structure.
 */
void lra_clear_error(struct lra_error *error);

/* TODO: lra_abort_if_failure */

/**
 * Forks the given generator.
 *
 * A fork of a generator is a newly created generator with the same
 * implementation as the original generator that has been seeded with output
 * from the original generator. The fork also has some default entropy mixed
 * into the seed.
 *
 * The fork is independent of the original generator. In particular they can be
 * used by different threads, and no happens-before relation is needed when
 * destroying them.
 *
 * Note that forking a generator is not thread-safe, so the generator must be
 * forked in the thread that owns the original generator before creating the new
 * thread.
 *
 * This function may fail with the error codes documented in lra_new_generator,
 * but it does not fail with LRA_NOT_IMPLEMENTED.
 *
 * @param generator the generator to fork
 * @param error the error structure to fill in on failure
 * @return the fork on success, or NULL on failure
 */
lra_generator_t *lra_fork_generator(lra_generator_t *generator,
                                    struct lra_error *error);

#endif /* !LORANGA_LORANGA_H */
