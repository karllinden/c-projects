/*
 * Copyright (C) 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * loranga/allocator.h
 * -------------------
 * A memory allocator that fails randomly with a given rate.
 *
 * This DIMA proxy implementation uses a Loranga generator (lra_generator_t) to
 * produce its pseudo-random numbers. This allows any PRNG implementation to be
 * used.
 */

#ifndef LORANGA_ALLOCATOR_H
#define LORANGA_ALLOCATOR_H

#include <dima/proxy.h>

#include <loranga/loranga.h>

typedef struct lra_allocator_s lra_allocator_t;

/**
 * Creates a new memory allocator that forwards to the given DIMA and fails
 * randomly with a given percentage.
 *
 * Initially, the failure percentage is 0. To set the failure percentage use
 * lra_set_allocator_failure_percentage.
 *
 * The allocator is allocated using the given DIMA. When the allocator is no
 * longer needed, it shall either be passed to lra_unref_allocator, or its
 * corresponding DIMA (see lra_allocator_to_dima) shall be passed to dima_unref.
 *
 * @param next the DIMA to forward to
 * @param generator the PRNG to use
 * @return the randomly failing allocator, or NULL if memory allocation fails
 */
lra_allocator_t *lra_new_allocator(dima_t *next, lra_generator_t *generator);

/**
 * Decrements the reference count of the allocator.
 *
 * If the allocator is NULL, this function does nothing.
 *
 * @param allocator the allocator, or NULL
 */
void lra_unref_allocator(lra_allocator_t *allocator);

/**
 * Returns the allocator's DIMA implementation.
 */
dima_t *lra_allocator_to_dima(lra_allocator_t *allocator);

/**
 * Sets the failure percentage of the given allocator.
 *
 * A failure_percentage of 0 causes the dima to never fail. On the other
 * extreme, a failure_percentage of 100 causes the allocator to always fail.
 * Calling this function with a failure_percentage that is less than 0 or
 * greater than 100 invokes undefined behavior.
 *
 * @param allocator the allocator DIMA to modify
 * @param failure_percentage the failure percentage
 */
void lra_set_allocator_failure_percentage(lra_allocator_t *allocator,
                                          unsigned failure_percentage);

#endif /* !LORANGA_ALLOCATOR_H */
