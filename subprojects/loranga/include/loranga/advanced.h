/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * loranga/advanced.h
 * ------------------
 * Advanced API to Loranga.
 *
 * The functions in this header allow clients to customize the Loranga generator
 * much more than the simple API. In particular, it is possible to customize:
 *  - the memory allocator (see lra_new_config),
 *  - the environment (see lra_set_getenv),
 *  - the output stream to use in testing mode (see lra_enable_testing),
 *  - the name of the implementation (see lra_set_implementation_name),
 *  - the exact implementation (see lra_set_implementation).
 *  - the entropy used for seeding the generator (see the lra_mix_* functions)
 *
 * To create a pseudo random number generator with the advanced API, clients
 * must follow this workflow:
 *  1. Create a new configuration with lra_new_config.
 *  2. Optionally, customize the newly created configuration.
 *  3. Create a generator with lra_new_generator from the configuration.
 *  4. Create a seeder with lra_new_seeder.
 *  5. Mix entropy into the seeder with the lra_mix_* functions.
 *  6. Seed the generator with lra_seed.
 */

#ifndef LORANGA_ADVANCED_H
#define LORANGA_ADVANCED_H

#include <stdio.h>

#include <dima/dima.h>

#include <loranga/implementation.h>
#include <loranga/loranga.h>

typedef struct lra_config_s lra_config_t;
typedef struct lra_seeder_s lra_seeder_t;

typedef const char *lra_getenv_fn(const char *name);

/**
 * The types of result that lra_mix_stream (and lra_mix_file) may produce.
 */
enum lra_mix_result_type {
    /**
     * All the requested bytes were read from the stream. In this case, the
     * bytes_read is equal to the requested number of bytes to read.
     */
    LRA_MIX_RESULT_OK,

    /**
     * End-of-file was encountered before all the requesed bytes were read.
     */
    LRA_MIX_RESULT_EOF,

    /**
     * An error was encountered while reading from the stream.
     */
    LRA_MIX_RESULT_ERROR,
};

/**
 * The result structure that lra_mix_stream (and lra_mix_file) fill in.
 */
struct lra_mix_result {
    /*
     * The type-code that describes this result.
     */
    enum lra_mix_result_type type;

    /**
     * The number of bytes that were read from the stream.
     */
    size_t bytes_read;
};

/**
 * The standard getenv function suitable for lra_set_getenv.
 */
const char *lra_standard_getenv(const char *name);

/**
 * The secure_getenv function, if the system supports it, suitable for
 * lra_set_getenv. If secure_getenv is not supported, this function behaves as
 * the standard getenv.
 */
const char *lra_secure_getenv(const char *name);

/**
 * Function suitable for lra_set_getenv that prevents Loranga from accessing the
 * environment.
 */
const char *lra_no_getenv(const char *name);

/**
 * Creates a new Loranga configuration.
 *
 * The given memory allocator is used to allocate the configuration and all
 * generators created with lra_new_generator with the returned configuration.
 *
 * When the configuration is no longer needed, its resources must be released
 * with lra_destroy_config.
 *
 * @param allocator the memory allocator to use
 * @return the configuration, or NULL on if memory allocation fails
 */
lra_config_t *lra_new_config(dima_t *allocator);

/**
 * Destroys the configuration.
 *
 * If the configuration is NULL, this function does nothing.
 */
void lra_destroy_config(lra_config_t *config);

/**
 * Sets the flags that the created generator must have.
 *
 * If a generator implementation with the given flags is not available, then
 * lra_new_generator fails with LRA_NOT_IMPLEMENTED.
 *
 * @params config the configuration
 * @params flags the required flags ORed together
 */
void lra_set_flags(lra_config_t *config, lra_flags_t flags);

/**
 * Sets the name of the implementation that the created generator must have.
 *
 * If a generator implementation with the given name is not available, then
 * lra_new_generator fails with LRA_NOT_IMPLEMENTED.
 *
 * @param config the configuration
 * @param name the implementation name
 */
void lra_set_implementation_name(lra_config_t *config, const char *name);

/**
 * Sets the implementation that the created generator will have.
 */
void lra_set_implementation(lra_config_t *config,
                            const struct lra_implementation *implementation);

/**
 * Enables testing mode on the created generators.
 *
 * By default Loranga is non-testing mode. Testing mode allows reproducing
 * failures in randomized tests.
 *
 * Testing mode means the following behavioral changes:
 *  - lra_new_generator prints the implementation name to the given output
 *    stream.
 *  - lra_seed uses the seed from the LORANGA_SEED environment variable, if set.
 *  - lra_seed prints the seed to the given file.
 *  - lra_fork_generator does not mix default entropy into the seed of  the
 *    fork.
 *
 * @param config the configuration
 * @param out the output stream for printing testing information
 */
void lra_enable_testing(lra_config_t *config, FILE *out);

/**
 * Sets the function that Loranga should use to access environment variables.
 *
 * By default Loranga uses secure_getenv, if supported, to access environment
 * variables. If secure_getenv is not supported Loranga uses getenv by default.
 * See lra_secure_getenv.
 *
 * @param config the configuration
 * @param getenv the function to call instead of lra_secure_getenv
 */
void lra_set_getenv(lra_config_t *config, lra_getenv_fn *getenv);

/**
 * Creates a new unseeded generator according to the given configuration.
 *
 * The returned generator is not seeded, and must be seeded before use. If the
 * seeding behavior of lra_new_generator is acceptable, use that function
 * instead of this one. Otherwise, see lra_new_seeder for how to customize the
 * seeding behavior.
 *
 * This function fails with LRA_NO_MEMORY if memory allocation fails, or
 * LRA_INIT_FAILED if initialization fails. It fails with LRA_NOT_IMPLEMENTED if
 * there is no implementation satisfying the requirements of lra_set_flags and
 * lra_set_implementation_name.
 *
 * This function may be called multiple times with the same configuration. The
 * configuration does not have to outlive the generator. In other words, it is
 * safe to call lra_destroy_config with the configuration before calling
 * lra_destroy_generator with the returned generator.
 *
 * @param config the configuration
 * @param error the error structure to fill in on failure
 * @return the generator on success, or NULL on failure
 */
lra_generator_t *lra_new_unseeded_generator(const lra_config_t *config,
                                            struct lra_error *error);

/**
 * Creates a new seeded generator according to the given configuration.
 *
 * The returned generator is seeded with the system's defult entropy
 * (lra_mix_default) and from /dev/random (lra_mix_dev_random), and can
 * therefore be used directly.
 *
 * This function may fail with an error from lra_new_unseeded_generator. If
 * lra_mix_dev_random fails, then this function fails with
 * LRA_DEV_RANDOM_FAILED.
 *
 * @param config the configuration
 * @param error the error structure to fill in on failure
 * @return the generator on success, or NULL on failure
 */
lra_generator_t *lra_new_generator(const lra_config_t *config,
                                   struct lra_error *error);

/**
 * Creates a new seeder for seeding the given generator.
 *
 * After creating a seeder, clients must mix entropy into the seed using one of
 * the lra_mix_* functions. When the seed has been fully mixed, the seeder must
 * be passed to lra_seed, which completes the seeding.
 *
 * When the seeder is no longer used, its resources must be released with
 * lra_seed, which also seeds the generator.
 *
 * Re-seeding a generator is allowed. However, it is undefined behavior to have
 * more than one seeder for any given generator at any given time.
 *
 * @param generator the generator to seed
 * @return the seeder, or NULL if memory allocation failed
 */
lra_seeder_t *lra_new_seeder(lra_generator_t *generator);

/**
 * Mixes bytes into the seed.
 */
void lra_mix_bytes(lra_seeder_t *seeder, const void *bytes, size_t size);

/**
 * Mixes default entropy into the seed.
 *
 * The default entropy is defined by what the operating system provides, but
 * could for example be the current time, current working directory, uptime,
 * system load, hostname, PID, UID, GID and more.
 *
 * This function does not rely on the kernel's entropy pool. Use
 * lra_mix_dev_random or lra_mix_dev_urandom to mix entropy from the kernel
 * into the seed.
 */
void lra_mix_default(lra_seeder_t *seeder);

/**
 * Mixes bytes from the given stream into the seed.
 *
 * If size is non-zero, that many bytes are read from the file. Otherwise, if
 * size is zero, the number of bytes to read is the seed size of the generator
 * implementation.
 *
 * The result structure is always fully filled in after a call to this function.
 *
 * @param seeder the seeder to update
 * @param stream the stream to update the seed with
 * @param size the number of bytes to read, or 0 to read the seed size
 * @param result a pointer to the structure where the result will be stored
 */
void lra_mix_stream(lra_seeder_t *seeder,
                    FILE *stream,
                    size_t size,
                    struct lra_mix_result *result);

/**
 * Mixes bytes from the given file into the seed.
 *
 * This function behaves exactly as lra_mix_stream, except that it opens and
 * closes the file.
 *
 * If the file cannot be opened the type of the result is LRA_MIX_RESULT_ERROR.
 */
void lra_mix_file(lra_seeder_t *seeder,
                  const char *filename,
                  size_t size,
                  struct lra_mix_result *result);

/**
 * Mixes bytes from /dev/random into the seed.
 *
 * The number of bytes is determined by the generator implementation's seed
 * size.
 *
 * @param seeder the seeder to update
 * @return zero on success, or non-zero on failure
 */
int lra_mix_dev_random(lra_seeder_t *seeder);

/**
 * Mixes bytes from /dev/urandom into the seed.
 *
 * The number of bytes is determined by the generator implementation's seed
 * size.
 *
 * @param seeder the seeder to update
 * @return zero on success, or non-zero on failure
 */
int lra_mix_dev_urandom(lra_seeder_t *seeder);

/**
 * Mixes the output of a pseudo random number generator into the seed.
 *
 * The number of bytes is determined by the generator implementation's seed
 * size.
 *
 * Because the generator whose output to mix into the seed may be the same as
 * the seeded generator, this function is useful to re-seed a generator with
 * some extra entropy while preserving entropy from an earlier seed. For
 * example:
 *
 *   lra_generator_t *generator = ...;
 *   lra_seeder_t *seeder = lra_new_seeder(generator);
 *   // ... error handing ...
 *   lra_mix_generator(seeder, generator);
 *   // ... more calls to lra_mix_* ...
 *   lra_seed(seeder);
 */
void lra_mix_generator(lra_seeder_t *seeder, lra_generator_t *generator);

/**
 * Seeds the generator with the seeder.
 *
 * The seeded generator is the one given to lra_new_seeder. This function
 * releases the resources held by the seeder and the seeder must therefore not
 * be used after a call to this function.
 */
void lra_seed(lra_seeder_t *seeder);

#endif /* !LORANGA_ADVANCED_H */
