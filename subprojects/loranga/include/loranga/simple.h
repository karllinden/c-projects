/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * loranga/simple.h
 * ----------------
 * Simple API to Loranga.
 *
 * The functions allows applications and testing programs to create pseudo
 * random number generators without a lot of customization or "ceremony".
 */

#ifndef LORANGA_SIMPLE_H
#define LORANGA_SIMPLE_H

#include <loranga/flags.h>
#include <loranga/loranga.h>

/**
 * Creates a new seeded generator suitable for (non-testing) applications.
 *
 * The returned generator is seeded, as if lra_new_generator was used. This
 * function may fail with the same errors as lra_new_generator.
 *
 * @param flags the flags that the generator must have, usually 0 or
 *              LRA_REENTRANT
 * @param error the error structure to fill in on failure
 * @return the generator on success, or NULL on failure
 */
lra_generator_t *lra_new_application_generator(lra_flags_t flags,
                                               struct lra_error *error);

/**
 * Creates a new seeded generator suitable for testing.
 *
 * The returned generator is seeded, as if lra_new_generator was used. This
 * function may fail with the same errors as lra_new_generator.
 *
 * @param flags the flags that the generator must have, usually 0 or
 *              LRA_REENTRANT
 * @param error the error structure to fill in on failure
 * @return the generator on success, or NULL on failure
 */
lra_generator_t *lra_new_testing_generator(lra_flags_t flags,
                                           struct lra_error *error);

#endif /* !LORANGA_SIMPLE_H */
