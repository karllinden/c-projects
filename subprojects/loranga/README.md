# Loranga Random Number Generator Abstraction

Loranga is library that abstracts random number generators (such as `rand()`,
`lrand48()` and `random()`) that allows clients to code against an interface
that can be implemented by many different random number generators.

## Dependencies

Loranga requires the header file `ecb.h` from
http://software.schmorp.de/pkg/libecb.html to be installed somewhere where the
compiler can find it.

## Installation

This library can be installed with the following commands

```bash
meson build
ninja -C build
sudo ninja -C build install
```

You can then get the compiler flags and libraries through `pkg-config` with
`pkg-config --cflags loranga-${X}` and `pkg-config --libs loranga-${X}` with
`${X}` substituted by the major version of this library.

## Documentation

See the header files in `include/loranga` for the API documentation.
