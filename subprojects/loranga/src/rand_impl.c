/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "rand_impl.h"

static void rand_seed(ecb_unused void *state, const unsigned char *seed) {
    unsigned int dest;
    memcpy(&dest, seed, sizeof(dest));
    srand(dest);
}

static uint32_t rand_generate(ecb_unused void *state) {
    return rand();
}

const struct lra_implementation lra_rand_impl = {
        "rand",
        0,
        sizeof(unsigned int),
        RAND_MAX,
        0,
        lra_no_init,
        lra_no_deinit,
        rand_seed,
        rand_generate,
};
