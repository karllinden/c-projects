/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "random_r_impl.h"

#define STATELEN 256

struct random_r_state {
    struct random_data buf;
    char statebuf[STATELEN];
};

static int random_r_init(void *state) {
    struct random_r_state *s = state;
    memset(s, 0, sizeof(*s));
    initstate_r(1, s->statebuf, STATELEN, &s->buf);
    return 0;
}

static void random_r_seed(void *state, const unsigned char *seed) {
    struct random_r_state *s = state;
    unsigned int dest;
    memcpy(&dest, seed, sizeof(dest));
    srandom_r(dest, &s->buf);
}

static uint32_t random_r_generate(void *state) {
    struct random_r_state *s = state;
    int32_t result;
    random_r(&s->buf, &result);
    return (uint32_t)result;
}

const struct lra_implementation lra_random_r_impl = {
        "random_r",
        sizeof(struct random_r_state),
        sizeof(unsigned int),
        RAND_MAX,
        LRA_REENTRANT,
        random_r_init,
        lra_no_deinit,
        random_r_seed,
        random_r_generate,
};
