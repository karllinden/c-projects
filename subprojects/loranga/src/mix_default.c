/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <time.h>

#if HAVE_UNISTD_H
#include <sys/types.h>
#include <unistd.h>
#endif

#include <loranga/advanced.h>

#define NAME_LEN 64

static void mix_seeder_address(lra_seeder_t *seeder) {
    lra_mix_bytes(seeder, &seeder, sizeof(seeder));
}

static void mix_credentials(ecb_unused lra_seeder_t *seeder) {
#if HAVE_UNISTD_H
    uid_t uid = getuid();
    uid_t euid = geteuid();
    gid_t gid = getgid();
    gid_t egid = getegid();
    pid_t pid = getpid();
    pid_t ppid = getppid();
    lra_mix_bytes(seeder, &uid, sizeof(uid));
    lra_mix_bytes(seeder, &euid, sizeof(euid));
    lra_mix_bytes(seeder, &gid, sizeof(gid));
    lra_mix_bytes(seeder, &egid, sizeof(egid));
    lra_mix_bytes(seeder, &pid, sizeof(pid));
    lra_mix_bytes(seeder, &ppid, sizeof(ppid));
#endif /* HAVE_UNISTD_H */
}

static void mix_times(lra_seeder_t *seeder) {
    clock_t clk = clock();
    lra_mix_bytes(seeder, &clk, sizeof(clk));

#if HAVE_CLOCK_GETTIME
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
#else /* !HAVE_CLOCK_GETTIME */
    time_t now = time(NULL);
#endif /* !HAVE_CLOCK_GETTIME */
    lra_mix_bytes(seeder, &now, sizeof(now));
}

static void mix_network_names(ecb_unused lra_seeder_t *seeder) {
#if HAVE_GETHOSTNAME
    char hostname[NAME_LEN];
    memset(hostname, 0, NAME_LEN);
    gethostname(hostname, NAME_LEN);
    lra_mix_bytes(seeder, hostname, NAME_LEN);
#endif /* HAVE_GETHOSTNAME */

#if HAVE_GETDOMAINNAME
    char domainname[NAME_LEN];
    memset(domainname, 0, NAME_LEN);
    int ret = getdomainname(domainname, NAME_LEN);
    if (ret == 0) {
        lra_mix_bytes(seeder, domainname, NAME_LEN);
    }
#endif /* HAVE_GETDOMAINNAME */
}

void lra_mix_default(lra_seeder_t *seeder) {
    mix_seeder_address(seeder);
    mix_credentials(seeder);
    mix_times(seeder);
    mix_network_names(seeder);
}
