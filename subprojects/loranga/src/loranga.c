/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <limits.h>
#include <stdalign.h>
#include <string.h>

#include <panic.h>

#include "internal.h"

#define SEEDING_FLAG 0x10u
#define SEED_ENV_NAME "LORANGA_SEED"

struct lra_generator_s {
    struct lra_system sys;

    /*
     * The struct lra_implementation is copied into the generator for the
     * following reasons:
     *  1. It prevents modifying the generator in unexpected ways through a
     *     pointer to the struct lra_implementation.
     *  2. It allows lra_new_impl to perform stricter sanity checks without a
     *     TOCTOU condition.
     *  3. It improves cache locality because the max and the generate function
     *     are stored in the same memory block as the state.
     */
    struct lra_implementation impl;

    alignas(max_align_t) char state[];
};

struct lra_seeder_s {
    lra_generator_t *generator;
    size_t current;
    unsigned char seed[];
};

void lra_clear_error(struct lra_error *error) {
    memset(error, 0, sizeof(*error));
}

static void error_no_memory(struct lra_error *error,
                            const struct lra_implementation *implementation) {
    error->code = LRA_NO_MEMORY;
    error->implementation_name = implementation->name;
}

lra_generator_t *lra_new_generator_impl(
        const struct lra_system *sys,
        const struct lra_implementation *implementation,
        struct lra_error *error) {
    panic_check(error != NULL);

    lra_generator_t *generator = dima_alloc(
            sys->allocator, sizeof(*generator) + implementation->state_size);
    if (ecb_expect_false(generator == NULL)) {
        error_no_memory(error, implementation);
        return NULL;
    }

    memcpy(&generator->sys, sys, sizeof(*sys));
    memcpy(&generator->impl, implementation, sizeof(*implementation));

    int init_failure_code = implementation->init(generator->state);
    if (ecb_expect_false(init_failure_code)) {
        dima_free(sys->allocator, generator);
        error->code = LRA_INIT_FAILED;
        error->implementation_name = implementation->name;
        error->init_failure_code = init_failure_code;
        return NULL;
    }

    dima_ref(sys->allocator);
    return generator;
}

static int is_not_seeding(const lra_generator_t *generator) {
    return !(generator->impl.flags & SEEDING_FLAG);
}

void lra_destroy_generator(lra_generator_t *generator) {
    if (generator == NULL) {
        return;
    }

    panic_check(is_not_seeding(generator));

    generator->impl.deinit(generator->state);
    memset(generator->state, 0, generator->impl.state_size);

    dima_t *allocator = generator->sys.allocator;
    dima_free(allocator, generator);
    dima_unref(allocator);
}

static size_t seeder_seed_size(const lra_seeder_t *seeder) {
    return seeder->generator->impl.seed_size;
}

static void zero_seed(lra_seeder_t *seeder) {
    memset(seeder->seed, 0, seeder_seed_size(seeder));
}

lra_seeder_t *lra_new_seeder(lra_generator_t *generator) {
    lra_seeder_t *seeder
            = dima_alloc(generator->sys.allocator,
                         sizeof(*seeder) + generator->impl.seed_size);
    if (ecb_expect_false(seeder == NULL)) {
        return NULL;
    }

    panic_check(is_not_seeding(generator));
    generator->impl.flags |= SEEDING_FLAG;

    seeder->generator = generator;
    seeder->current = 0;
    zero_seed(seeder);
    return seeder;
}

void lra_mix_bytes(lra_seeder_t *seeder, const void *src, size_t size) {
    const unsigned char *bytes = src;
    for (size_t i = 0; i < size; ++i) {
        seeder->seed[seeder->current] ^= bytes[i];
        seeder->current++;
        seeder->current %= seeder_seed_size(seeder);
    }
}

void lra_mix_stream(lra_seeder_t *seeder,
                    FILE *stream,
                    size_t size,
                    struct lra_mix_result *result) {
    if (size == 0) {
        size = seeder_seed_size(seeder);
    }

    size_t bytes_read = 0;
    int r;
    while (bytes_read < size && ecb_expect_true((r = fgetc(stream)) != EOF)) {
        bytes_read++;
        unsigned char ch = (unsigned char)r;
        lra_mix_bytes(seeder, &ch, 1);
    }
    if (ecb_expect_true(bytes_read == size)) {
        result->type = LRA_MIX_RESULT_OK;
    } else if (feof(stream)) {
        result->type = LRA_MIX_RESULT_EOF;
    } else {
        panic_check(ferror(stream));
        result->type = LRA_MIX_RESULT_ERROR;
    }
    result->bytes_read = bytes_read;
}

void lra_mix_file(lra_seeder_t *seeder,
                  const char *filename,
                  size_t size,
                  struct lra_mix_result *result) {
    FILE *stream = fopen(filename, "rb");
    if (stream == NULL) {
        result->type = LRA_MIX_RESULT_ERROR;
        result->bytes_read = 0;
        return;
    }

    lra_mix_stream(seeder, stream, size, result);

    if (fclose(stream)) {
        panic("Failed closing read-only file %s.", filename);
    }
}

static int mix_dev(lra_seeder_t *seeder, const char *filename) {
    struct lra_mix_result result;
    lra_mix_file(seeder, filename, 0, &result);
    return result.type != LRA_MIX_RESULT_OK;
}

int lra_mix_dev_random(lra_seeder_t *seeder) {
    return mix_dev(seeder, "/dev/random");
}

int lra_mix_dev_urandom(lra_seeder_t *seeder) {
    return mix_dev(seeder, "/dev/urandom");
}

void lra_mix_generator(lra_seeder_t *seeder, lra_generator_t *generator) {
    size_t size = seeder_seed_size(seeder);

    uint32_t u32;
    while (size >= sizeof(u32)) {
        size -= sizeof(u32);
        u32 = lra_generate_uint32(generator, UINT32_MAX);
        lra_mix_bytes(seeder, &u32, sizeof(u32));
    }

    while (size > 0) {
        size--;
        unsigned char byte
                = (unsigned char)lra_generate_uint(generator, UCHAR_MAX);
        lra_mix_bytes(seeder, &byte, 1);
    }
}

static char value_to_hexadecimal_char(unsigned char value) {
    assert(value < 16);
    if (value < 10) {
        return '0' + value;
    } else {
        return 'a' + value - 10;
    }
}

static unsigned char value_from_hexadecimal_char(char ch) {
    if (ch < 'a') {
        return ch - '0';
    } else {
        return ch - 'a' + 10;
    }
}

static bool is_hexadecimal_char(char ch) {
    return (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f');
}

static void byte_to_hexadecimal_str(unsigned char byte, char result[3]) {
    result[0] = value_to_hexadecimal_char(byte / 16);
    result[1] = value_to_hexadecimal_char(byte % 16);
    result[2] = '\0';
}

static unsigned char byte_from_hexadecimal_str(const char *str) {
    return value_from_hexadecimal_char(str[0]) * 16
         + value_from_hexadecimal_char(str[1]);
}

static char find_non_hexadecimal_char(const char *str) {
    const char *cp = str;
    while (*cp != '\0' && is_hexadecimal_char(*cp)) {
        ++cp;
    }
    return *cp;
}

static void respect_env_seed(lra_seeder_t *seeder) {
    const struct lra_system *sys = &seeder->generator->sys;

    const char *value = sys->getenv(SEED_ENV_NAME);
    if (value == NULL) {
        return;
    }

    size_t seed_size = seeder_seed_size(seeder);
    size_t len = strlen(value);
    size_t required_len = 2 * seed_size;
    if (len != required_len) {
        fprintf(sys->testing_out,
                "warning: "
                "%s is %zu characters, but should have been %zu\n",
                SEED_ENV_NAME,
                len,
                required_len);
        return;
    }

    char invalid_char = find_non_hexadecimal_char(value);
    if (invalid_char != '\0') {
        fprintf(sys->testing_out,
                "warning: %s contains invalid character '%c'\n",
                SEED_ENV_NAME,
                invalid_char);
        return;
    }

    for (size_t i = 0; i < seed_size; ++i) {
        seeder->seed[i] = byte_from_hexadecimal_str(value + 2 * i);
    }
}

static void print_seed(const lra_seeder_t *seeder) {
    size_t seed_size = seeder_seed_size(seeder);
    FILE *out = seeder->generator->sys.testing_out;

    fputs("export ", out);
    fputs(SEED_ENV_NAME, out);
    fputc('=', out);
    for (size_t i = 0; i < seed_size; ++i) {
        char str[3];
        byte_to_hexadecimal_str(seeder->seed[i], str);
        fputs(str, out);
    }
    fputc('\n', out);
}

static bool is_testing_mode_enabled(const lra_generator_t *generator) {
    return ecb_expect_false(generator->sys.testing_out != NULL);
}

static void seed_silently(lra_seeder_t *seeder) {
    lra_generator_t *generator = seeder->generator;
    generator->impl.seed(generator->state, seeder->seed);

    /* Zeroing out the seed prevents it from being compromised through the
     * dynamic memory allocator. */
    zero_seed(seeder);

    /* Although the rest of the seeder is not as confidential as the seed,
     * zeroing it out does not hurt. */
    memset(seeder, 0, sizeof(*seeder));
    generator->impl.flags &= ~SEEDING_FLAG;
    dima_free(generator->sys.allocator, seeder);
}

void lra_seed(lra_seeder_t *seeder) {
    if (is_testing_mode_enabled(seeder->generator)) {
        respect_env_seed(seeder);
        print_seed(seeder);
    }
    seed_silently(seeder);
}

lra_generator_t *lra_fork_generator(lra_generator_t *generator,
                                    struct lra_error *error) {
    panic_check(lra_generator_is_reentrant(generator));

    lra_generator_t *fork
            = lra_new_generator_impl(&generator->sys, &generator->impl, error);
    if (ecb_expect_false(fork == NULL)) {
        return NULL;
    }

    lra_seeder_t *seeder = lra_new_seeder(fork);
    if (ecb_expect_false(seeder == NULL)) {
        lra_destroy_generator(fork);
        error_no_memory(error, &generator->impl);
        return NULL;
    }

    lra_mix_generator(seeder, generator);
    if (!is_testing_mode_enabled(fork)) {
        lra_mix_default(seeder);
    }
    seed_silently(seeder);

    return fork;
}

struct uint32_generator {
    /* These are on the stack to tell the compiler that the members do not
     * change as a result of a call to generate. */
    uint32_t max;
    uint32_t (*generate)(void *);
    void *state;
};

static void init_uint32_generator(struct uint32_generator *g,
                                  lra_generator_t *src) {
    g->max = src->impl.max;
    g->generate = src->impl.generate;
    g->state = src->state;
}

static uint32_t generate_raw_uint32(const struct uint32_generator *g) {
    uint32_t result = g->generate(g->state);
    panic_check(result <= g->max);
    return result;
}

static uint32_t generate_cutoff_uint32(const struct uint32_generator *g,
                                       uint32_t limit) {
    assert(limit <= g->max);
    uint32_t res;
    do {
        res = generate_raw_uint32(g);
    } while (ecb_expect_false(res > limit));
    return res;
}

static uint32_t generate_maybe_too_large_uint32(
        const struct uint32_generator *g,
        uint32_t max) {
    assert(g->max < UINT32_MAX);
    uint32_t g_max_incr = g->max + 1;

    uint32_t result = 0;
    uint32_t multiplier = 1;
    while (max >= g_max_incr) {
        result += multiplier * generate_raw_uint32(g);
        multiplier *= g_max_incr;
        max /= g_max_incr;
    }

    uint32_t m = max + 1;
    uint32_t limit = g->max - g_max_incr % m;
    return result + multiplier * (generate_cutoff_uint32(g, limit) % m);
}

static uint32_t generate_composed_uint32(const struct uint32_generator *g,
                                         uint32_t max) {
    assert(g->max < UINT32_MAX);

    uint32_t result;
    do {
        result = generate_maybe_too_large_uint32(g, max);
    } while (ecb_expect_false(result > max));
    return result;
}

static uint32_t generate_plain_uint32(const struct uint32_generator *g,
                                      uint32_t max) {
    assert(g->max == UINT32_MAX);
    assert(max < UINT32_MAX);

    uint32_t m = max + 1;
    uint32_t limit = UINT32_MAX - (UINT32_MAX % m + 1) % m;
    return generate_cutoff_uint32(g, limit) % m;
}

/* This function is expected to be called much more often than any other
 * function in the library, and is therefore marked as hot. */
ecb_hot uint32_t lra_generate_uint32(lra_generator_t *generator, uint32_t max) {
    struct uint32_generator g;
    init_uint32_generator(&g, generator);

    if (g.max != UINT32_MAX) {
        return generate_composed_uint32(&g, max);
    } else if (max != UINT32_MAX) {
        return generate_plain_uint32(&g, max);
    } else {
        return generate_raw_uint32(&g);
    }
}

unsigned lra_generate_uint(lra_generator_t *generator, unsigned max) {
#if SIZEOF_INT <= 4
    return (unsigned)lra_generate_uint32(generator, (uint32_t)max);
#else
#error "" \
    "Loranga currently only works on systems where int is at most 32 bits. " \
    "Submit an issue or merge request to have this fixed."
#endif
}

int lra_no_init(void *state) {
    (void)state;
    return 0;
}

void lra_no_deinit(void *state) {
    (void)state;
}

bool lra_generator_is_reentrant(const lra_generator_t *generator) {
    return !!(generator->impl.flags & LRA_REENTRANT);
}

const char *lra_generator_name(const lra_generator_t *generator) {
    return generator->impl.name;
}
