/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "lrand48_impl.h"

#define LRAND48_MAX ((1u << 31) - 1)

static void lrand48_seed(ecb_unused void *state, const unsigned char *seed) {
    unsigned short dest[3];
    memcpy(&dest, seed, sizeof(dest));
    seed48(dest);
}

static uint32_t lrand48_generate(ecb_unused void *state) {
    return lrand48();
}

const struct lra_implementation lra_lrand48_impl = {
        "lrand48",
        0,
        3 * sizeof(unsigned short),
        LRAND48_MAX,
        0,
        lra_no_init,
        lra_no_deinit,
        lrand48_seed,
        lrand48_generate,
};
