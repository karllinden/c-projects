/*
 * Copyright (C) 2022-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include <dima/system.h>
#include <panic.h>

#include <loranga/simple.h>

#include "impls.h"
#include "internal.h"

#define PUBLIC_FLAGS (LRA_REENTRANT)

struct impl_reqs {
    const char *name;
    lra_flags_t flags;
};

struct lra_config_s {
    struct lra_system sys;
    struct impl_reqs reqs;
    struct lra_implementation impl;
};

const char *lra_standard_getenv(const char *name) {
    return getenv(name);
}

const char *lra_secure_getenv(const char *name) {
#if HAVE_SECURE_GETENV
    return secure_getenv(name);
#else /* !HAVE_SECURE_GETENV */
    return getenv(name);
#endif /* !HAVE_SECURE_GETENV */
}

const char *lra_no_getenv(const char *name) {
    (void)name;
    return NULL;
}

static void init_system(struct lra_system *sys, dima_t *allocator) {
    sys->allocator = allocator;
    sys->testing_out = NULL;
    sys->getenv = lra_secure_getenv;
}

static void init_config(lra_config_t *config, dima_t *allocator) {
    init_system(&config->sys, allocator);
    config->reqs.name = NULL;
    config->reqs.flags = 0;
    memset(&config->impl, 0, sizeof(config->impl));
}

lra_config_t *lra_new_config(dima_t *allocator) {
    lra_config_t *config = dima_alloc(allocator, sizeof(*config));
    if (ecb_expect_false(config == NULL)) {
        return NULL;
    }

    init_config(config, allocator);
    dima_ref(allocator);
    return config;
}

void lra_destroy_config(lra_config_t *config) {
    if (config == NULL) {
        return;
    }

    dima_t *allocator = config->sys.allocator;
    dima_free(allocator, config);
    dima_unref(allocator);
}

static void check_flags(lra_flags_t flags) {
    if (flags & ~PUBLIC_FLAGS) {
        panic("Invalid flags `%#.8" PRIx32 "'.", flags);
    }
}

void lra_set_flags(lra_config_t *config, lra_flags_t flags) {
    check_flags(flags);
    config->reqs.flags = flags;
}

void lra_set_implementation_name(lra_config_t *config, const char *name) {
    panic_check(name != NULL);
    config->reqs.name = name;
}

static void check_implementation(const struct lra_implementation *impl) {
    panic_check(impl->name != NULL);
    panic_check(impl->seed_size > 0);
    panic_check(impl->max > 0);
    check_flags(impl->flags);
    panic_check(impl->init != NULL);
    panic_check(impl->deinit != NULL);
    panic_check(impl->seed != NULL);
    panic_check(impl->generate != NULL);
}

void lra_set_implementation(lra_config_t *config,
                            const struct lra_implementation *impl) {
    panic_check(impl != NULL);
    memcpy(&config->impl, impl, sizeof(*impl));
    check_implementation(&config->impl);
}

void lra_enable_testing(lra_config_t *config, FILE *out) {
    panic_check(out != NULL);
    config->sys.testing_out = out;
}

void lra_set_getenv(lra_config_t *config, lra_getenv_fn *getenv) {
    panic_check(getenv != NULL);
    config->sys.getenv = getenv;
}

static bool impl_fulfills_reqs(const struct lra_implementation *impl,
                               const struct impl_reqs *reqs) {
    return (impl->flags & reqs->flags) == reqs->flags
        && (reqs->name == NULL || strcmp(reqs->name, impl->name) == 0);
}

static const struct lra_implementation *get_selected_impl(
        const lra_config_t *config) {
    if (impl_fulfills_reqs(&config->impl, &config->reqs)) {
        return &config->impl;
    } else {
        return NULL;
    }
}

static const struct lra_implementation *find_built_in_impl(
        const struct impl_reqs *reqs) {
    for (int i = 0; i < N_IMPLS; ++i) {
        const struct lra_implementation *impl = lra_impls[i];
        if (impl_fulfills_reqs(impl, reqs)) {
            return impl;
        }
    }
    return NULL;
}

static void copy_and_enrich_reqs(struct impl_reqs *dest,
                                 const lra_config_t *config) {
    memcpy(dest, &config->reqs, sizeof(*dest));
    if (dest->name == NULL) {
        dest->name = config->sys.getenv("LORANGA_IMPLEMENTATION");
    }
}

static const struct lra_implementation *find_impl(const lra_config_t *config) {
    if (config->impl.generate != NULL) {
        return get_selected_impl(config);
    }

    struct impl_reqs reqs;
    copy_and_enrich_reqs(&reqs, config);
    return find_built_in_impl(&reqs);
}

static void print_implementation(FILE *out,
                                 const struct lra_implementation *impl) {
    if (out != NULL) {
        fprintf(out, "export LORANGA_IMPLEMENTATION=%s\n", impl->name);
    }
}

lra_generator_t *lra_new_unseeded_generator(const lra_config_t *config,
                                            struct lra_error *error) {
    const struct lra_implementation *impl = find_impl(config);
    if (ecb_expect_false(impl == NULL)) {
        error->code = LRA_NOT_IMPLEMENTED;
        return NULL;
    }

    FILE *testing_out = config->sys.testing_out;
    print_implementation(testing_out, impl);
    return lra_new_generator_impl(&config->sys, impl, error);
}

lra_generator_t *lra_new_generator(const lra_config_t *config,
                                   struct lra_error *error) {
    lra_generator_t *generator = lra_new_unseeded_generator(config, error);
    if (ecb_expect_false(generator == NULL)) {
        return NULL;
    }

    lra_seeder_t *seeder = lra_new_seeder(generator);
    if (ecb_expect_false(seeder == NULL)) {
        error->code = LRA_NO_MEMORY;
        error->implementation_name = lra_generator_name(generator);
        lra_destroy_generator(generator);
        return NULL;
    }

    lra_mix_default(seeder);

    int dev_random_failed = lra_mix_dev_random(seeder);
    if (ecb_expect_false(dev_random_failed)) {
        error->code = LRA_DEV_RANDOM_FAILED;
        error->implementation_name = lra_generator_name(generator);
        lra_destroy_generator(generator);
        return NULL;
    }

    lra_seed(seeder);
    return generator;
}

static void init_simple_config(lra_config_t *config, lra_flags_t flags) {
    init_config(config, dima_system_instance());
    lra_set_flags(config, flags);
}

lra_generator_t *lra_new_application_generator(lra_flags_t flags,
                                               struct lra_error *error) {
    lra_config_t config;
    init_simple_config(&config, flags);
    return lra_new_generator(&config, error);
}

lra_generator_t *lra_new_testing_generator(lra_flags_t flags,
                                           struct lra_error *error) {
    lra_config_t config;
    init_simple_config(&config, flags);
    lra_enable_testing(&config, stderr);
    return lra_new_generator(&config, error);
}
