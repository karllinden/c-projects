/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "random_impl.h"

static void random_seed(ecb_unused void *state, const unsigned char *seed) {
    unsigned int dest;
    memcpy(&dest, seed, sizeof(dest));
    srandom(dest);
}

static uint32_t random_generate(ecb_unused void *state) {
    return random();
}

const struct lra_implementation lra_random_impl = {
        "random",
        0,
        sizeof(unsigned int),
        RAND_MAX,
        0,
        lra_no_init,
        lra_no_deinit,
        random_seed,
        random_generate,
};
