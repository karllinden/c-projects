/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include "lrand48_r_impl.h"

#define LRAND48_R_MAX ((1u << 31) - 1)

static int lrand48_r_init(void *state) {
    struct drand48_data *buffer = state;
    memset(buffer, 0, sizeof(*buffer));
    return 0;
}

static void lrand48_r_seed(void *state, const unsigned char *seed) {
    struct drand48_data *buffer = state;
    unsigned short seed16v[3];
    memcpy(&seed16v, seed, sizeof(seed16v));
    seed48_r(seed16v, buffer);
}

static uint32_t lrand48_r_generate(void *state) {
    struct drand48_data *buffer = state;
    long result;
    lrand48_r(buffer, &result);
    return (uint32_t)result;
}

const struct lra_implementation lra_lrand48_r_impl = {
        "lrand48_r",
        sizeof(struct drand48_data),
        3 * sizeof(unsigned short),
        LRAND48_R_MAX,
        LRA_REENTRANT,
        lrand48_r_init,
        lra_no_deinit,
        lrand48_r_seed,
        lrand48_r_generate,
};
