/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERNAL_H
#define INTERNAL_H

#include <loranga/advanced.h>

struct lra_system {
    dima_t *allocator;
    FILE *testing_out;
    lra_getenv_fn *getenv;
};

lra_generator_t *lra_new_generator_impl(
        const struct lra_system *sys,
        const struct lra_implementation *implementation,
        struct lra_error *error);

#endif /* !INTERNAL_H */
