/*
 * Copyright (C) 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ecb.h>

#include <panic.h>

#include <loranga/allocator.h>

struct lra_allocator_s {
    lra_generator_t *generator;
    unsigned failure_percentage;
};

static int should_succeed(const lra_allocator_t *allocator) {
    unsigned r = lra_generate_uint(allocator->generator, 99);
    return r >= allocator->failure_percentage;
}

static void *invoke_allocator(dima_t *next,
                              void *userdata,
                              const struct dima_invocation *invocation) {
    lra_allocator_t *allocator = userdata;
    if (invocation->function == DIMA_FREE || should_succeed(allocator)) {
        return dima_invoke(next, invocation);
    } else {
        return NULL;
    }
}

lra_allocator_t *lra_new_allocator(dima_t *next, lra_generator_t *generator) {
    unsigned flags = dima_flags(next);
    flags &= ~DIMA_EXITS_ON_FAILURE;
    flags &= ~DIMA_IS_THREAD_SAFE;
    if (!lra_generator_is_reentrant(generator)) {
        flags |= DIMA_IS_THREAD_HOSTILE;
    }

    lra_allocator_t *allocator
            = dima_new_proxy(next, invoke_allocator, flags, sizeof(*allocator));
    if (ecb_expect_false(allocator == NULL)) {
        return NULL;
    }

    allocator->generator = generator;
    allocator->failure_percentage = 0;
    return allocator;
}

void lra_unref_allocator(lra_allocator_t *allocator) {
    dima_unref_proxy(allocator);
}

dima_t *lra_allocator_to_dima(lra_allocator_t *allocator) {
    return dima_from_proxy(allocator);
}

void lra_set_allocator_failure_percentage(lra_allocator_t *allocator,
                                          unsigned failure_percentage) {
    panic_check(failure_percentage <= 100);
    allocator->failure_percentage = failure_percentage;
}
