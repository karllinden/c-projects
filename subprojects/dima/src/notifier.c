/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdalign.h>
#include <stddef.h>

#include <ecb.h>
#include <panic.h>

#include <dima/notifier.h>
#include <dima/proxy.h>

struct dima_notifier_s {
    dima_listener_t *first_listener;
    dima_listener_t **next_listener;
};

struct dima_listener_s {
    dima_notifier_t *notifier;
    dima_listener_t *prev;
    dima_listener_t *next;
    dima_notify_fn *notify;
    alignas(max_align_t) char userdata[];
};

static void notify_listeners(dima_notifier_t *notifier,
                             const struct dima_invocation *invocation,
                             void *result) {
    if (notifier->next_listener != NULL) {
        panic("A listener invoked its notifier.");
    }

    dima_listener_t *next_listener = notifier->first_listener;
    notifier->next_listener = &next_listener;
    while (next_listener != NULL) {
        dima_listener_t *listener = next_listener;
        next_listener = listener->next;
        listener->notify(listener, invocation, result);
    }
    notifier->next_listener = NULL;
}

static void *invoke_notifier(dima_t *next,
                             void *userdata,
                             const struct dima_invocation *invocation) {
    dima_notifier_t *notifier = userdata;
    void *result = dima_invoke(next, invocation);
    notify_listeners(notifier, invocation, result);
    return result;
}

static void deinit_notifier(void *userdata) {
    dima_notifier_t *notifier = userdata;

    /* Because each listener holds a reference to the notifier, the check only
     * fails if there is a dima_unref on the notifier that should have been from
     * dima_destroy_listener. */
    panic_check(notifier->first_listener == NULL);
}

dima_notifier_t *dima_new_notifier(dima_t *next) {
    dima_notifier_t *notifier = dima_new_proxy(
            next, invoke_notifier, dima_flags(next), sizeof(*notifier));
    if (ecb_expect_false(notifier == NULL)) {
        return NULL;
    }

    dima_set_deinit_proxy(notifier, deinit_notifier);
    notifier->first_listener = NULL;
    notifier->next_listener = NULL;
    return notifier;
}

void dima_unref_notifier(dima_notifier_t *notifier) {
    dima_unref_proxy(notifier);
}

dima_t *dima_from_notifier(dima_notifier_t *notifier) {
    return dima_from_proxy(notifier);
}

static void link_listener(dima_listener_t *listener) {
    dima_notifier_t *notifier = listener->notifier;
    dima_listener_t *next = notifier->first_listener;
    listener->prev = NULL;
    listener->next = next;
    notifier->first_listener = listener;

    if (next != NULL) {
        next->prev = listener;
    }
}

static void ref_notifier(dima_notifier_t *notifier) {
    dima_ref(dima_from_notifier(notifier));
}

dima_listener_t *dima_new_listener(dima_notifier_t *notifier,
                                   dima_notify_fn *notify,
                                   size_t userdata_size) {
    panic_check(notify != NULL);

    dima_t *next = dima_proxy_next(notifier);
    dima_listener_t *listener
            = dima_alloc(next, sizeof(*listener) + userdata_size);
    if (ecb_expect_false(listener == NULL)) {
        return NULL;
    }

    ref_notifier(notifier);
    listener->notifier = notifier;
    listener->notify = notify;
    link_listener(listener);
    return listener;
}

static void unlink_listener(dima_listener_t *listener) {
    dima_notifier_t *notifier = listener->notifier;
    dima_listener_t *prev = listener->prev;
    dima_listener_t *next = listener->next;

    if (prev != NULL) {
        prev->next = next;
    } else {
        notifier->first_listener = next;
    }

    if (next != NULL) {
        next->prev = prev;
    }

    if (ecb_expect_false(notifier->next_listener != NULL
                         && listener == *notifier->next_listener)) {
        *notifier->next_listener = next;
    }
}

void dima_destroy_listener(dima_listener_t *listener) {
    if (listener == NULL) {
        return;
    }

    unlink_listener(listener);

    dima_notifier_t *notifier = listener->notifier;
    dima_t *next = dima_proxy_next(notifier);
    dima_free(next, listener);
    dima_unref_notifier(notifier);
}

void *dima_listener_userdata(dima_listener_t *listener) {
    return listener->userdata;
}
