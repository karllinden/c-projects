/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <limits.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ecb.h>
#include <panic.h>

#include <dima/adapter.h>
#include <dima/failure_hook.h>
#include <dima/proxy.h>
#include <dima/system.h>

#include "mutex.h"
#include "size_bits.h"

#define SQRT_SIZE_MAX (((size_t)1) << (SIZE_BITS / 2))

enum dima_type {
    DIMA_SYSTEM,
    DIMA_ADAPTER,
    DIMA_FAILURE_HOOK,
    DIMA_PROXY,
};

struct dima_s {
    enum dima_type type;
    unsigned flags;
    pthread_mutex_t mutex; /* protecting ref */
    unsigned long ref;
};

struct dima_adapter_s {
    dima_t dima;
    void *userptr;
    dima_adapter_free_fn *free;
    dima_adapter_realloc_fn *realloc;
};

struct dima_decorator {
    dima_t dima;
    dima_t *next;
};

struct dima_failure_hook {
    struct dima_decorator decorator;
    dima_on_failure_fn *on_failure;
    alignas(max_align_t) char userdata[];
};

struct dima_proxy {
    struct dima_decorator decorator;
    dima_invoke_fn *invoke;
    dima_deinit_fn *deinit;
    alignas(max_align_t) char userdata[];
};

static void *invoke_proxy(dima_t *dima,
                          const struct dima_invocation *invocation) {
    struct dima_proxy *proxy = (struct dima_proxy *)dima;

    /* The assignment is to shut up cppcheck. */
    void *result
            = proxy->invoke(proxy->decorator.next, proxy->userdata, invocation);
    return result;
}

static void *invoke_non_proxy(dima_t *dima, const struct dima_invocation *inv) {
    switch (inv->function) {
        case DIMA_FREE:
            dima_free(dima, inv->ptr);
            return NULL;
        case DIMA_ALLOC:
            return dima_alloc(dima, inv->size);
        case DIMA_ALLOC0:
            return dima_alloc0(dima, inv->size);
        case DIMA_REALLOC:
            return dima_realloc(dima, inv->ptr, inv->size);
        case DIMA_ALLOC_ARRAY:
            return dima_alloc_array(dima, inv->nmemb, inv->size);
        case DIMA_ALLOC_ARRAY0:
            return dima_alloc_array0(dima, inv->nmemb, inv->size);
        case DIMA_REALLOC_ARRAY:
            return dima_realloc_array(dima, inv->ptr, inv->nmemb, inv->size);
        case DIMA_STRDUP:
            return dima_strdup(dima, inv->s);
        case DIMA_STRNDUP:
            return dima_strndup(dima, inv->s, inv->n);
    }

    panic("Invalid function %d in invocation.", inv->function);
}

void *dima_invoke(dima_t *dima, const struct dima_invocation *inv) {
    if (dima->type == DIMA_PROXY) {
        return invoke_proxy(dima, inv);
    } else {
        return invoke_non_proxy(dima, inv);
    }
}

static inline void init_invocation(struct dima_invocation *invocation) {
    /* Because memcmpy is used for dima_compare_invocations any padding bits
     * need to be zero-initialized. */
    memset(invocation, 0, sizeof(*invocation));
}

void dima_init_free_invocation(struct dima_invocation *invocation, void *ptr) {
    init_invocation(invocation);
    invocation->function = DIMA_FREE;
    invocation->ptr = ptr;
}

void dima_init_alloc_invocation(struct dima_invocation *invocation,
                                size_t size) {
    init_invocation(invocation);
    invocation->function = DIMA_ALLOC;
    invocation->size = size;
}

void dima_init_alloc0_invocation(struct dima_invocation *invocation,
                                 size_t size) {
    init_invocation(invocation);
    invocation->function = DIMA_ALLOC0;
    invocation->size = size;
}

void dima_init_realloc_invocation(struct dima_invocation *invocation,
                                  void *ptr,
                                  size_t size) {
    init_invocation(invocation);
    invocation->function = DIMA_REALLOC;
    invocation->ptr = ptr;
    invocation->size = size;
}

void dima_init_alloc_array_invocation(struct dima_invocation *invocation,
                                      size_t nmemb,
                                      size_t size) {
    init_invocation(invocation);
    invocation->function = DIMA_ALLOC_ARRAY;
    invocation->nmemb = nmemb;
    invocation->size = size;
}

void dima_init_alloc_array0_invocation(struct dima_invocation *invocation,
                                       size_t nmemb,
                                       size_t size) {
    init_invocation(invocation);
    invocation->function = DIMA_ALLOC_ARRAY0;
    invocation->nmemb = nmemb;
    invocation->size = size;
}

void dima_init_realloc_array_invocation(struct dima_invocation *invocation,
                                        void *ptr,
                                        size_t nmemb,
                                        size_t size) {
    init_invocation(invocation);
    invocation->function = DIMA_REALLOC_ARRAY;
    invocation->ptr = ptr;
    invocation->nmemb = nmemb;
    invocation->size = size;
}

void dima_init_strdup_invocation(struct dima_invocation *invocation,
                                 const char *s) {
    init_invocation(invocation);
    invocation->function = DIMA_STRDUP;
    invocation->s = s;
}

void dima_init_strndup_invocation(struct dima_invocation *invocation,
                                  const char *s,
                                  size_t n) {
    init_invocation(invocation);
    invocation->function = DIMA_STRNDUP;
    invocation->s = s;
    invocation->n = n;
}

int dima_compare_invocations(const struct dima_invocation *a,
                             const struct dima_invocation *b) {
    return memcmp(a, b, sizeof(*a));
}

void dima_copy_invocation(struct dima_invocation *dest,
                          const struct dima_invocation *src) {
    memcpy(dest, src, sizeof(*dest));
}

static inline int array_size_overflows(size_t nmemb, size_t size) {
    size_t bytes = nmemb * size;
    return (nmemb >= SQRT_SIZE_MAX || size >= SQRT_SIZE_MAX) //
        && nmemb > 0 //
        && (bytes / nmemb != size);
}

static inline size_t min_size(size_t a, size_t b) {
    return a < b ? a : b;
}

static void *alloc0_with_alloc(dima_t *dima, size_t size) {
    void *ptr = dima_alloc(dima, size);
    if (ptr != NULL) {
        memset(ptr, 0, size);
    }
    return ptr;
}

static void *alloc_array_with_alloc(dima_t *dima, size_t nmemb, size_t size) {
    if (array_size_overflows(nmemb, size)) {
        return NULL;
    }
    return dima_alloc(dima, nmemb * size);
}

static void *alloc_array0_with_alloc_array(dima_t *dima,
                                           size_t nmemb,
                                           size_t size) {
    void *ptr = dima_alloc_array(dima, nmemb, size);
    if (ptr != NULL) {
        memset(ptr, 0, nmemb * size);
    }
    return ptr;
}

static void *realloc_array_with_realloc(dima_t *dima,
                                        void *ptr,
                                        size_t nmemb,
                                        size_t size) {
    if (array_size_overflows(nmemb, size)) {
        return NULL;
    }
    return dima_realloc(dima, ptr, nmemb * size);
}

static char *strdup_with_alloc(dima_t *dima, const char *s) {
    size_t size = strlen(s) + 1;
    char *dup = dima_alloc(dima, size);
    if (dup != NULL) {
        memcpy(dup, s, size);
    }
    return dup;
}

static char *strndup_with_alloc(dima_t *dima, const char *s, size_t n) {
    size_t len = min_size(strlen(s), n);
    char *dup = dima_alloc(dima, len + 1);
    if (dup != NULL) {
        memcpy(dup, s, len);
        dup[len] = '\0';
    }
    return dup;
}

#if HAVE_REALLOCARRAY
static void *system_reallocarray(ecb_unused dima_t *dima,
                                 void *ptr,
                                 size_t nmemb,
                                 size_t size) {
    return reallocarray(ptr, nmemb, size);
}
#else
#define system_reallocarray realloc_array_with_realloc
#endif

#if HAVE_STRDUP
static char *system_strdup(ecb_unused dima_t *dima, const char *s) {
    return strdup(s);
}
#else
#define system_strdup strdup_with_alloc
#endif

#if HAVE_STRNDUP
static char *system_strndup(ecb_unused dima_t *dima, const char *s, size_t n) {
    return strndup(s, n);
}
#else
#define system_strndup strndup_with_alloc
#endif

static dima_t system_instance = {
        DIMA_SYSTEM,
        DIMA_IS_THREAD_SAFE,
        PTHREAD_MUTEX_INITIALIZER,
        1,
};

static int init_dima(dima_t *dima, enum dima_type type, unsigned flags) {
    dima->type = type;
    dima->flags = flags;
    dima->ref = 1;
    return dima_pthread_mutex_init(&dima->mutex, NULL);
}

static void deinit_dima(dima_t *dima) {
    dima_pthread_mutex_destroy(&dima->mutex);
}

static void adapter_free(dima_adapter_t *adapter, void *ptr) {
    adapter->free(adapter->userptr, ptr);
}

static void *adapter_realloc(dima_adapter_t *adapter, void *ptr, size_t size) {
    return adapter->realloc(adapter->userptr, ptr, size);
}

static void *adapter_alloc(dima_adapter_t *adapter, size_t size) {
    return adapter_realloc(adapter, NULL, size);
}

static void destroy_adapter(dima_adapter_t *adapter) {
    adapter_free(adapter, adapter);
}

dima_adapter_t *dima_new_adapter(void *userptr,
                                 dima_adapter_free_fn *adapter_free,
                                 dima_adapter_realloc_fn *adapter_realloc,
                                 unsigned flags) {
    dima_adapter_t *adapter = adapter_realloc(userptr, NULL, sizeof(*adapter));
    if (ecb_expect_false(adapter == NULL)) {
        return NULL;
    }

    int init_failed = init_dima(&adapter->dima, DIMA_ADAPTER, flags);
    if (ecb_expect_false(init_failed)) {
        adapter_free(userptr, adapter);
        return NULL;
    }

    adapter->userptr = userptr;
    adapter->free = adapter_free;
    adapter->realloc = adapter_realloc;
    return adapter;
}

void dima_unref_adapter(dima_adapter_t *adapter) {
    dima_unref(dima_from_adapter(adapter));
}

dima_t *dima_from_adapter(dima_adapter_t *adapter) {
    return (dima_t *)adapter;
}

static void *new_decorator(dima_t *next,
                           size_t size,
                           enum dima_type type,
                           unsigned flags) {
    struct dima_decorator *decorator = dima_alloc(next, size);
    if (ecb_expect_false(decorator == NULL)) {
        return NULL;
    }

    int init_failed = init_dima(&decorator->dima, type, flags);
    if (ecb_expect_false(init_failed)) {
        dima_free(next, decorator);
        return NULL;
    }

    dima_ref(next);
    decorator->next = next;
    return decorator;
}

static void destroy_decorator(struct dima_decorator *decorator) {
    dima_t *next = decorator->next;
    dima_free(next, decorator);
    dima_unref(next);
}

static void failure_hook_free(struct dima_failure_hook *hook, void *ptr) {
    dima_free(hook->decorator.next, ptr);
}

static void *failure_hook_handle(struct dima_failure_hook *hook, void *result) {
    if (ecb_expect_false(result == NULL)) {
        hook->on_failure(hook->userdata);
    }
    return result;
}
#define FAILURE_HOOK_INVOKE(function, ...) \
    failure_hook_handle(hook, \
                        dima_##function(hook->decorator.next, __VA_ARGS__))

static void *failure_hook_alloc(struct dima_failure_hook *hook, size_t size) {
    return FAILURE_HOOK_INVOKE(alloc, size);
}

static void *failure_hook_alloc0(struct dima_failure_hook *hook, size_t size) {
    return FAILURE_HOOK_INVOKE(alloc0, size);
}

static void *failure_hook_realloc(struct dima_failure_hook *hook,
                                  void *ptr,
                                  size_t size) {
    return FAILURE_HOOK_INVOKE(realloc, ptr, size);
}

static void *failure_hook_alloc_array(struct dima_failure_hook *hook,
                                      size_t nmemb,
                                      size_t size) {
    return FAILURE_HOOK_INVOKE(alloc_array, nmemb, size);
}

static void *failure_hook_alloc_array0(struct dima_failure_hook *hook,
                                       size_t nmemb,
                                       size_t size) {
    return FAILURE_HOOK_INVOKE(alloc_array0, nmemb, size);
}

static void *failure_hook_realloc_array(struct dima_failure_hook *hook,
                                        void *ptr,
                                        size_t nmemb,
                                        size_t size) {
    return FAILURE_HOOK_INVOKE(realloc_array, ptr, nmemb, size);
}

static void *failure_hook_strdup(struct dima_failure_hook *hook,
                                 const char *s) {
    return FAILURE_HOOK_INVOKE(strdup, s);
}

static void *failure_hook_strndup(struct dima_failure_hook *hook,
                                  const char *s,
                                  size_t n) {
    return FAILURE_HOOK_INVOKE(strndup, s, n);
}
#undef FAILURE_HOOK_INVOKE

void *dima_new_failure_hook(dima_t *next,
                            dima_on_failure_fn *on_failure,
                            unsigned flags,
                            size_t size) {
    struct dima_failure_hook *hook = new_decorator(
            next, sizeof(*hook) + size, DIMA_FAILURE_HOOK, flags);
    if (ecb_expect_false(hook == NULL)) {
        return NULL;
    }

    hook->on_failure = on_failure;
    return hook->userdata;
}

dima_t *dima_from_failure_hook(void *userdata) {
    size_t offset = offsetof(struct dima_failure_hook, userdata);
    char *addr = ((char *)userdata) - offset;
    return (dima_t *)addr;
}

dima_t *dima_system_instance(void) {
    return &system_instance;
}

static void proxy_free(dima_t *dima, void *ptr) {
    struct dima_invocation inv;
    dima_init_free_invocation(&inv, ptr);
    invoke_proxy(dima, &inv);
}

static void *proxy_alloc(dima_t *dima, size_t size) {
    struct dima_invocation inv;
    dima_init_alloc_invocation(&inv, size);
    return invoke_proxy(dima, &inv);
}

static void *proxy_alloc0(dima_t *dima, size_t size) {
    struct dima_invocation inv;
    dima_init_alloc0_invocation(&inv, size);
    return invoke_proxy(dima, &inv);
}

static void *proxy_realloc(dima_t *dima, void *ptr, size_t size) {
    struct dima_invocation inv;
    dima_init_realloc_invocation(&inv, ptr, size);
    return invoke_proxy(dima, &inv);
}

static void *proxy_alloc_array(dima_t *dima, size_t nmemb, size_t size) {
    struct dima_invocation inv;
    dima_init_alloc_array_invocation(&inv, nmemb, size);
    return invoke_proxy(dima, &inv);
}

static void *proxy_alloc_array0(dima_t *dima, size_t nmemb, size_t size) {
    struct dima_invocation inv;
    dima_init_alloc_array0_invocation(&inv, nmemb, size);
    return invoke_proxy(dima, &inv);
}

static void *proxy_realloc_array(dima_t *dima,
                                 void *ptr,
                                 size_t nmemb,
                                 size_t size) {
    struct dima_invocation inv;
    dima_init_realloc_array_invocation(&inv, ptr, nmemb, size);
    return invoke_proxy(dima, &inv);
}

static char *proxy_strdup(dima_t *dima, const char *s) {
    struct dima_invocation inv;
    dima_init_strdup_invocation(&inv, s);
    return invoke_proxy(dima, &inv);
}

static char *proxy_strndup(dima_t *dima, const char *s, size_t n) {
    struct dima_invocation inv;
    dima_init_strndup_invocation(&inv, s, n);
    return invoke_proxy(dima, &inv);
}

static void destroy_proxy(struct dima_proxy *proxy) {
    dima_deinit_fn *deinit = proxy->deinit;
    if (deinit != NULL) {
        deinit(proxy->userdata);
    }

    destroy_decorator(&proxy->decorator);
}

static struct dima_proxy *proxy_from_userdata(void *userdata) {
    size_t offset = offsetof(struct dima_proxy, userdata);
    char *addr = ((char *)userdata) - offset;
    return (struct dima_proxy *)addr;
}

void *dima_new_proxy(dima_t *next,
                     dima_invoke_fn *invoke,
                     unsigned flags,
                     size_t size) {
    struct dima_proxy *proxy
            = new_decorator(next, sizeof(*proxy) + size, DIMA_PROXY, flags);
    if (ecb_expect_false(proxy == NULL)) {
        return NULL;
    }

    proxy->invoke = invoke;
    proxy->deinit = NULL;
    return proxy->userdata;
}

void dima_unref_proxy(void *userdata) {
    if (userdata != NULL) {
        dima_unref(dima_from_proxy(userdata));
    }
}

dima_t *dima_from_proxy(void *userdata) {
    return (dima_t *)proxy_from_userdata(userdata);
}

dima_t *dima_proxy_next(void *userdata) {
    return proxy_from_userdata(userdata)->decorator.next;
}

void dima_set_deinit_proxy(void *userdata, dima_deinit_fn *deinit) {
    struct dima_proxy *proxy = proxy_from_userdata(userdata);
    proxy->deinit = deinit;
}

static void destroy_dima(dima_t *dima) {
    deinit_dima(dima);

    switch (dima->type) {
        case DIMA_SYSTEM:
            panic("Attempt to destroy DIMA's system instance.");
        case DIMA_ADAPTER:
            destroy_adapter((dima_adapter_t *)dima);
            break;
        case DIMA_FAILURE_HOOK:
            destroy_decorator((struct dima_decorator *)dima);
            break;
        case DIMA_PROXY:
            destroy_proxy((struct dima_proxy *)dima);
            break;
    }
}

static void lock_dima(dima_t *dima) {
    pthread_mutex_lock(&dima->mutex);
}

static void unlock_dima(dima_t *dima) {
    pthread_mutex_unlock(&dima->mutex);
}

void dima_ref(dima_t *dima) {
    lock_dima(dima);
    dima->ref++;
    panic_check(dima->ref > 0);
    unlock_dima(dima);
}

void dima_unref(dima_t *dima) {
    if (dima == NULL) {
        return;
    }

    lock_dima(dima);
    dima->ref--;
    unsigned long ref = dima->ref;
    unlock_dima(dima);

    if (ref == 0) {
        destroy_dima(dima);
    }
}

ecb_cold ecb_noreturn static void panic_on_type(const dima_t *dima) {
    panic("Invalid dima type %d.", dima->type);
}

void dima_free(dima_t *dima, void *ptr) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            free(ptr);
            return;
        case DIMA_ADAPTER:
            adapter_free((dima_adapter_t *)dima, ptr);
            return;
        case DIMA_FAILURE_HOOK:
            failure_hook_free((struct dima_failure_hook *)dima, ptr);
            return;
        case DIMA_PROXY:
            proxy_free(dima, ptr);
            return;
    }
    panic_on_type(dima);
}

void *dima_alloc(dima_t *dima, size_t size) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            return malloc(size);
        case DIMA_ADAPTER:
            return adapter_alloc((dima_adapter_t *)dima, size);
        case DIMA_FAILURE_HOOK:
            return failure_hook_alloc((struct dima_failure_hook *)dima, size);
        case DIMA_PROXY:
            return proxy_alloc(dima, size);
    }
    panic_on_type(dima);
}

void *dima_alloc0(dima_t *dima, size_t size) {
    switch (dima->type) {
        case DIMA_SYSTEM:
        case DIMA_ADAPTER:
            return alloc0_with_alloc(dima, size);
        case DIMA_FAILURE_HOOK:
            return failure_hook_alloc0((struct dima_failure_hook *)dima, size);
        case DIMA_PROXY:
            return proxy_alloc0(dima, size);
    }
    panic_on_type(dima);
}

void *dima_realloc(dima_t *dima, void *ptr, size_t size) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            return realloc(ptr, size);
        case DIMA_ADAPTER:
            return adapter_realloc((dima_adapter_t *)dima, ptr, size);
        case DIMA_FAILURE_HOOK:
            return failure_hook_realloc(
                    (struct dima_failure_hook *)dima, ptr, size);
        case DIMA_PROXY:
            return proxy_realloc(dima, ptr, size);
    }
    panic_on_type(dima);
}

void *dima_alloc_array(dima_t *dima, size_t nmemb, size_t size) {
    switch (dima->type) {
        case DIMA_SYSTEM:
        case DIMA_ADAPTER:
            return alloc_array_with_alloc(dima, nmemb, size);
        case DIMA_FAILURE_HOOK:
            return failure_hook_alloc_array(
                    (struct dima_failure_hook *)dima, nmemb, size);
        case DIMA_PROXY:
            return proxy_alloc_array(dima, nmemb, size);
    }
    panic_on_type(dima);
}

void *dima_alloc_array0(dima_t *dima, size_t nmemb, size_t size) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            return calloc(nmemb, size);
        case DIMA_ADAPTER:
            return alloc_array0_with_alloc_array(dima, nmemb, size);
        case DIMA_FAILURE_HOOK:
            return failure_hook_alloc_array0(
                    (struct dima_failure_hook *)dima, nmemb, size);
        case DIMA_PROXY:
            return proxy_alloc_array0(dima, nmemb, size);
    }
    panic_on_type(dima);
}

void *dima_realloc_array(dima_t *dima, void *ptr, size_t nmemb, size_t size) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            return system_reallocarray(dima, ptr, nmemb, size);
        case DIMA_ADAPTER:
            return realloc_array_with_realloc(dima, ptr, nmemb, size);
        case DIMA_FAILURE_HOOK:
            return failure_hook_realloc_array(
                    (struct dima_failure_hook *)dima, ptr, nmemb, size);
        case DIMA_PROXY:
            return proxy_realloc_array(dima, ptr, nmemb, size);
    }
    panic_on_type(dima);
}

char *dima_strdup(dima_t *dima, const char *s) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            return system_strdup(dima, s);
        case DIMA_ADAPTER:
            return strdup_with_alloc(dima, s);
        case DIMA_FAILURE_HOOK:
            return failure_hook_strdup((struct dima_failure_hook *)dima, s);
        case DIMA_PROXY:
            return proxy_strdup(dima, s);
    }
    panic_on_type(dima);
}

char *dima_strndup(dima_t *dima, const char *s, size_t n) {
    switch (dima->type) {
        case DIMA_SYSTEM:
            return system_strndup(dima, s, n);
        case DIMA_ADAPTER:
            return strndup_with_alloc(dima, s, n);
        case DIMA_FAILURE_HOOK:
            return failure_hook_strndup((struct dima_failure_hook *)dima, s, n);
        case DIMA_PROXY:
            return proxy_strndup(dima, s, n);
    }
    panic_on_type(dima);
}

unsigned dima_flags(const dima_t *dima) {
    return dima->flags;
}
