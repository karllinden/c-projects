/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>

#include <ecb.h>

#include <dima/proxy.h>
#include <dima/synchronizer.h>

#include "mutex.h"

struct dima_synchronizer_s {
    pthread_mutex_t mutex;
};

static void *invoke_synchronizer(dima_t *next,
                                 void *userdata,
                                 const struct dima_invocation *invocation) {
    dima_synchronizer_t *synchronizer = userdata;
    pthread_mutex_lock(&synchronizer->mutex);
    void *result = dima_invoke(next, invocation);
    pthread_mutex_unlock(&synchronizer->mutex);
    return result;
}

static void deinit_synchronizer(void *userdata) {
    dima_synchronizer_t *synchronizer = userdata;
    dima_pthread_mutex_destroy(&synchronizer->mutex);
}

dima_synchronizer_t *dima_new_synchronizer(dima_t *next) {
    assert(!dima_is_thread_hostile(next));

    unsigned flags = dima_flags(next);
    flags |= DIMA_IS_THREAD_SAFE;

    dima_synchronizer_t *synchronizer = dima_new_proxy(
            next, invoke_synchronizer, flags, sizeof(*synchronizer));
    if (ecb_expect_false(synchronizer == NULL)) {
        return NULL;
    }

    int status = dima_pthread_mutex_init(&synchronizer->mutex, NULL);
    if (ecb_expect_false(status)) {
        dima_unref_proxy(synchronizer);
        return NULL;
    }
    dima_set_deinit_proxy(synchronizer, deinit_synchronizer);

    return synchronizer;
}

void dima_unref_synchronizer(dima_synchronizer_t *synchronizer) {
    dima_unref_proxy(synchronizer);
}

dima_t *dima_from_synchronizer(dima_synchronizer_t *synchronizer) {
    return dima_from_proxy(synchronizer);
}
