/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RACE_H
#define RACE_H

typedef void race_run_lap_fn(void);

/* Attempts to create a race condition by running the given run_lap function
 * n_laps times from n_racers threads. */
void race(unsigned n_racers, unsigned n_laps, race_run_lap_fn *run_lap);

/* Runs race with some fixed default values of n_racers and n_laps. */
void race_with_defaults(race_run_lap_fn *run_lap);

#endif /* !RACE_H */
