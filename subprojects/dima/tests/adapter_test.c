/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>

#include <dima/adapter.h>
#include <dima/countdown.h>
#include <dima/notifier.h>
#include <dima/system.h>

#include "standard_tests.h"
#include "test_listener.h"

static dima_notifier_t *notifier;
static dima_listener_t *listener;
static dima_countdown_t *countdown;
static dima_adapter_t *adapter;
static dima_t *dima_under_test;
static struct test_listener_data *data;

static void my_free(void *userptr, void *ptr) {
    dima_free(userptr, ptr);
}

static void *my_realloc(void *userptr, void *ptr, size_t size) {
    return dima_realloc(userptr, ptr, size);
}

static dima_adapter_t *create_adapter(dima_t *adaptee) {
    return dima_new_adapter(adaptee, my_free, my_realloc, dima_flags(adaptee));
}

static dima_adapter_t *create_default_adapter(void) {
    return create_adapter(dima_from_countdown(countdown));
}

void set_up(void) {
    notifier = dima_new_notifier(dima_system_instance());
    listener = new_test_listener(notifier);
    countdown = dima_new_countdown(dima_from_notifier(notifier));
    dima_disable_countdown(countdown);
    adapter = create_default_adapter();
    dima_under_test = dima_from_adapter(adapter);

    data = dima_listener_userdata(listener);
    clear_test_listener_data(data);

    set_up_standard_tests(dima_under_test);
}

void tear_down(void) {
    tear_down_standard_tests();
    data = NULL;
    dima_under_test = NULL;
    dima_unref_adapter(adapter);
    adapter = NULL;
    dima_unref_countdown(countdown);
    countdown = NULL;
    dima_destroy_listener(listener);
    listener = NULL;
    dima_unref_notifier(notifier);
    notifier = NULL;
}

START_TEST(new_adapter_with_failing_realloc_returns_null) {
    dima_enable_countdown(countdown);
    dima_adapter_t *a = create_default_adapter();
    ck_assert_ptr_eq(NULL, a);
}
END_TEST

START_TEST(unref_null_works) {
    dima_unref_adapter(NULL);
}
END_TEST

START_TEST(is_not_thread_safe_if_adaptee_is_not) {
    ck_assert_int_eq(0, dima_is_thread_safe(dima_under_test));
}
END_TEST

START_TEST(is_thread_safe_if_adaptee_is) {
    dima_adapter_t *a = create_adapter(dima_system_instance());
    ck_assert_int_ne(0, dima_is_thread_safe(dima_from_adapter(a)));
    dima_unref_adapter(a);
}
END_TEST

START_TEST(free_calls_free) {
    dima_free(dima_under_test, NULL);
    ck_assert_int_eq(1, data->count);
    ck_assert_int_eq(DIMA_FREE, data->invocation.function);
    ck_assert_ptr_eq(NULL, data->invocation.ptr);
}
END_TEST

START_TEST(alloc_calls_realloc) {
    void *ptr = dima_alloc(dima_under_test, 78);
    ck_assert_ptr_ne(NULL, ptr);
    ck_assert_int_eq(1, data->count);
    ck_assert_int_eq(DIMA_REALLOC, data->invocation.function);
    ck_assert_ptr_eq(NULL, data->invocation.ptr);
    ck_assert_int_eq(78, data->invocation.size);
    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(alloc0_calls_realloc) {
    void *ptr = dima_alloc0(dima_under_test, 64);
    ck_assert_ptr_ne(NULL, ptr);
    ck_assert_int_eq(1, data->count);
    ck_assert_int_eq(DIMA_REALLOC, data->invocation.function);
    ck_assert_ptr_eq(NULL, data->invocation.ptr);
    ck_assert_int_eq(64, data->invocation.size);
    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(alloc0_with_failing_realloc_returns_null) {
    dima_enable_countdown(countdown);
    void *ptr = dima_alloc0(dima_under_test, 8);
    ck_assert_ptr_eq(NULL, ptr);
}
END_TEST

START_TEST(realloc_calls_realloc) {
    void *ptr1 = dima_realloc(dima_under_test, NULL, 42);
    ck_assert_ptr_ne(NULL, ptr1);
    ck_assert_int_eq(1, data->count);
    ck_assert_int_eq(DIMA_REALLOC, data->invocation.function);
    ck_assert_ptr_eq(NULL, data->invocation.ptr);
    ck_assert_int_eq(42, data->invocation.size);

    void *ptr2 = dima_realloc(dima_under_test, ptr1, 100);
    ck_assert_ptr_ne(NULL, ptr2);
    ck_assert_int_eq(2, data->count);
    ck_assert_int_eq(DIMA_REALLOC, data->invocation.function);
    ck_assert_ptr_eq(ptr1, data->invocation.ptr);
    ck_assert_int_eq(100, data->invocation.size);

    dima_free(dima_under_test, ptr2);
}
END_TEST

START_TEST(strdup_with_failing_realloc_returns_null) {
    dima_enable_countdown(countdown);
    char *s = dima_strdup(dima_under_test, "Goofy");
    ck_assert_ptr_eq(NULL, s);
}
END_TEST

START_TEST(strndup_with_failing_realloc_returns_null) {
    dima_enable_countdown(countdown);
    char *s = dima_strndup(dima_under_test, "Minnie", 3);
    ck_assert_ptr_eq(NULL, s);
}
END_TEST

void add_tests(void) {
    ADD_TEST(new_adapter_with_failing_realloc_returns_null);
    ADD_TEST(unref_null_works);
    ADD_TEST(is_not_thread_safe_if_adaptee_is_not);
    ADD_TEST(is_thread_safe_if_adaptee_is);
    ADD_TEST(free_calls_free);
    ADD_TEST(alloc_calls_realloc);
    ADD_TEST(alloc0_calls_realloc);
    ADD_TEST(alloc0_with_failing_realloc_returns_null);
    ADD_TEST(realloc_calls_realloc);
    ADD_TEST(strdup_with_failing_realloc_returns_null);
    ADD_TEST(strndup_with_failing_realloc_returns_null);
    add_standard_tests();
}
