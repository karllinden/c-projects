/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>
#include <ecb.h>

#include <dima/countdown.h>
#include <dima/exiting_on_failure.h>
#include <dima/system.h>

#include "standard_tests.h"
#include "test_listener.h"

static dima_notifier_t *notifier;
static dima_t *dima_under_test;

void set_up(void) {
    notifier = dima_new_notifier(dima_system_instance());
    dima_under_test = dima_from_notifier(notifier);
    set_up_standard_tests(dima_under_test);
}

void tear_down(void) {
    tear_down_standard_tests();
    dima_under_test = NULL;
    dima_unref_notifier(notifier);
    notifier = NULL;
}

START_TEST(exits_on_failure_if_next_does) {
    dima_t *exiting = dima_new_exiting_on_failure(dima_system_instance(), 77);
    dima_notifier_t *n = dima_new_notifier(exiting);
    ck_assert_int_eq(1, dima_exits_on_failure(dima_from_notifier(n)));
    dima_unref_notifier(n);
    dima_unref(exiting);
}
END_TEST

START_TEST(new_notifier_returns_null_if_allocation_fails) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_notifier_t *n = dima_new_notifier(dima_from_countdown(countdown));
    ck_assert_ptr_eq(NULL, n);
    dima_unref_countdown(countdown);
}
END_TEST

static void notify_noop_listener(
        ecb_unused dima_listener_t *listener,
        ecb_unused const struct dima_invocation *invocation,
        ecb_unused void *result) {
    /* empty */
}

static dima_listener_t *new_noop_listener(dima_notifier_t *notifier) {
    return dima_new_listener(notifier, notify_noop_listener, 0);
}

START_TEST(new_listener_returns_null_if_allocation_fails) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_start_countdown(countdown, 1);

    dima_notifier_t *n = dima_new_notifier(dima_from_countdown(countdown));
    ck_assert_ptr_ne(NULL, n);

    dima_listener_t *l = new_noop_listener(n);
    ck_assert_ptr_eq(NULL, l);

    dima_unref_notifier(n);
    dima_unref_countdown(countdown);
}
END_TEST

START_TEST(unref_null_notifier) {
    dima_unref_notifier(NULL);
}
END_TEST

START_TEST(destroy_null_listener) {
    dima_destroy_listener(NULL);
}
END_TEST

START_TEST(notifies_about_alloc_and_free) {
    dima_listener_t *listener = new_test_listener(notifier);
    struct test_listener_data *data = dima_listener_userdata(listener);

    void *ptr = dima_alloc(dima_under_test, 16);
    ck_assert_int_eq(1, data->count);
    ck_assert_int_eq(DIMA_ALLOC, data->invocation.function);
    ck_assert_int_eq(16, data->invocation.size);
    ck_assert_ptr_eq(ptr, data->result);

    dima_free(dima_under_test, ptr);
    ck_assert_int_eq(2, data->count);
    ck_assert_int_eq(DIMA_FREE, data->invocation.function);
    ck_assert_ptr_eq(ptr, data->invocation.ptr);
    ck_assert_ptr_eq(NULL, data->result);

    dima_destroy_listener(listener);
}
END_TEST

START_TEST(strdup_with_destroyed_listener) {
    dima_listener_t *l = new_test_listener(notifier);
    dima_destroy_listener(l);
    char *s = dima_strdup(dima_under_test, "");
    dima_free(dima_under_test, s);
}
END_TEST

START_TEST(notifies_multiple_listeners) {
    dima_listener_t *listeners[3];
    for (int i = 0; i < 3; ++i) {
        listeners[i] = new_test_listener(notifier);
    }

    void *ptr = dima_realloc(dima_under_test, NULL, 64);

    for (int i = 0; i < 3; ++i) {
        struct test_listener_data *data = dima_listener_userdata(listeners[i]);
        ck_assert_int_eq(1, data->count);
        ck_assert_int_eq(DIMA_REALLOC, data->invocation.function);
        ck_assert_ptr_eq(NULL, data->invocation.ptr);
        ck_assert_int_eq(64, data->invocation.size);
        ck_assert_ptr_eq(ptr, data->result);
        dima_destroy_listener(listeners[i]);
    }

    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(notifies_first_listener_after_destroying_second) {
    dima_listener_t *l1 = new_test_listener(notifier);
    dima_listener_t *l2 = new_noop_listener(notifier);
    void *ptr = dima_alloc_array(dima_under_test, 16, 32);

    struct test_listener_data *data = dima_listener_userdata(l1);
    ck_assert_int_eq(1, data->count);
    ck_assert_int_eq(DIMA_ALLOC_ARRAY, data->invocation.function);

    dima_destroy_listener(l2);
    dima_free(dima_under_test, ptr);

    ck_assert_int_eq(2, data->count);
    ck_assert_int_eq(DIMA_FREE, data->invocation.function);

    dima_destroy_listener(l1);
}
END_TEST

START_TEST(unref_notifier_before_destroying_listener) {
    dima_notifier_t *n = dima_new_notifier(dima_system_instance());
    dima_listener_t *l = new_noop_listener(n);
    dima_unref_notifier(n);
    dima_destroy_listener(l);
}
END_TEST

static void notify_self_destructing_listener(
        dima_listener_t *listener,
        ecb_unused const struct dima_invocation *invocation,
        ecb_unused void *result) {
    dima_destroy_listener(listener);
}

START_TEST(self_destructing_listener) {
    dima_new_listener(notifier, notify_self_destructing_listener, 0);
    dima_free(dima_under_test, NULL);
}
END_TEST

struct killing_listener_data {
    dima_listener_t *victim;
};

static void notify_killing_listener(
        dima_listener_t *listener,
        ecb_unused const struct dima_invocation *invocation,
        ecb_unused void *result) {
    struct killing_listener_data *data = dima_listener_userdata(listener);
    dima_destroy_listener(data->victim);
    data->victim = NULL;
}

START_TEST(destroy_other_listener_during_notification) {
    dima_listener_t *victim = new_noop_listener(notifier);
    dima_listener_t *killer
            = dima_new_listener(notifier,
                                notify_killing_listener,
                                sizeof(struct killing_listener_data));
    struct killing_listener_data *data = dima_listener_userdata(killer);
    data->victim = victim;
    dima_free(dima_under_test, NULL);
    dima_destroy_listener(killer);
}
END_TEST

static void alloc_and_free(void) {
    void *ptr = dima_alloc(dima_under_test, 20);
    dima_free(dima_under_test, ptr);
}

struct invoking_listener_data {
    int invoke;
};

static void notify_invoking_listener(
        dima_listener_t *invoker,
        ecb_unused const struct dima_invocation *invocation,
        ecb_unused void *result) {
    struct invoking_listener_data *data = dima_listener_userdata(invoker);
    if (data->invoke) {
        data->invoke = 0;
        alloc_and_free();
    }
}

START_TEST(aborts_if_listener_invokes_notifier) {
    dima_listener_t *invoker
            = dima_new_listener(notifier,
                                notify_invoking_listener,
                                sizeof(struct invoking_listener_data));
    struct invoking_listener_data *data = dima_listener_userdata(invoker);
    data->invoke = 1;

    alloc_and_free();

    dima_destroy_listener(invoker);
}
END_TEST

START_TEST(aborts_if_too_many_unrefs_with_listener) {
    dima_notifier_t *n = dima_new_notifier(dima_under_test);
    new_noop_listener(n);
    dima_t *dima = dima_from_notifier(n);
    dima_unref(dima);
    dima_unref(dima);
}
END_TEST

START_TEST(aborts_if_notify_is_null) {
    dima_new_listener(notifier, NULL, 0);
}
END_TEST

void add_tests(void) {
    add_standard_tests();
    ADD_TEST(exits_on_failure_if_next_does);
    ADD_TEST(new_notifier_returns_null_if_allocation_fails);
    ADD_TEST(new_listener_returns_null_if_allocation_fails);
    ADD_TEST(unref_null_notifier);
    ADD_TEST(destroy_null_listener);
    ADD_TEST(notifies_about_alloc_and_free);
    ADD_TEST(strdup_with_destroyed_listener);
    ADD_TEST(notifies_multiple_listeners);
    ADD_TEST(notifies_first_listener_after_destroying_second);
    ADD_TEST(unref_notifier_before_destroying_listener);
    ADD_TEST(self_destructing_listener);
    ADD_TEST(destroy_other_listener_during_notification);
    ADD_ABORT_TEST(aborts_if_listener_invokes_notifier);
    ADD_ABORT_TEST(aborts_if_too_many_unrefs_with_listener);
    ADD_ABORT_TEST(aborts_if_notify_is_null);
}
