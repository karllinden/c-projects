/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>

#include <dima/notifier.h>
#include <dima/system.h>

#include "forwarding_tests.h"
#include "test_listener.h"

static dima_notifier_t *notifier;
static dima_listener_t *listener;
static struct test_listener_data *data;
static dima_t *dima_under_test;

void set_up(void) {
    notifier = dima_new_notifier(dima_system_instance());
    listener = new_test_listener(notifier);
    data = dima_listener_userdata(listener);
    dima_under_test = create_forwarding_dima(dima_from_notifier(notifier));
    clear_test_listener_data(data);
}

void tear_down(void) {
    dima_unref(dima_under_test);
    dima_under_test = NULL;
    data = NULL;
    dima_destroy_listener(listener);
    listener = NULL;
    dima_unref_notifier(notifier);
    notifier = NULL;
}

static void assert_invocation(unsigned long expected_count,
                              const struct dima_invocation *expected_invocation,
                              void *expected_result) {
    int cmp = dima_compare_invocations(&data->invocation, expected_invocation);
    ck_assert_int_eq(expected_count, data->count);
    ck_assert_int_eq(0, cmp);
    ck_assert_ptr_eq(expected_result, data->result);
}

static void *invoke_expect_non_null(unsigned long expected_count,
                                    const struct dima_invocation *invocation) {
    void *ptr = dima_invoke(dima_under_test, invocation);
    ck_assert_ptr_ne(NULL, ptr);
    assert_invocation(expected_count, invocation, ptr);
    return ptr;
}

START_TEST(forwards_alloc_and_free) {
    struct dima_invocation alloc_inv;
    dima_init_alloc_invocation(&alloc_inv, 16);
    void *ptr = invoke_expect_non_null(1, &alloc_inv);

    struct dima_invocation free_inv;
    dima_init_free_invocation(&free_inv, ptr);
    void *nptr = dima_invoke(dima_under_test, &free_inv);
    ck_assert_ptr_eq(NULL, nptr);
    assert_invocation(2, &free_inv, NULL);
}
END_TEST

START_TEST(forwards_alloc0) {
    struct dima_invocation alloc0_inv;
    dima_init_alloc0_invocation(&alloc0_inv, 40);
    void *ptr = invoke_expect_non_null(1, &alloc0_inv);

    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(forwards_realloc) {
    void *ptr = dima_alloc(dima_under_test, 16);
    ck_assert_ptr_ne(NULL, ptr);

    struct dima_invocation inv;
    dima_init_realloc_invocation(&inv, ptr, 32);
    void *new_ptr = invoke_expect_non_null(2, &inv);

    dima_free(dima_under_test, new_ptr);
}
END_TEST

START_TEST(forwards_alloc_array) {
    struct dima_invocation inv;
    dima_init_alloc_array_invocation(&inv, 16, 48);
    void *ptr = dima_invoke(dima_under_test, &inv);
    ck_assert_ptr_ne(NULL, ptr);
    assert_invocation(1, &inv, ptr);

    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(forwards_alloc_array0) {
    struct dima_invocation inv;
    dima_init_alloc_array0_invocation(&inv, 64, 4);
    void *ptr = invoke_expect_non_null(1, &inv);

    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(forwards_realloc_array) {
    void *ptr = dima_alloc_array(dima_under_test, 30, 40);
    ck_assert_ptr_ne(NULL, ptr);

    struct dima_invocation inv;
    dima_init_realloc_array_invocation(&inv, ptr, 50, 40);
    void *new_ptr = invoke_expect_non_null(2, &inv);

    dima_free(dima_under_test, new_ptr);
}
END_TEST

START_TEST(forwards_strdup) {
    struct dima_invocation inv;
    dima_init_strdup_invocation(&inv, "Mary had a little lamb.");
    char *s = invoke_expect_non_null(1, &inv);

    dima_free(dima_under_test, s);
}
END_TEST

START_TEST(forwards_strndup) {
    struct dima_invocation inv;
    dima_init_strndup_invocation(&inv, "The quick brown fox...", 10);
    char *s = invoke_expect_non_null(1, &inv);

    dima_free(dima_under_test, s);
}
END_TEST

void add_tests(void) {
    ADD_TEST(forwards_alloc_and_free);
    ADD_TEST(forwards_alloc0);
    ADD_TEST(forwards_realloc);
    ADD_TEST(forwards_alloc_array);
    ADD_TEST(forwards_alloc_array0);
    ADD_TEST(forwards_realloc_array);
    ADD_TEST(forwards_strdup);
    ADD_TEST(forwards_strndup);
}
