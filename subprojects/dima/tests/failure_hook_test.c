/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>

#include <check-simpler.h>
#include <ecb.h>

#include <dima/countdown.h>
#include <dima/failure_hook.h>
#include <dima/invocation.h>
#include <dima/system.h>

typedef void *init_test_fn(void);
typedef void *run_test_fn(const struct dima_invocation *invocation,
                          void *init_ptr);

struct hook_test {
    init_test_fn *init;
    run_test_fn *run;
    struct dima_invocation invocation;
};

#define N_HOOK_TESTS 10
static struct hook_test hook_tests[N_HOOK_TESTS];
static int n_hook_tests = 0;

static int my_hook_count;

static dima_countdown_t *countdown;
static dima_t *dima_under_test;

static void my_hook(void *userdata) {
    long *lp = userdata;
    ck_assert_int_eq(0xDEADBEEF, *lp);
    my_hook_count++;
}

static long *create_hook(void) {
    dima_t *next = dima_from_countdown(countdown);
    return dima_new_failure_hook(next, my_hook, dima_flags(next), sizeof(long));
}

void set_up(void) {
    my_hook_count = 0;

    countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);
    long *hook = create_hook();
    *hook = 0xDEADBEEF;

    dima_under_test = dima_from_failure_hook(hook);
}

static void tear_down_countdown(void) {
    dima_unref_countdown(countdown);
    countdown = NULL;
}

void tear_down(void) {
    dima_unref(dima_under_test);
    dima_under_test = NULL;
    tear_down_countdown();
    my_hook_count = 0;
}

START_TEST(new_failure_hook_returns_null_if_allocation_fails) {
    dima_enable_countdown(countdown);
    long *hook = create_hook();
    ck_assert_ptr_eq(hook, NULL);
}
END_TEST

START_TEST(failure_hook_owns_next) {
    tear_down_countdown();
    dima_free(dima_under_test, NULL);
}
END_TEST

START_TEST(does_not_call_hook_on_success) {
    const struct hook_test *test = hook_tests + _i;

    void *init_ptr = test->init();
    void *ptr = test->run(&test->invocation, init_ptr);
    dima_free(dima_under_test, ptr);

    ck_assert_ptr_ne(NULL, ptr);
    ck_assert_int_eq(0, my_hook_count);
}
END_TEST

START_TEST(calls_hook_on_failure) {
    const struct hook_test *test = hook_tests + _i;

    void *init_ptr = test->init();
    dima_enable_countdown(countdown);
    void *ptr = test->run(&test->invocation, init_ptr);
    dima_free(dima_under_test, init_ptr);

    ck_assert_ptr_eq(NULL, ptr);
    ck_assert_int_eq(1, my_hook_count);
}
END_TEST

static struct dima_invocation *add_hook_test(init_test_fn *init,
                                             run_test_fn *run) {
    assert(n_hook_tests < N_HOOK_TESTS);
    struct hook_test *test = hook_tests + n_hook_tests;
    n_hook_tests++;
    test->init = init;
    test->run = run;
    return &test->invocation;
}

static void *init_invocation_test(void) {
    return NULL;
}

static void *run_invocation_test(const struct dima_invocation *invocation,
                                 ecb_unused void *init_ptr) {
    return dima_invoke(dima_under_test, invocation);
}

static void add_invocation_test(const struct dima_invocation *invocation) {
    struct dima_invocation *dest
            = add_hook_test(init_invocation_test, run_invocation_test);
    dima_copy_invocation(dest, invocation);
}

static void *init_realloc_test(void) {
    return dima_alloc(dima_under_test, 70);
}

static void *run_realloc_test(
        ecb_unused const struct dima_invocation *invocation,
        void *init_ptr) {
    return dima_realloc(dima_under_test, init_ptr, 120);
}

static void *init_realloc_array_test(void) {
    return dima_alloc_array(dima_under_test, 12, 32);
}

static void *run_realloc_array_test(
        ecb_unused const struct dima_invocation *invocation,
        void *init_ptr) {
    return dima_realloc_array(dima_under_test, init_ptr, 24, 32);
}

static void init_hook_tests(void) {
    struct dima_invocation invocation;

    dima_init_alloc_invocation(&invocation, 8);
    add_invocation_test(&invocation);

    dima_init_alloc0_invocation(&invocation, 40);
    add_invocation_test(&invocation);

    dima_init_realloc_invocation(&invocation, NULL, 32);
    add_invocation_test(&invocation);

    dima_init_alloc_array_invocation(&invocation, 23, 25);
    add_invocation_test(&invocation);

    dima_init_alloc_array0_invocation(&invocation, 12, 21);
    add_invocation_test(&invocation);

    dima_init_realloc_array_invocation(&invocation, NULL, 31, 12);
    add_invocation_test(&invocation);

    dima_init_strdup_invocation(&invocation, "Hello!");
    add_invocation_test(&invocation);

    dima_init_strndup_invocation(&invocation, "Goodbye!", 6);
    add_invocation_test(&invocation);

    add_hook_test(init_realloc_test, run_realloc_test);
    add_hook_test(init_realloc_array_test, run_realloc_array_test);

    assert(n_hook_tests == N_HOOK_TESTS);
}

void add_tests(void) {
    init_hook_tests();

    ADD_TEST(new_failure_hook_returns_null_if_allocation_fails);
    ADD_TEST(failure_hook_owns_next);
    ADD_LOOP_TEST(does_not_call_hook_on_success, 0, N_HOOK_TESTS);
    ADD_LOOP_TEST(calls_hook_on_failure, 0, N_HOOK_TESTS);
}
