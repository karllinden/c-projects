/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>

#include <dima/countdown.h>
#include <dima/exiting_on_failure.h>
#include <dima/system.h>

static dima_t *dima_under_test;

START_TEST(exits_on_failure) {
    ck_assert_int_ne(0, dima_exits_on_failure(dima_under_test));
}
END_TEST

START_TEST(new_returns_null_if_allocation_fails) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_t *next = dima_from_countdown(countdown);
    dima_t *result = dima_new_exiting_on_failure(next, 42);
    ck_assert_ptr_eq(NULL, result);
    dima_unref_countdown(countdown);
}
END_TEST

void set_up(void) {
    dima_under_test = dima_new_exiting_on_failure(dima_system_instance(), 88);
}

void tear_down(void) {
    dima_unref(dima_under_test);
    dima_under_test = NULL;
}

void add_tests(void) {
    ADD_TEST(exits_on_failure);
    ADD_TEST(new_returns_null_if_allocation_fails);
}
