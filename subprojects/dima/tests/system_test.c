/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>

#include <dima/system.h>

#include "standard_tests.h"

static dima_t *dima_under_test;

START_TEST(system_is_thread_safe) {
    ck_assert_int_ne(0, dima_is_thread_safe(dima_under_test));
}
END_TEST

START_TEST(unref_system_aborts) {
    dima_unref(dima_under_test);
}
END_TEST

void set_up(void) {
    dima_under_test = dima_system_instance();
    set_up_standard_tests(dima_under_test);
}

void tear_down(void) {
    tear_down_standard_tests();
    dima_under_test = NULL;
}

void add_tests(void) {
    ADD_TEST(system_is_thread_safe);
    ADD_ABORT_TEST(unref_system_aborts);
    add_standard_tests();
}
