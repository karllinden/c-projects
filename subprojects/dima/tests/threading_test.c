/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include <check-simpler.h>
#include <ecb.h>

#include <dima/adapter.h>
#include <dima/exiting_on_failure.h>
#include <dima/system.h>

#include "mutex_tests.h"
#include "race.h"

static dima_t *dima_under_test;

void set_up(void) {
    set_up_mutex_tests();
    dima_under_test = dima_system_instance();
}

void tear_down(void) {
    dima_under_test = NULL;
    tear_down_mutex_tests();
}

static void my_free(ecb_unused void *userptr, void *ptr) {
    free(ptr);
}

static void *my_realloc(ecb_unused void *userptr, void *ptr, size_t size) {
    return realloc(ptr, size);
}

START_TEST(new_adapter_with_failing_mutex_init_returns_null) {
    fail_pthread_mutex_init_after(0);
    dima_adapter_t *adapter = dima_new_adapter(NULL, my_free, my_realloc, 0);
    ck_assert_ptr_eq(NULL, adapter);
}
END_TEST

START_TEST(new_exiting_on_failure_with_failing_mutex_init_returns_null) {
    fail_pthread_mutex_init_after(0);
    dima_t *dima = dima_new_exiting_on_failure(dima_system_instance(), 2);
    ck_assert_ptr_eq(NULL, dima);
}
END_TEST

static void ref_and_unref_like_crazy(void) {
    dima_ref(dima_under_test);

    dima_ref(dima_under_test);
    dima_unref(dima_under_test);

    dima_ref(dima_under_test);
    dima_unref(dima_under_test);

    dima_unref(dima_under_test);
}

START_TEST(ref_and_unref_are_thread_safe) {
    race_with_defaults(ref_and_unref_like_crazy);
}
END_TEST

void add_tests(void) {
    ADD_TEST(new_adapter_with_failing_mutex_init_returns_null);
    ADD_TEST(new_exiting_on_failure_with_failing_mutex_init_returns_null);
    ADD_TEST(ref_and_unref_are_thread_safe);
}
