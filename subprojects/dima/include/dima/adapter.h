/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dima/adapter.h
 * --------------
 * A skeletal implementation that adapts an existing memory allocator to DIMA's
 * interface.
 *
 * Adapters are created with dima_new_adapter(). The DIMA functions are
 * implemented with the free and realloc functions that clients must provide.
 * The free and realloc functions shall behave as the corresponding standard C11
 * memory allocation functions, except that they take a user-defined pointer as
 * their first argument.
 */

#ifndef DIMA_ADAPTER_H
#define DIMA_ADAPTER_H

#include <dima/dima.h>

typedef void dima_adapter_free_fn(void *userptr, void *ptr);
typedef void *dima_adapter_realloc_fn(void *userptr, void *ptr, size_t size);

typedef struct dima_adapter_s dima_adapter_t;

/**
 * Creates a DIMA adapter that uses the given free and realloc functions.
 *
 * The adapter is allocated with the realloc function. When the adapter is no
 * longer needed, it shall either be passed to dima_unref_adapter(), or its
 * corresponding DIMA (see dima_from_adapter()) shall be passed to dima_unref().
 *
 * @param userptr a user-defined pointer, which may be NULL, that will be passed
 *                to the free and realloc functions
 * @param free the free function
 * @param realloc the realloc function
 * @param flags the flags
 * @return the newly created adapter, or NULL on failure
 */
dima_adapter_t *dima_new_adapter(void *userptr,
                                 dima_adapter_free_fn *free,
                                 dima_adapter_realloc_fn *realloc,
                                 unsigned flags);

/**
 * Decrements the reference count of the adapter.
 *
 * If the adapter is NULL, this function does nothing.
 *
 * @param adapter the adapter, or NULL
 */
void dima_unref_adapter(dima_adapter_t *adapter);

/**
 * Returns the adapter's DIMA implementation.
 */
dima_t *dima_from_adapter(dima_adapter_t *adapter);

#endif /* !DIMA_ADAPTER_H */
