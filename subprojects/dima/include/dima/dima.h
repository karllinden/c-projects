/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dima/dima.h
 * -----------
 * Declaration of the dependency-injectible memory allocator (DIMA) abtraction.
 *
 * The DIMA abstraction supports the following functions:
 *  + dima_free()
 *  + dima_alloc()
 *  + dima_alloc0()
 *  + dima_realloc()
 *  + dima_alloc_array()
 *  + dima_alloc_array0()
 *  + dima_realloc_array()
 *  + dima_strdup()
 *  + dima_strndup().
 * The signatures of these functions are described in this header. They all take
 * a pointer to the DIMA implementation as their first argument, which is not
 * allowed to be NULL.
 *
 * On error the functions in this header return NULL or exit, depending on the
 * implementation. Implementations are required to set the DIMA_EXITS_ON_FAILURE
 * flag if and only if the implementation exits on failure. The flag can be
 * queried with dima_exits_on_failure(). See dima_new_exiting_on_failure() for
 * an implementation that exits on failure.
 *
 * The functions in this header are not required to set errno on failure. This
 * can be achieved by using dima_new_failure_hook().
 *
 * The dima_alloc(), dima_realloc() and dima_alloc_array0() behave as malloc(),
 * realloc() and calloc() from the C11 standard library, except that the memory
 * is allocated according to the DIMA implementation and that they may exit on
 * failure.
 *
 * The dima_free() function behaves as the C11 standard library free(), except
 * that it free's memory according to the DIMA implementation.
 *
 * dima_alloc_array() behaves as dima_alloc_array0() except that the returned
 * memory is not initialized to zero.
 *
 * The dima_realloc_array(), dima_strdup() and dima_strndup() functions behave
 * as their glibc counterparts reallocarray(), strdup() and strndup(), except
 * that they allocate memory according to the DIMA implementation, are not
 * required to set errno on failure, and may exit on failure. Unlike in glibc,
 * dima_realloc_array(d, p, 0, 0) is not required to be equivalent to
 * dima_free(d, p).
 *
 * The dima_alloc0() function behaves as dima_alloc() except that the returned
 * memory is initialized to zero.
 *
 * The dima_*alloc_array*() functions fail if the total size, nmemb * size,
 * overflows.
 *
 * Implementations are free to make further guarantees that are not required by
 * this abstraction. For example an implementation may guarantees such as:
 *  + the functions are multi-thread safe
 *  + the functions never exit on failure
 *  + the functions always exit on failure
 *  + errno is always set on error
 *  + dima_alloc(d, 0) always returns non-NULL
 *  + dima_realloc(d, p, 0) behaves as dima_free(d, p)
 *  + etc.
 *
 * Some of the additional guarantees have corresponding flags, and
 * implementations must fill in the flags correctly. See the defined flags
 * below. For example an implementation must set DIMA_EXITS_ON_FAILURE if and
 * only if it exits on failure.
 *
 * Implementors should document any additional guarantees in comments and/or by
 * setting flags. See for example dima_new_exiting_on_failure() and
 * dima_new_synchronizer(). Clients may depend on any additional guarantees, but
 * they should document any additional requirements, either in comments or by
 * asserting on flags.
 *
 * For examples of some implementations of this abstraction see:
 *  + dima/system.h
 *  + dima/adapter.h
 *  + dima/exiting_on_failure.h
 *  + dima/failure_hook.h
 *  + dima/proxy.h
 *  + dima/synchronizer.h
 *  + dima/countdown.h
 *  + dima/notifier.h
 *
 * Clients outside the DIMA library may implement this abstraction, by using one
 * of the skeletal implementations. An adapter can be used to plug any
 * allocation functions into DIMA. A failure hook can be used if only the
 * behavior on failue needs customization. A proxy can be used to implement the
 * same customization for all allocation functions. See dima_new_adapter(),
 * dima_new_failure_hook() and dima_new_proxy().
 */

#ifndef DIMA_DIMA_H
#define DIMA_DIMA_H

#include <stddef.h>

/**
 * Flag that is set if and only if the implementation exits on failure.
 */
#define DIMA_EXITS_ON_FAILURE (1u << 0)

/**
 * Flag that is set if and only if the implementation is safe for multi-threaded
 * use.
 *
 * Note that an implementation cannot be thread safe and thread hostile at the
 * same time.
 */
#define DIMA_IS_THREAD_SAFE (1u << 1)

/**
 * Flag that is set if and only if the implementation is thread hostile.
 *
 * A thread hostile implementation is thread unsafe and cannot be made thread
 * safe by wrapping it in a synchronized proxy (such as a dima_synchronizer_t).
 *
 * Note that an implementation cannot be thread safe and thread hostile at the
 * same time.
 */
#define DIMA_IS_THREAD_HOSTILE (1u << 2)

typedef struct dima_s dima_t;

/* TODO: document when implementation has stabilized */
void dima_ref(dima_t *dima);
void dima_unref(dima_t *dima);

void dima_free(dima_t *dima, void *ptr);
void *dima_alloc(dima_t *dima, size_t size);
void *dima_alloc0(dima_t *dima, size_t size);
void *dima_realloc(dima_t *dima, void *ptr, size_t size);
void *dima_alloc_array(dima_t *dima, size_t nmemb, size_t size);
void *dima_alloc_array0(dima_t *dima, size_t nmemb, size_t size);
void *dima_realloc_array(dima_t *dima, void *ptr, size_t nmemb, size_t size);
char *dima_strdup(dima_t *dima, const char *s);
char *dima_strndup(dima_t *dima, const char *s, size_t n);

/**
 * Returns the flags of the given DIMA.
 */
unsigned dima_flags(const dima_t *dima);

/**
 * Returns non-zero if and only if the implementation exits on failure.
 *
 * DIMA implementations must declare whether or not they exit on failure by
 * setting DIMA_EXITS_ON_FAILURE appropriately.
 */
static inline int dima_exits_on_failure(const dima_t *dima) {
    return dima_flags(dima) & DIMA_EXITS_ON_FAILURE;
}

/**
 * Returns non-zero if and only if the implementation is safe for multi-threaded
 * use.
 *
 * DIMA implementations must declare whether or not they are thread safe by
 * setting DIMA_IS_THREAD_SAFE appropriately.
 */
static inline int dima_is_thread_safe(const dima_t *dima) {
    return dima_flags(dima) & DIMA_IS_THREAD_SAFE;
}

/**
 * Returns non-zero if and only if the implementation is thread-hostile.
 *
 * DIMA implementations must declare whether or not they are thread hostile by
 * setting DIMA_IS_THREAD_HOSTILE appropriately.
 */
static inline int dima_is_thread_hostile(const dima_t *dima) {
    return dima_flags(dima) & DIMA_IS_THREAD_HOSTILE;
}

#endif /* !DIMA_DIMA_H */
