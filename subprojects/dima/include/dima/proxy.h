/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * dima/proxy.h
 * ------------
 * Partial DIMA implementation that makes it easy to write proxy
 * implementations.
 *
 * DIMA proxies expose an exhaustive DIMA abstraction, and only requires clients
 * to implement a single function of type dima_invoke_fn that handles all
 * invocations on the proxy. The invocation on the proxy is passed as a struct
 * dima_invocation. This allows implementations to perform a common operation on
 * each invocation, including, but not limited to, invoking the next DIMA with
 * dima_invoke().
 *
 * The dima_new_proxy() function creates a new proxy, which can be used wherever
 * a dima_t * is needed, by using the dima_from_proxy() function.
 *
 * There is no public proxy type. Instead, each proxy is identified by its
 * userdata pointer. This makes the user code more concise and reduces the
 * number of API functions needed to implement proxies in user code. The
 * drawback is a slight loss of type safety.
 */

#ifndef DIMA_PROXY_H
#define DIMA_PROXY_H

#include <dima/invocation.h>

typedef void *dima_invoke_fn(dima_t *next,
                             void *userdata,
                             const struct dima_invocation *invocation);

typedef void dima_deinit_fn(void *userdata);

/**
 * Creates a new DIMA proxy.
 *
 * The proxy is allocated using the next DIMA. When the proxy is no longer
 * needed, it shall be either passed to dima_unref_proxy, or its corresponding
 * DIMA (see dima_from_proxy) shall be passed to dima_unref.
 *
 * The newly created proxy is uniquely identified by its userdata, whose size is
 * fixed during the lifetime of the proxy and is given by the size parameter.
 *
 * @param next the next DIMA
 * @param invoke the function to call with each invocation on the proxy
 * @param flags the flags
 * @param size the size of the userdata
 * @return a pointer to the userdata of the proxy, or NULL on failure
 */
void *dima_new_proxy(dima_t *next,
                     dima_invoke_fn *invoke,
                     unsigned flags,
                     size_t size);

/**
 * Decrements the reference count of the proxy given a pointer to its userdata.
 *
 * If the userdata is NULL, this function does nothing.
 *
 * @param userdata the userdata, or NULL
 */
void dima_unref_proxy(void *userdata);

/**
 * Returns the DIMA implementation of the proxy given a pointer to its userdata.
 */
dima_t *dima_from_proxy(void *userdata);

/**
 * Returns the next DIMA of the proxy.
 */
dima_t *dima_proxy_next(void *userdata);

/**
 * Sets a function to be called to de-initialize the proxy userdata.
 *
 * @param proxy the proxy
 * @param deinit the function to call to de-initialize the proxy
 */
void dima_set_deinit_proxy(void *userdata, dima_deinit_fn *deinit);

#endif /* !DIMA_PROXY_H */
