/*
 * This file is part of flml.
 *
 * Copyright (C) 2018, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>
#include <dima/countdown.h>
#include <dima/system.h>

#include <flml/flml.h>

/* Only pointers to these variables are needed. */
static int arbitrary;
static double other_arbitrary;

#define PTR (&arbitrary)
#define PTR2 (&other_arbitrary)

static struct flml *flml;

void set_up(void) {
    struct flml_error error;
    flml = flml_new(&error);
}

void tear_down(void) {
    flml_destroy(flml);
    flml = NULL;
}

START_TEST(new) {
    struct flml_error error;
    error.type = FLML_ERROR_NONE;
    struct flml *res = flml_new(&error);

    ck_assert_ptr_ne(res, NULL);
    ck_assert_int_eq(error.type, FLML_ERROR_NONE);

    flml_destroy(res);
}
END_TEST

static void test_new_fails_after(unsigned n_allocations) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_start_countdown(countdown, n_allocations);
    dima_t *dima = dima_from_countdown(countdown);

    struct flml_error error;
    error.type = FLML_ERROR_NONE;
    struct flml *res = flml_new_with_dima(dima, &error);

    ck_assert_ptr_eq(res, NULL);
    ck_assert_int_eq(error.type, FLML_ERROR_MEM);

    dima_unref_countdown(countdown);
}

START_TEST(new_fails_after_n_allocations) {
    test_new_fails_after(_i);
}
END_TEST

START_TEST(default_ptr_is_null) {
    ck_assert_ptr_eq(flml_get_ptr(flml), NULL);
}
END_TEST

START_TEST(set_ptr) {
    flml_set_ptr(flml, PTR);
    ck_assert_ptr_eq(flml_get_ptr(flml), PTR);
}
END_TEST

START_TEST(set_ptr_twice) {
    flml_set_ptr(flml, PTR);
    flml_set_ptr(flml, PTR2);
    ck_assert_ptr_eq(flml_get_ptr(flml), PTR2);
}
END_TEST

START_TEST(default_last_error_is_none) {
    const struct flml_error *error = flml_get_last_error(flml);
    ck_assert_int_eq(error->type, FLML_ERROR_NONE);
}
END_TEST

START_TEST(destroy_null) {
    flml_destroy(NULL);
}
END_TEST

void add_tests(void) {
    ADD_TEST(new);
    ADD_LOOP_TEST(new_fails_after_n_allocations, 0, 4);
    ADD_TEST(default_ptr_is_null);
    ADD_TEST(set_ptr);
    ADD_TEST(set_ptr_twice);
    ADD_TEST(default_last_error_is_none);
    ADD_TEST(destroy_null);
}
