/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <ecb.h>

#include <check-simpler.h>
#include <dima/countdown.h>
#include <dima/system.h>

#include <flml/timer.h>

#include "src/timer.h"
#include "src/timespec.h"

#define MINUTE 60
#define HOUR (60 * MINUTE)
#define DAY (24 * HOUR)
#define WEEK (7 * DAY)

#define MILLISECOND (1000 * 1000)

#define PTR ((void *)94)
#define PTR2 ((void *)128)

#define PERIODIC_MILLIS 32

static const struct timespec dummy_expiry = {0, 1};

static dima_countdown_t *countdown;
static struct flml *flml;

void set_up(void) {
    countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);

    struct flml_error error;
    flml = flml_new_with_dima(dima_from_countdown(countdown), &error);
}

void tear_down(void) {
    flml_destroy(flml);
    flml = NULL;

    dima_unref_countdown(countdown);
    countdown = NULL;
}

static void init_next(struct timespec *next) {
    next->tv_sec = -1;
    next->tv_nsec = -1;
}

static void assert_no_next(const struct timespec *next) {
    ck_assert_int_eq(next->tv_sec, -1);
    ck_assert_int_eq(next->tv_nsec, -1);
}

static void assert_next_before(const struct timespec *next,
                               const struct timespec *bound) {
    struct timespec zero;
    zero.tv_sec = 0;
    zero.tv_nsec = 0;
    ck_assert_int_lt(flml_timespec_compare(&zero, next), 0);
    ck_assert_int_lt(next->tv_nsec, 1000 * MILLISECOND);
    ck_assert_int_le(flml_timespec_compare(next, bound), 0);
}

static void dummy_cb(ecb_unused struct flml *flml,
                     ecb_unused flml_timer_key key) {
    /* empty */
}

static void set_to_42_cb(struct flml *flml, flml_timer_key key) {
    int *ip = flml_timer_get_ptr(flml, key);
    *ip = 42;
}

static void set_to_83_cb(struct flml *flml, flml_timer_key key) {
    int *ip = flml_timer_get_ptr(flml, key);
    *ip = 83;
}

static void increment_cb(struct flml *flml, flml_timer_key key) {
    int *ip = flml_timer_get_ptr(flml, key);
    (*ip)++;
}

static void sleep_timespec(const struct timespec *ts) {
    struct timespec remaining;
    remaining.tv_sec = ts->tv_sec;
    remaining.tv_nsec = ts->tv_nsec;

    int ret;
    do {
        ret = nanosleep(&remaining, &remaining);
        if (ret && errno != EINTR) {
            perror("nanosleep");
            exit(1);
        }
    } while (ret);
}

static void sleep_millis(unsigned millis) {
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = millis * MILLISECOND;
    sleep_timespec(&ts);
}

/**
 * Get a struct timespec that will cause an overflow on registration.
 *
 * @param res the struct timespec to fill in
 */
static void get_overflowing_timespec(struct timespec *res) {
    struct timespec now;
    flml_timespec_now(&now);
    res->tv_sec = LONG_MAX - now.tv_sec + 1;
    res->tv_nsec = 0;
}

static void test_register_fails_after(int n_allocations) {
    dima_start_countdown(countdown, n_allocations);
    dima_enable_countdown(countdown);

    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 5 * MILLISECOND;

    flml_timer_key key = flml_timer_register(flml, &ts, &dummy_cb);

    ck_assert(flml_timer_key_is_error(key));
    ck_assert_int_eq(flml_get_last_error(flml)->type, FLML_ERROR_MEM);
}

START_TEST(register_fails_if_first_allocation_fails) {
    test_register_fails_after(0);
}
END_TEST

START_TEST(register_fails_if_second_allocation_fails) {
    test_register_fails_after(1);
}
END_TEST

static flml_timer_key register_dummy_timer(struct flml *flml) {
    return flml_timer_register(flml, &dummy_expiry, dummy_cb);
}

START_TEST(register_and_unregister_repeatedly_does_not_allocate) {
    register_dummy_timer(flml);
    flml_timer_key dummy2 = register_dummy_timer(flml);
    register_dummy_timer(flml);
    flml_timer_key dummy4 = register_dummy_timer(flml);

    flml_timer_unregister(flml, dummy2);
    flml_timer_unregister(flml, dummy4);

    dima_enable_countdown(countdown);
    for (int i = 0; i < 1000; ++i) {
        flml_timer_key key = register_dummy_timer(flml);
        flml_timer_unregister(flml, key);
    }
}
END_TEST

START_TEST(register_returns_non_error_key) {
    flml_timer_key key = register_dummy_timer(flml);
    ck_assert(!flml_timer_key_is_error(key));
}
END_TEST

START_TEST(default_ptr_is_null) {
    flml_timer_key key = register_dummy_timer(flml);
    ck_assert_ptr_eq(flml_timer_get_ptr(flml, key), NULL);
}
END_TEST

START_TEST(get_ptr) {
    flml_timer_key key = register_dummy_timer(flml);
    flml_timer_set_ptr(flml, key, PTR);
    ck_assert_ptr_eq(flml_timer_get_ptr(flml, key), PTR);
}
END_TEST

START_TEST(set_ptr_twice) {
    flml_timer_key key = register_dummy_timer(flml);
    flml_timer_set_ptr(flml, key, PTR);
    flml_timer_set_ptr(flml, key, PTR2);
    ck_assert_ptr_eq(flml_timer_get_ptr(flml, key), PTR2);
}
END_TEST

START_TEST(trigger_without_timers) {
    struct timespec ts;
    ts.tv_sec = 75;
    ts.tv_nsec = 357992735;
    flml_timers_trigger(flml, &ts);

    ck_assert_int_eq(ts.tv_sec, 75);
    ck_assert_int_eq(ts.tv_nsec, 357992735);
}
END_TEST

START_TEST(trigger_with_one_unexpired_timer) {
    /*
     * Something that is guaranteed to not timer during the test is
     * needed. Assume this test is running for less than 8 weeks...
     */
    struct timespec ts;
    ts.tv_sec = 8 * WEEK;
    ts.tv_nsec = 9 * MILLISECOND;
    flml_timer_register(flml, &ts, dummy_cb);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);

    assert_next_before(&next, &ts);
}
END_TEST

START_TEST(trigger_with_two_unexpired_timers) {
    struct timespec ts1;
    ts1.tv_sec = 7 * WEEK;
    ts1.tv_nsec = 824 * MILLISECOND;
    flml_timer_register(flml, &ts1, dummy_cb);

    struct timespec ts2;
    ts2.tv_sec = 4 * WEEK;
    ts2.tv_nsec = 348 * MILLISECOND;
    flml_timer_register(flml, &ts2, &dummy_cb);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);

    assert_next_before(&next, &ts2);
}
END_TEST

START_TEST(trigger_with_one_expired_timer) {
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 10 * MILLISECOND;
    int value = 0;
    flml_timer_key key = flml_timer_register(flml, &ts, set_to_42_cb);
    flml_timer_set_ptr(flml, key, &value);

    sleep_millis(10);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);

    ck_assert_int_eq(value, 42);
    assert_no_next(&next);
}
END_TEST

START_TEST(trigger_with_two_expired_timers) {
    struct timespec ts1;
    ts1.tv_sec = 0;
    ts1.tv_nsec = 7 * MILLISECOND;
    int value1 = 0;
    flml_timer_key key1 = flml_timer_register(flml, &ts1, set_to_42_cb);
    flml_timer_set_ptr(flml, key1, &value1);

    struct timespec ts2;
    ts2.tv_sec = 0;
    ts2.tv_nsec = 11 * MILLISECOND;
    int value2 = 0;
    flml_timer_key key2 = flml_timer_register(flml, &ts2, set_to_83_cb);
    flml_timer_set_ptr(flml, key2, &value2);

    sleep_millis(11);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);

    ck_assert_int_eq(value1, 42);
    ck_assert_int_eq(value2, 83);
    assert_no_next(&next);
}
END_TEST

START_TEST(trigger_with_one_expired_and_one_unexpired_timer) {
    struct timespec ts1;
    ts1.tv_sec = 0;
    ts1.tv_nsec = 6 * MILLISECOND;
    int value1 = 0;
    flml_timer_key key1 = flml_timer_register(flml, &ts1, set_to_42_cb);
    flml_timer_set_ptr(flml, key1, &value1);

    struct timespec ts2;
    ts2.tv_sec = 2 * WEEK;
    ts2.tv_nsec = 113 * MILLISECOND;
    int value2 = 0;
    flml_timer_key key2 = flml_timer_register(flml, &ts2, set_to_83_cb);
    flml_timer_set_ptr(flml, key2, &value2);

    sleep_millis(6);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);

    ck_assert_int_eq(value1, 42);
    ck_assert_int_eq(value2, 0);
    assert_next_before(&next, &ts2);
}
END_TEST

START_TEST(callback_is_invoked_periodically) {
    struct timespec interval;
    interval.tv_sec = 0;
    interval.tv_nsec = PERIODIC_MILLIS * MILLISECOND;

    int counter = 0;
    flml_timer_key key = flml_timer_register(flml, &interval, increment_cb);
    flml_timer_set_interval(flml, key, &interval);
    flml_timer_set_ptr(flml, key, &counter);

    for (int expected_counter = 0; expected_counter < 10; ++expected_counter) {
        struct timespec next;
        init_next(&next);
        flml_timers_trigger(flml, &next);
        ck_assert_int_eq(counter, expected_counter);
        assert_next_before(&next, &interval);
        sleep_timespec(&interval);
    }
}
END_TEST

START_TEST(trigger_after_half_interval_does_not_invoke_callback) {
    struct timespec interval;
    interval.tv_sec = 0;
    interval.tv_nsec = 50 * MILLISECOND;

    int counter = 0;
    flml_timer_key key = flml_timer_register(flml, &interval, increment_cb);
    flml_timer_set_interval(flml, key, &interval);
    flml_timer_set_ptr(flml, key, &counter);
    sleep_timespec(&interval);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);
    ck_assert_int_eq(counter, 1);
    assert_next_before(&next, &interval);

    struct timespec half_interval;
    half_interval.tv_sec = interval.tv_sec / 2;
    half_interval.tv_nsec = interval.tv_nsec / 2;
    sleep_timespec(&half_interval);

    flml_timers_trigger(flml, &next);
    ck_assert_int_eq(counter, 1);
    assert_next_before(&next, &half_interval);
}
END_TEST

START_TEST(many_consecutive_timers) {
    struct timespec expiry;
    expiry.tv_sec = 0;
    expiry.tv_nsec = 1 * MILLISECOND;

    int count = 16;
    int counter = 0;
    for (int i = 0; i < count; ++i) {
        flml_timer_key key = flml_timer_register(flml, &expiry, increment_cb);
        flml_timer_set_ptr(flml, key, &counter);
        sleep_timespec(&expiry);
        struct timespec next;
        flml_timers_trigger(flml, &next);
    }

    ck_assert_int_eq(counter, count);
}
END_TEST

struct in_order_timer {
    int expiry;
    int invoked;
};

#define IN_ORDER_TIMERS_MAX 8
#define IN_ORDER_GRANULARITY (5 * MILLISECOND)
struct in_order_test {
    size_t n_timers;
    struct in_order_timer timers[IN_ORDER_TIMERS_MAX];
    const struct in_order_timer *sorted[IN_ORDER_TIMERS_MAX];
};

static void add_in_order_timer(struct in_order_test *test, int expiry) {
    struct in_order_timer *to = test->timers + test->n_timers;
    to->expiry = expiry;
    to->invoked = 0;
    test->sorted[test->n_timers] = to;
    test->n_timers++;
}

static void add_in_order_timers(struct in_order_test *test,
                                int *expiries,
                                size_t n_expiries) {
    for (size_t i = 0; i < n_expiries; ++i) {
        add_in_order_timer(test, expiries[i]);
    }
}

static int compare_in_order_timers(const void *v1, const void *v2) {
    const struct in_order_timer *to1 = *(const struct in_order_timer **)v1;
    const struct in_order_timer *to2 = *(const struct in_order_timer **)v2;
    return to1->expiry - to2->expiry;
}

static void sort_in_order_timers(struct in_order_test *test) {
    qsort(test->sorted,
          test->n_timers,
          sizeof(*test->sorted),
          compare_in_order_timers);
}

static void init_in_order_test(struct in_order_test *test,
                               int *expiries,
                               size_t n_expiries) {
    test->n_timers = 0;
    add_in_order_timers(test, expiries, n_expiries);
    sort_in_order_timers(test);
}

static void mark_in_order_timer_as_invoked(struct flml *flml,
                                           flml_timer_key key) {
    struct in_order_timer *to = flml_timer_get_ptr(flml, key);
    to->invoked = 1;
}

static void register_in_order_timer(struct flml *flml,
                                    const struct in_order_timer *timer) {
    struct timespec expiry;
    expiry.tv_sec = 0;
    expiry.tv_nsec = timer->expiry * IN_ORDER_GRANULARITY;
    flml_timer_key key = flml_timer_register(
            flml, &expiry, mark_in_order_timer_as_invoked);
    flml_timer_set_ptr(flml, key, timer);
}

static void register_in_order_timers(struct flml *flml,
                                     const struct in_order_test *test) {
    for (size_t i = 0; i < test->n_timers; ++i) {
        const struct in_order_timer *to = test->timers + i;
        register_in_order_timer(flml, to);
    }
}

static void assert_invocations_in_order(struct flml *flml,
                                        const struct in_order_test *test) {
    for (size_t i = 0; i <= test->n_timers; ++i) {
        struct timespec next;
        init_next(&next);
        flml_timers_trigger(flml, &next);
        if (i > 0) {
            ck_assert(test->sorted[i - 1]->invoked);
        }
        if (i < test->n_timers) {
            ck_assert(!test->sorted[i]->invoked);
            sleep_timespec(&next);
        }
    }
}

static void run_in_order_test(struct flml *flml,
                              int *expiries,
                              size_t n_expiries) {
    struct in_order_test test;
    init_in_order_test(&test, expiries, n_expiries);
    register_in_order_timers(flml, &test);
    assert_invocations_in_order(flml, &test);
}

START_TEST(three_timers_are_invoked_in_order) {
    int expiries[] = {2, 5, 7};
    run_in_order_test(flml, expiries, 3);
}
END_TEST

START_TEST(four_timers_are_invoked_in_order) {
    int expiries[] = {6, 3, 4, 10};
    run_in_order_test(flml, expiries, 4);
}
END_TEST

START_TEST(five_timers_are_invoked_in_order) {
    int expiries[] = {11, 9, 7, 5, 3};
    run_in_order_test(flml, expiries, 5);
}
END_TEST

START_TEST(abs_overflow) {
    struct timespec ts;
    get_overflowing_timespec(&ts);

    flml_timer_key key = flml_timer_register(flml, &ts, dummy_cb);
    ck_assert(flml_timer_key_is_error(key));

    ck_assert_int_eq(flml_get_last_error(flml)->type, FLML_ERROR_OVERFLOW);
}
END_TEST

START_TEST(unregister_registered) {
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = 300 * MILLISECOND;

    int i = 5;
    flml_timer_key key = flml_timer_register(flml, &ts, set_to_42_cb);
    flml_timer_set_ptr(flml, key, &i);

    flml_timer_unregister(flml, key);
    sleep_millis(300);

    struct timespec next;
    init_next(&next);
    flml_timers_trigger(flml, &next);

    assert_no_next(&next);

    ck_assert_int_eq(i, 5);
}
END_TEST

START_TEST(get_ptr_with_zero_key_aborts) {
    flml_timer_get_ptr(flml, 0);
}
END_TEST

START_TEST(register_with_null_cb_aborts) {
    flml_timer_register(flml, &dummy_expiry, NULL);
}
END_TEST

static void register_with_expiry(long tv_sec, long tv_nsec) {
    struct timespec expiry = {tv_sec, tv_nsec};
    flml_timer_register(flml, &expiry, NULL);
}

START_TEST(register_with_negative_expiry_aborts) {
    register_with_expiry(-1, 10 * MILLISECOND);
}
END_TEST

START_TEST(register_with_negative_nsec_aborts) {
    register_with_expiry(2, -1);
}
END_TEST

START_TEST(register_with_too_large_nsec_aborts) {
    register_with_expiry(1, 1000000000);
}
END_TEST

START_TEST(get_ptr_with_non_existing_key_aborts) {
    flml_timer_get_ptr(flml, 1);
}
END_TEST

START_TEST(get_ptr_with_error_key_aborts) {
    flml_timer_get_ptr(flml, FLML_TIMER_KEY_ERROR);
}
END_TEST

START_TEST(get_ptr_with_unregistered_key_aborts) {
    flml_timer_key key = register_dummy_timer(flml);
    flml_timer_unregister(flml, key);
    flml_timer_get_ptr(flml, key);
}
END_TEST

START_TEST(set_invalid_interval_aborts) {
    flml_timer_key key = register_dummy_timer(flml);

    struct timespec interval = {-1, 0};
    flml_timer_set_interval(flml, key, &interval);
}
END_TEST

START_TEST(unregister_twice_aborts) {
    flml_timer_key key = register_dummy_timer(flml);

    flml_timer_unregister(flml, key);
    flml_timer_unregister(flml, key);
}
END_TEST

START_TEST(aborts_if_interval_overflows) {
    struct timespec interval;
    interval.tv_sec = LONG_MAX;
    interval.tv_nsec = 0;

    flml_timer_key key = register_dummy_timer(flml);
    flml_timer_set_interval(flml, key, &interval);
    sleep_timespec(&dummy_expiry);

    struct timespec next;
    flml_timers_trigger(flml, &next);
}
END_TEST

void add_tests(void) {
    ADD_TEST(register_fails_if_first_allocation_fails);
    ADD_TEST(register_fails_if_second_allocation_fails);
    ADD_TEST(register_and_unregister_repeatedly_does_not_allocate);
    ADD_TEST(register_returns_non_error_key);
    ADD_TEST(default_ptr_is_null);
    ADD_TEST(get_ptr);
    ADD_TEST(set_ptr_twice);
    ADD_TEST(trigger_without_timers);
    ADD_TEST(trigger_with_one_unexpired_timer);
    ADD_TEST(trigger_with_two_unexpired_timers);
    ADD_TEST(trigger_with_one_expired_timer);
    ADD_TEST(trigger_with_two_expired_timers);
    ADD_TEST(trigger_with_one_expired_and_one_unexpired_timer);
    ADD_TEST(callback_is_invoked_periodically);
    ADD_TEST(trigger_after_half_interval_does_not_invoke_callback);
    ADD_TEST(many_consecutive_timers);
    ADD_TEST(three_timers_are_invoked_in_order);
    ADD_TEST(four_timers_are_invoked_in_order);
    ADD_TEST(five_timers_are_invoked_in_order);
    ADD_TEST(abs_overflow);
    ADD_TEST(unregister_registered);
    ADD_ABORT_TEST(get_ptr_with_zero_key_aborts);
    ADD_ABORT_TEST(get_ptr_with_non_existing_key_aborts);
    ADD_ABORT_TEST(get_ptr_with_error_key_aborts);
    ADD_ABORT_TEST(get_ptr_with_unregistered_key_aborts);
    ADD_ABORT_TEST(register_with_null_cb_aborts);
    ADD_ABORT_TEST(register_with_negative_expiry_aborts);
    ADD_ABORT_TEST(register_with_negative_nsec_aborts);
    ADD_ABORT_TEST(register_with_too_large_nsec_aborts);
    ADD_ABORT_TEST(unregister_twice_aborts);
    ADD_ABORT_TEST(set_invalid_interval_aborts);
    ADD_ABORT_TEST(aborts_if_interval_overflows);
}
