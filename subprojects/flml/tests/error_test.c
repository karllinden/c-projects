/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>

#include <check-simpler.h>

#include "error.h"

#define N_TEST_ERRORS 6
#define SUFFICIENT_BUFFER_SIZE 2048
#define INSUFFICIENT_BUFFER_SIZE 5

struct test_error {
    struct flml_error error;
    const char *expected_message;
};

static struct test_error test_errors[N_TEST_ERRORS];

void set_up(void) {
    /* empty */
}

void tear_down(void) {
    /* empty */
}

static void init_test_errors(void) {
    test_errors[0].error.type = FLML_ERROR_NONE;
    test_errors[0].expected_message = "Success";

    flml_error_mem(&test_errors[1].error);
    test_errors[1].expected_message = "Could not allocate memory";

    flml_error_overflow(&test_errors[2].error);
    test_errors[2].expected_message = "Computation would overflow";

    flml_error_signal(&test_errors[3].error);
    test_errors[3].expected_message = "Could not set up signal handling";

    errno = EPERM;
    flml_error_system(&test_errors[4].error, "fopen");
    test_errors[4].expected_message = "fopen failed: Operation not permitted";

    errno = EIO;
    flml_error_system(&test_errors[5].error, "fwrite");
    test_errors[5].expected_message = "fwrite failed: Input/output error";
}

START_TEST(fill_with_mem) {
    struct flml_error error;
    flml_error_mem(&error);
    ck_assert_int_eq(error.type, FLML_ERROR_MEM);
}
END_TEST

START_TEST(fill_with_overflow) {
    struct flml_error error;
    flml_error_overflow(&error);
    ck_assert_int_eq(error.type, FLML_ERROR_OVERFLOW);
}
END_TEST

START_TEST(fill_with_signal) {
    struct flml_error error;
    flml_error_signal(&error);
    ck_assert_int_eq(error.type, FLML_ERROR_SIGNAL);
}
END_TEST

START_TEST(fill_with_system) {
    struct flml_error error;
    errno = 8;
    flml_error_system(&error, "some_system_call");
    ck_assert_int_eq(error.type, FLML_ERROR_SYSTEM);
    ck_assert_str_eq(error.system.call, "some_system_call");
    ck_assert_int_eq(error.system.err, 8);
}
END_TEST

static void test_snprint(const struct test_error *test_error, size_t size) {
    char buffer[size];
    int ret = flml_error_snprint(buffer, size, &test_error->error);

    char expected_buffer[size];
    strncpy(expected_buffer, test_error->expected_message, size - 1);
    expected_buffer[size - 1] = '\0';

    ck_assert_int_eq(ret, strlen(test_error->expected_message));
    ck_assert_str_eq(buffer, expected_buffer);
}

START_TEST(snprint_with_sufficient_size) {
    struct test_error *test_error = test_errors + _i;
    test_snprint(test_error, SUFFICIENT_BUFFER_SIZE);
}
END_TEST

START_TEST(snprint_with_exact_size) {
    struct test_error *test_error = test_errors + _i;
    size_t len = strlen(test_error->expected_message);
    test_snprint(test_error, len + 1);
}
END_TEST

START_TEST(snprint_with_just_too_small_size) {
    struct test_error *test_error = test_errors + _i;
    size_t len = strlen(test_error->expected_message);
    test_snprint(test_error, len);
}
END_TEST

START_TEST(snprint_with_insufficient_size) {
    struct test_error *test_error = test_errors + _i;
    test_snprint(test_error, INSUFFICIENT_BUFFER_SIZE);
}
END_TEST

START_TEST(snprint_with_invalid_error_type_aborts) {
    struct flml_error error;
    error.type = 79;

    char buffer[2048];
    flml_error_snprint(buffer, 2048, &error);
}
END_TEST

#if HAVE_FMEMOPEN
static void test_fprint(const struct flml_error *error,
                        size_t buffer_size,
                        int expected_return_value,
                        const char *expected_result) {
    char buf[buffer_size];
    FILE *file = fmemopen(buf, buffer_size, "w");
    ck_assert_ptr_ne(file, NULL);
    setbuf(file, NULL);

    int ret = flml_error_fprint(file, error);
    ck_assert_int_eq(ret, expected_return_value);

    fclose(file);

    if (expected_result != NULL) {
        ck_assert_str_eq(buf, expected_result);
    }
}

START_TEST(fprint_with_sufficient_space) {
    struct test_error *test_error = test_errors + _i;
    test_fprint(&test_error->error,
                SUFFICIENT_BUFFER_SIZE,
                strlen(test_error->expected_message),
                test_error->expected_message);
}
END_TEST

START_TEST(fprint_with_insufficient_space) {
    struct test_error *test_error = test_errors + _i;
    test_fprint(&test_error->error,
                INSUFFICIENT_BUFFER_SIZE,
                -strlen(test_error->expected_message),
                NULL);
}
END_TEST

START_TEST(fprint_with_too_long_error) {
#define BUFSIZE 128
    char function_name[BUFSIZE];
    memset(function_name, 'a', BUFSIZE);
    function_name[BUFSIZE - 1] = '\0';

    struct flml_error error;
    flml_error_system(&error, function_name);

    test_fprint(&error, SUFFICIENT_BUFFER_SIZE, BUFSIZE - 1, function_name);
#undef BUFSIZE
}
END_TEST
#endif // HAVE_FMEMOPEN

void add_tests(void) {
    init_test_errors();

    ADD_TEST(fill_with_mem);
    ADD_TEST(fill_with_overflow);
    ADD_TEST(fill_with_signal);
    ADD_TEST(fill_with_system);
    ADD_LOOP_TEST(snprint_with_sufficient_size, 0, N_TEST_ERRORS);
    ADD_LOOP_TEST(snprint_with_exact_size, 0, N_TEST_ERRORS);
    ADD_LOOP_TEST(snprint_with_just_too_small_size, 0, N_TEST_ERRORS);
    ADD_LOOP_TEST(snprint_with_insufficient_size, 0, N_TEST_ERRORS);
    ADD_ABORT_TEST(snprint_with_invalid_error_type_aborts);

#if HAVE_FMEMOPEN
    ADD_LOOP_TEST(fprint_with_sufficient_space, 0, N_TEST_ERRORS);
    ADD_LOOP_TEST(fprint_with_insufficient_space, 0, N_TEST_ERRORS);
    ADD_TEST(fprint_with_too_long_error);
#endif // HAVE_FMEMOPEN
}
