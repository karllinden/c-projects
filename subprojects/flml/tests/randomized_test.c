#include "randomized_test.h"

#include <stdlib.h>

#include <dima/exiting_on_failure.h>
#include <dima/system.h>
#include <loranga/advanced.h>
#include <panic.h>

static lra_generator_t *create_rng(dima_t *infallible_allocator) {
    lra_config_t *config = lra_new_config(infallible_allocator);
    panic_check(config != NULL);
    lra_enable_testing(config, stderr);

    struct lra_error error;
    lra_clear_error(&error);
    lra_generator_t *rng = lra_new_generator(config, &error);
    panic_check(rng != NULL);

    lra_destroy_config(config);
    return rng;
}

static void init_randomized_test(struct randomized_test *rt) {
    rt->infallible_allocator
            = dima_new_exiting_on_failure(dima_system_instance(), 2);
    panic_check(rt->infallible_allocator != NULL);

    rt->rng = create_rng(rt->infallible_allocator);
    rt->fallible_allocator
            = lra_new_allocator(rt->infallible_allocator, rt->rng);

    struct flml_error error;
    rt->flml = flml_new_with_dima(lra_allocator_to_dima(rt->fallible_allocator),
                                  &error);
    panic_check(rt->flml != NULL);
}

static void deinit_randomized_test(struct randomized_test *rt) {
    flml_destroy(rt->flml);
    lra_unref_allocator(rt->fallible_allocator);
    lra_destroy_generator(rt->rng);
    dima_unref(rt->infallible_allocator);
}

int main(void) {
    struct randomized_test rt;
    init_randomized_test(&rt);
    run_randomized_test(&rt);
    deinit_randomized_test(&rt);
    return EXIT_SUCCESS;
}
