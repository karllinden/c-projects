/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <panic.h>

#include "flml.h"

#include "randomized_test.h"

/**
 * Whether or not to report the iteration of the main loop.
 */
#define REPORT 1

/**
 * The number of calls to flml_fds_poll to make before exiting.
 */
#define N_POLLS 8192

/**
 * The maximum number of file descriptors.
 */
#define MAX_FDS 1000

/**
 * The number of file descriptors to replace if the main loop has
 * stalled, i.e. there are no more callbacks fired.
 */
#define STALL_REPLACE 2

/**
 * The rate (in percent) of creating a new pipe during a read or write
 * callback.
 */
#define CREATE_RATE 17

/**
 * The rate (in percent) of removing a struct ps_fd during a read or
 * write callback.
 */
#define REMOVE_RATE 24

/**
 * The rate (in percent) of unregistering the current ps_fd during a
 * callback.
 */
#define UNREGISTER_RATE 1

/**
 * Data structure for keeping track of file descriptors.
 */
struct ps_fd {
    /**
     * The file descriptor.
     */
    int fd;

    /**
     * The index in the ps_fdps array.
     */
    unsigned index;

    /**
     * The key of the flml file descriptor associated with this file descriptor,
     * or FLML_FD_KEY_ERROR if there is none.
     */
    flml_fd_key key;

    /**
     * A pointer to the next free ps_fd. Only used if this ps_fd is
     * free.
     */
    struct ps_fd *next_free;
};

/**
 * Data structure to use in this test.
 */
struct pollstress {
    dima_t *infallible_allocator;
    lra_generator_t *rng;
    lra_allocator_t *fallible_allocator;
    struct flml *flml;

    /**
     * An array containing all allocated pollstress file descriptors.
     * This array has MAX_FDS elements.
     */
    struct ps_fd *all_ps_fds;

    /**
     * A pointer to the first free pollstress file descriptor.
     */
    struct ps_fd *first_free_ps_fd;

    /**
     * An array of pointers to the pollstress file descriptors that are
     * currently in use.
     */
    struct ps_fd **ps_fdps;
    unsigned n_ps_fdps;

    /**
     * Number of calls to try_create_fd.
     */
    unsigned long n_try_create_fd_calls;

    /**
     * Number  of failed calls to try_create_fd.
     */
    unsigned long n_failed_try_create_fd_calls;

    /**
     * Total number of fds in the main loop.
     *
     * Used for computing the average number of file descriptors during
     * a main loop iteration.
     */
    unsigned long n_fds_total;

    /**
     * Maximum number of fds during an iteration.
     */
    unsigned n_fds_max;
};

static void event_cb(struct flml *, flml_fd_key, enum flml_fd_event);

static struct ps_fd *ps_fd_alloc(struct pollstress *ps) {
    struct ps_fd *ps_fd = ps->first_free_ps_fd;
    if (ps_fd != NULL) {
        ps->first_free_ps_fd = ps_fd->next_free;
    }
    return ps_fd;
}

static void ps_fd_free(struct pollstress *ps, struct ps_fd *ps_fd) {
    ps_fd->next_free = ps->first_free_ps_fd;
    ps->first_free_ps_fd = ps_fd;
}

static struct ps_fd *ps_fd_new(struct pollstress *ps,
                               int fd,
                               int create_flml_fd) {
    struct ps_fd *ps_fd;
    int index;

    ps_fd = ps_fd_alloc(ps);
    if (!ps_fd) {
        return NULL;
    }

    ps_fd->fd = fd;
    if (create_flml_fd) {
        ps_fd->key = flml_fd_register(ps->flml, fd, event_cb);
        if (flml_fd_key_is_error(ps_fd->key)) {
            ps_fd_free(ps, ps_fd);
            return NULL;
        }
        flml_fd_set_ptr(ps->flml, ps_fd->key, ps_fd);
    } else {
        ps_fd->key = FLML_FD_KEY_ERROR;
    }

    index = ps->n_ps_fdps++;
    ps_fd->index = index;
    ps->ps_fdps[index] = ps_fd;

    return ps_fd;
}

static void ps_fd_destroy(struct pollstress *ps, struct ps_fd *ps_fdp) {
    struct ps_fd *last_ps_fdp;
    int index = ps_fdp->index;

    last_ps_fdp = ps->ps_fdps[--ps->n_ps_fdps];
    ps->ps_fdps[index] = last_ps_fdp;
    last_ps_fdp->index = index;

    if (!flml_fd_key_is_error(ps_fdp->key)) {
        flml_fd_unregister(ps->flml, ps_fdp->key);
    }
    close(ps_fdp->fd);

    ps_fd_free(ps, ps_fdp);
}

static void try_create_fd(struct pollstress *ps) {
    int pipefd[2];
    int ret = pipe(pipefd);
    if (ret) {
        perror("pipe");
        abort();
    }

    int mode = 1 + lra_generate_uint(ps->rng, 2);
    int create_rd_flml_fd = mode & 0x1;
    int create_wr_flml_fd = mode & 0x2;

    struct ps_fd *rdfd = ps_fd_new(ps, pipefd[0], create_rd_flml_fd);
    struct ps_fd *wrfd = ps_fd_new(ps, pipefd[1], create_wr_flml_fd);

    ps->n_try_create_fd_calls++;
    if (rdfd == NULL || wrfd == NULL) {
        /* Failure. */
        ps->n_failed_try_create_fd_calls++;
        if (rdfd != NULL) {
            ps_fd_destroy(ps, rdfd);
        } else if (wrfd != NULL) {
            ps_fd_destroy(ps, wrfd);
        }
        close(pipefd[0]);
        close(pipefd[1]);
        return;
    }

    if (create_rd_flml_fd) {
        flml_fd_set_mode(ps->flml, rdfd->key, FLML_FD_MODE_READ);
    }

    if (create_wr_flml_fd) {
        flml_fd_set_mode(ps->flml, wrfd->key, FLML_FD_MODE_WRITE);
    }
}

static void remove_fd(struct pollstress *ps) {
    /*
     * Make sure there is a file descriptor to remove. Do not remove the
     * last file descriptor.
     */
    if (ps->n_ps_fdps <= 1) {
        return;
    }

    /* Randomize which file descriptor to remove. */
    unsigned index = lra_generate_uint(ps->rng, ps->n_ps_fdps - 1);
    ps_fd_destroy(ps, ps->ps_fdps[index]);
}

static void verify_flml_fd_key(const struct flml *flml, flml_fd_key key) {
    struct ps_fd *ps_fdp = flml_fd_get_ptr(flml, key);
    panic_check(ps_fdp->fd == flml_fd_get_fd(flml, key));
}

static void on_destroy_event(struct flml *flml, flml_fd_key key) {
    struct pollstress *ps = flml_get_ptr(flml);
    struct ps_fd *ps_fdp = flml_fd_get_ptr(flml, key);

    verify_flml_fd_key(flml, key);

    ps_fd_destroy(ps, ps_fdp);
}

static void randomize(struct flml *flml, flml_fd_key key) {
    struct pollstress *ps = flml_get_ptr(flml);

    /* It is important to unregister before removing randomly. */
    if (lra_generate_uint(ps->rng, 99) <= UNREGISTER_RATE) {
        flml_fd_set_mode(flml, key, 0);
    }

    if (lra_generate_uint(ps->rng, 99) <= CREATE_RATE) {
        try_create_fd(ps);
    }
    if (lra_generate_uint(ps->rng, 99) <= REMOVE_RATE) {
        remove_fd(ps);
    }
}

static void on_read_event(struct flml *flml, flml_fd_key key) {
    verify_flml_fd_key(flml, key);

    /* Read a byte from the file descriptor. */
    char byte;
    ssize_t ret = read(flml_fd_get_fd(flml, key), &byte, 1);
    panic_check(ret == 1);

    randomize(flml, key);
}

static void on_write_event(struct flml *flml, flml_fd_key key) {
    verify_flml_fd_key(flml, key);

    /* Write a random byte to the file descriptor. */
    struct pollstress *ps = flml_get_ptr(flml);
    char byte = CHAR_MIN + lra_generate_uint(ps->rng, CHAR_MAX - CHAR_MIN);
    ssize_t ret = write(flml_fd_get_fd(flml, key), &byte, 1);
    if (ret < 0 && errno == EPIPE) {
        struct ps_fd *ps_fdp = flml_fd_get_ptr(flml, key);
        ps_fd_destroy(ps, ps_fdp);
        return;
    }

    randomize(flml, key);
}

static void event_cb(struct flml *flml,
                     flml_fd_key key,
                     enum flml_fd_event event) {
    switch (event) {
        case FLML_FD_EVENT_CLOSED:
        case FLML_FD_EVENT_ERROR:
        case FLML_FD_EVENT_HANGUP:
            on_destroy_event(flml, key);
            break;
        case FLML_FD_EVENT_READ:
            on_read_event(flml, key);
            break;
        case FLML_FD_EVENT_WRITE:
            on_write_event(flml, key);
            break;
    }
}

static void init(struct pollstress *ps, const struct randomized_test *test) {
    ps->infallible_allocator = test->infallible_allocator;
    ps->rng = test->rng;
    ps->fallible_allocator = test->fallible_allocator;
    ps->flml = test->flml;
    flml_set_ptr(ps->flml, ps);

    /*
     * Allocate the array of all pollstress file descriptors and link
     * each one of them into the list of free ones.
     */
    struct ps_fd *all
            = dima_alloc_array(ps->infallible_allocator, MAX_FDS, sizeof(*all));
    ps->all_ps_fds = all;
    ps->first_free_ps_fd = all;
    for (unsigned i = 0; i < MAX_FDS - 1; ++i) {
        all[i].next_free = &all[i + 1];
    }
    all[MAX_FDS - 1].next_free = NULL;

    ps->ps_fdps = dima_alloc_array(
            ps->infallible_allocator, MAX_FDS, sizeof(struct ps_fd *));
    ps->n_ps_fdps = 0;

    ps->n_try_create_fd_calls = 0;
    ps->n_failed_try_create_fd_calls = 0;

    ps->n_fds_total = 0;
    ps->n_fds_max = 0;
}

static void deinit(struct pollstress *ps) {
    while (ps->n_ps_fdps) {
        ps_fd_destroy(ps, *ps->ps_fdps);
    }
    dima_free(ps->infallible_allocator, ps->ps_fdps);
    dima_free(ps->infallible_allocator, ps->all_ps_fds);
}

static void handle_sigpipe(ecb_unused int signum) {
    /* Do nothing. */
}

static void set_sigpipe_handler(void) {
    struct sigaction action;
    int ret;

    memset(&action, 0, sizeof(action));
    action.sa_handler = &handle_sigpipe;
    ret = sigaction(SIGPIPE, &action, NULL);
    if (ret) {
        perror("sigaction");
        abort();
    }
}

static void print_report(struct pollstress *ps, unsigned iteration) {
    double avg = (double)ps->n_fds_total / iteration;

    printf("Iterations run:                %d\n", iteration);
    printf("Calls to try_create_fd:        %lu\n", ps->n_try_create_fd_calls);
    printf("Failed calls to try_create_fd: %lu\n",
           ps->n_failed_try_create_fd_calls);
    printf("Maximum number of fds:         %d\n", ps->n_fds_max);
    printf("Average number of fds:         %.2f\n", avg);
    printf("Current number of fds:         %d\n", ps->n_ps_fdps);
}

#if REPORT
static void unprint_report(void) {
    for (int i = 0; i < 6; ++i) {
        fputs("\033[K", stdout);
        fputs("\033[1A", stdout);
    }
}
#endif /* REPORT */

static void run_poll(struct pollstress *ps, ecb_unused int poll_index) {
#if REPORT
    print_report(ps, poll_index);
#endif /* REPORT */

    /* Update ps->n_fds_total. Make sure it does not overflow. */
    unsigned long new_total = ps->n_fds_total + ps->n_ps_fdps;
    if (new_total < ps->n_fds_total) {
        fprintf(stderr, "ps->n_fds_total would wrap around.\n");
        abort();
    }
    ps->n_fds_total = new_total;

    /* Update ps->n_fds_max. */
    if (ps->n_ps_fdps > ps->n_fds_max) {
        ps->n_fds_max = ps->n_ps_fdps;
    }

    int ret = flml_fds_poll(ps->flml, 0);
    if (ret == 0) {
        /*
         * Timed out, so there is nothing that can be done with the
         * file descriptors. Create a bunch of extra file
         * descriptors to break the stall and continue the stress.
         */
        for (int j = 0; j < STALL_REPLACE; ++j) {
            /*
             * Remove twice because try_create_fd create both read
             * and write end of the pipe.
             */
            remove_fd(ps);
            remove_fd(ps);
            try_create_fd(ps);
        }
    }

#if REPORT
    unprint_report();
#endif /* REPORT */
}

static void run_polls(struct pollstress *ps) {
    /* Fail randomly to trigger unusual code paths. */
    lra_set_allocator_failure_percentage(ps->fallible_allocator, 50);

    for (int i = 0; i < N_POLLS; ++i) {
        run_poll(ps, i);
    }
    print_report(ps, N_POLLS);
}

void run_randomized_test(const struct randomized_test *test) {
    set_sigpipe_handler();

    struct pollstress ps;
    init(&ps, test);
    run_polls(&ps);
    deinit(&ps);
}
