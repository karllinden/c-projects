/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <check-simpler.h>
#include <dima/countdown.h>
#include <dima/system.h>

#include <flml/fd.h>
#include <flml/flml.h>

#include "flml.h"

#define FD 815
#define PTR ((void *)440)
#define PTR2 ((void *)160)
#define N_MANY 128

struct test_pipe {
    int rdfd;
    int wrfd;
    struct test_pipe *next;
};

static dima_countdown_t *countdown;
static struct flml *flml;
static struct test_pipe *pipes;

static void close_fd(int *fdp) {
    if (*fdp >= 0) {
        close(*fdp);
        *fdp = -1;
    }
}

static void close_rdfd(struct test_pipe *p) {
    close_fd(&p->rdfd);
}

static void close_wrfd(struct test_pipe *p) {
    close_fd(&p->wrfd);
}

static struct test_pipe *create_pipe(void) {
    struct test_pipe *p = malloc(sizeof(*p));
    if (p == NULL) {
        perror("malloc");
        abort();
    }

    int pipefd[2];
    int ret = pipe(pipefd);
    if (ret) {
        perror("pipe");
        abort();
    }

    p->rdfd = pipefd[0];
    p->wrfd = pipefd[1];

    p->next = pipes;
    pipes = p;

    return p;
}

static void destroy_pipe(struct test_pipe *p) {
    close_rdfd(p);
    close_wrfd(p);
    free(p);
}

static void destroy_pipes(void) {
    while (pipes != NULL) {
        struct test_pipe *pipe = pipes;
        pipes = pipe->next;
        destroy_pipe(pipe);
    }
}

void set_up(void) {
    countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);

    struct flml_error error;
    flml = flml_new_with_dima(dima_from_countdown(countdown), &error);
    pipes = NULL;
}

void tear_down(void) {
    destroy_pipes();
    flml_destroy(flml);
    flml = NULL;
    dima_unref_countdown(countdown);
    countdown = NULL;
}

static void dummy_cb(ecb_unused struct flml *flml,
                     ecb_unused flml_fd_key key,
                     ecb_unused enum flml_fd_event event) {
    /* empty */
}

START_TEST(register_one) {
    flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);

    ck_assert(!flml_fd_key_is_error(key));
    ck_assert_int_eq(flml_get_last_error(flml)->type, FLML_ERROR_NONE);
}
END_TEST

START_TEST(register_many) {
    for (unsigned i = 0; i < N_MANY; ++i) {
        flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
        ck_assert_int_eq(key, i);
    }
}
END_TEST

START_TEST(register_unregister_register) {
    flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
    ck_assert_int_eq(key, 0);

    flml_fd_unregister(flml, key);

    key = flml_fd_register(flml, 0, dummy_cb);
    ck_assert_int_eq(key, 0);
}
END_TEST

START_TEST(register_register_unregister_register) {
    flml_fd_key key1 = flml_fd_register(flml, 0, dummy_cb);
    ck_assert_int_eq(key1, 0);

    flml_fd_key key2 = flml_fd_register(flml, 0, dummy_cb);
    ck_assert_int_eq(key2, 1);

    flml_fd_unregister(flml, key1);

    key1 = flml_fd_register(flml, 0, dummy_cb);
    ck_assert_int_eq(key1, 0);
}
END_TEST

START_TEST(empty_poll) {
    int ret = flml_fds_poll(flml, 50);
    ck_assert_int_eq(ret, 0);
}
END_TEST

START_TEST(get_fd) {
    flml_fd_key key = flml_fd_register(flml, FD, dummy_cb);
    int fd = flml_fd_get_fd(flml, key);
    ck_assert_int_eq(fd, FD);
}
END_TEST

START_TEST(default_ptr_is_null) {
    flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
    void *ptr = flml_fd_get_ptr(flml, key);
    ck_assert_ptr_eq(ptr, NULL);
}
END_TEST

START_TEST(set_ptr) {
    flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
    flml_fd_set_ptr(flml, key, PTR);
    void *ptr = flml_fd_get_ptr(flml, key);
    ck_assert_ptr_eq(ptr, PTR);
}
END_TEST

START_TEST(set_ptr_twice) {
    flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
    flml_fd_set_ptr(flml, key, PTR);
    flml_fd_set_ptr(flml, key, PTR2);
    void *ptr = flml_fd_get_ptr(flml, key);
    ck_assert_ptr_eq(ptr, PTR2);
}
END_TEST

START_TEST(unregister_error_aborts) {
    flml_fd_unregister(flml, FLML_FD_KEY_ERROR);
}
END_TEST

START_TEST(unregister_twice_aborts) {
    flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
    flml_fd_unregister(flml, key);
    flml_fd_unregister(flml, key);
}
END_TEST

START_TEST(get_fd_with_invalid_key_aborts) {
    flml_fd_get_fd(flml, 1337);
}
END_TEST

START_TEST(set_ptr_with_invalid_key_aborts) {
    flml_fd_set_ptr(flml, 20, flml);
}
END_TEST

START_TEST(get_ptr_with_invalid_key_aborts) {
    flml_fd_get_ptr(flml, 128);
}
END_TEST

START_TEST(register_many_fail) {
    dima_enable_countdown(countdown);

    int at_least_one_failed = 0;
    for (unsigned i = 0; i < N_MANY; ++i) {
        flml_fd_key key = flml_fd_register(flml, 0, dummy_cb);
        at_least_one_failed |= flml_fd_key_is_error(key);
    }
    ck_assert(at_least_one_failed);
    ck_assert_int_eq(flml_get_last_error(flml)->type, FLML_ERROR_MEM);
}
END_TEST

static void set_to_777_cb(struct flml *flml,
                          flml_fd_key key,
                          enum flml_fd_event event) {
    ck_assert_int_eq(event, FLML_FD_EVENT_ERROR);

    int *intp = flml_fd_get_ptr(flml, key);
    *intp = 777;
}

static void set_to_679_cb(struct flml *flml,
                          flml_fd_key key,
                          enum flml_fd_event event) {
    ck_assert_int_eq(event, FLML_FD_EVENT_HANGUP);

    int *intp = flml_fd_get_ptr(flml, key);
    *intp = 679;
}

static void set_to_46_cb(struct flml *flml,
                         flml_fd_key key,
                         enum flml_fd_event event) {
    ck_assert_int_eq(event, FLML_FD_EVENT_READ);

    int *intp = flml_fd_get_ptr(flml, key);
    *intp = 46;
}

static void set_to_722_cb(struct flml *flml,
                          flml_fd_key key,
                          enum flml_fd_event event) {
    ck_assert_int_eq(event, FLML_FD_EVENT_WRITE);

    int *intp = flml_fd_get_ptr(flml, key);
    *intp = 722;
}

static void increment_cb(struct flml *flml,
                         flml_fd_key key,
                         ecb_unused enum flml_fd_event event) {
    int *intp = flml_fd_get_ptr(flml, key);
    (*intp)++;
}

START_TEST(error) {
    /*
     * According to man 2 poll:
     *
     * POLLERR
     * Error condition (only returned in revents; ignored in events).
     * This bit is also set for a file descriptor referring to the write
     * end of a pipe when the read end has been closed.
     */
    struct test_pipe *p = create_pipe();
    close_rdfd(p);

    int val = 0;
    flml_fd_key key = flml_fd_register(flml, p->wrfd, set_to_777_cb);
    flml_fd_set_ptr(flml, key, &val);
    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_eq(ret, 1);
    ck_assert_int_eq(val, 777);
}
END_TEST

START_TEST(hangup) {
    /*
     * According to man 2 poll:
     *
     * POLLHUP
     * Hang up (only returned in revents; ignored in events). Note that
     * when reading from a channel such as a pipe or a stream socket,
     * this event merely indicates that the peer closed its end of the
     * channel. Subsequent reads from the channel will return 0 (end of
     * file) only after all outstanding data in  the channel has been
     * consumed
     */
    struct test_pipe *p = create_pipe();
    close_wrfd(p);

    int val = 0;
    flml_fd_key key = flml_fd_register(flml, p->rdfd, set_to_679_cb);
    flml_fd_set_ptr(flml, key, &val);
    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_eq(ret, 1);
    ck_assert_int_eq(val, 679);
}
END_TEST

START_TEST(calls_read_cb) {
    struct test_pipe *p = create_pipe();

    ssize_t wret = write(p->wrfd, "X", 1);
    if (wret != 1) {
        perror("write");
        abort();
    }

    int val = 0;
    flml_fd_key key = flml_fd_register(flml, p->rdfd, set_to_46_cb);
    flml_fd_set_ptr(flml, key, &val);
    flml_fd_set_mode(flml, key, FLML_FD_MODE_READ);

    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_eq(ret, 1);
    ck_assert_int_eq(val, 46);
}
END_TEST

START_TEST(read_without_mode) {
    struct test_pipe *p = create_pipe();

    ssize_t wret = write(p->wrfd, "X", 1);
    if (wret != 1) {
        perror("write");
        abort();
    }

    int val = 0;
    flml_fd_key key = flml_fd_register(flml, p->rdfd, set_to_46_cb);
    flml_fd_set_ptr(flml, key, &val);
    int ret = flml_fds_poll(flml, 50);
    ck_assert_int_eq(ret, 0);
    ck_assert_int_eq(val, 0);
}
END_TEST

START_TEST(calls_write_cb) {
    struct test_pipe *p = create_pipe();

    int val = 0;
    flml_fd_key key = flml_fd_register(flml, p->wrfd, set_to_722_cb);
    flml_fd_set_ptr(flml, key, &val);
    flml_fd_set_mode(flml, key, FLML_FD_MODE_WRITE);

    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_eq(ret, 1);
    ck_assert_int_eq(val, 722);
}
END_TEST

START_TEST(write_without_mode) {
    struct test_pipe *p = create_pipe();

    int val = 0;
    flml_fd_key key = flml_fd_register(flml, p->wrfd, set_to_722_cb);
    flml_fd_set_ptr(flml, key, &val);

    int ret = flml_fds_poll(flml, 50);
    ck_assert_int_eq(ret, 0);
    ck_assert_int_eq(val, 0);
}
END_TEST

static void register_many_cb(struct flml *flml,
                             flml_fd_key key,
                             ecb_unused enum flml_fd_event event) {
    int *count = flml_get_ptr(flml);

    for (unsigned i = 1; i <= N_MANY; ++i) {
        struct test_pipe *p = create_pipe();
        flml_fd_key key = flml_fd_register(flml, p->wrfd, increment_cb);
        flml_fd_set_ptr(flml, key, count);
        ck_assert_int_eq(key, i);
        flml_fd_set_mode(flml, key, FLML_FD_MODE_WRITE);
    }

    /* Unregister the original key to avoid triggering this callback again. */
    flml_fd_unregister(flml, key);
}

START_TEST(register_many_while_polling) {
    int count = 0;
    flml_set_ptr(flml, &count);

    struct test_pipe *p = create_pipe();

    flml_fd_key key = flml_fd_register(flml, p->wrfd, register_many_cb);
    flml_fd_set_mode(flml, key, FLML_FD_MODE_WRITE);

    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_eq(ret, 1);

    ret = flml_fds_poll(flml, -1);
    ck_assert_int_eq(ret, N_MANY);
    ck_assert_int_eq(count, N_MANY);
}
END_TEST

static void unregister_fd_cb(struct flml *flml,
                             flml_fd_key key,
                             ecb_unused enum flml_fd_event event) {
    flml_fd_key *other_key_ptr = flml_fd_get_ptr(flml, key);
    flml_fd_unregister(flml, *other_key_ptr);
}

START_TEST(unregister_while_polling) {
    struct test_pipe *p1 = create_pipe();
    struct test_pipe *p2 = create_pipe();

    int val = 331;
    flml_fd_key key1 = flml_fd_register(flml, p1->wrfd, set_to_722_cb);
    flml_fd_set_ptr(flml, key1, &val);
    flml_fd_set_mode(flml, key1, FLML_FD_MODE_WRITE);

    flml_fd_key key2 = flml_fd_register(flml, p2->wrfd, unregister_fd_cb);
    flml_fd_set_ptr(flml, key2, &key1);
    flml_fd_set_mode(flml, key2, FLML_FD_MODE_WRITE);

    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_gt(ret, 0);
    ck_assert_int_eq(val, 331);
}
END_TEST

static void test_set_mode_while_polling_cb(
        struct flml *flml,
        flml_fd_key key,
        ecb_unused enum flml_fd_event event) {
    flml_fd_key *other_key_ptr = flml_fd_get_ptr(flml, key);
    flml_fd_set_mode(flml, *other_key_ptr, 0);
}

START_TEST(set_mode_while_polling) {
    struct test_pipe *p1 = create_pipe();
    struct test_pipe *p2 = create_pipe();

    int val = 331;
    flml_fd_key key1 = flml_fd_register(flml, p1->wrfd, set_to_722_cb);
    flml_fd_set_ptr(flml, key1, &val);
    flml_fd_set_mode(flml, key1, FLML_FD_MODE_WRITE);

    flml_fd_key key2
            = flml_fd_register(flml, p2->wrfd, test_set_mode_while_polling_cb);
    flml_fd_set_ptr(flml, key2, &key1);
    flml_fd_set_mode(flml, key2, FLML_FD_MODE_WRITE);

    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_gt(ret, 0);
    ck_assert_int_eq(val, 331);
}
END_TEST

struct increment_and_unregister {
    int val; /* the value to increment */
    flml_fd_key key; /* the key to unregister */
};

static void increment_and_unregister_cb(struct flml *flml,
                                        flml_fd_key key,
                                        ecb_unused enum flml_fd_event event) {
    struct increment_and_unregister *iau = flml_fd_get_ptr(flml, key);

    iau->val++;
    flml_fd_unregister(flml, iau->key);
    iau->key = FLML_FD_KEY_ERROR;
}

START_TEST(cb_only_called_once) {
    struct test_pipe *p1 = create_pipe();
    int val = 0;
    flml_fd_key key1 = flml_fd_register(flml, p1->wrfd, set_to_777_cb);
    flml_fd_set_ptr(flml, key1, &val);
    flml_fd_set_mode(flml, key1, FLML_FD_MODE_WRITE);

    struct test_pipe *p2 = create_pipe();
    struct increment_and_unregister iau;
    iau.val = 0;
    iau.key = key1;

    flml_fd_key key2
            = flml_fd_register(flml, p2->wrfd, increment_and_unregister_cb);
    flml_fd_set_ptr(flml, key2, &iau);
    flml_fd_set_mode(flml, key2, FLML_FD_MODE_WRITE);

    int ret = flml_fds_poll(flml, -1);
    ck_assert_int_gt(ret, 0);

    ck_assert_int_eq(val, 0);
    ck_assert_int_eq(iau.val, 1);
    ck_assert_int_eq(iau.key, FLML_FD_KEY_ERROR);
}
END_TEST

START_TEST(nonblock) {
    struct test_pipe *p = create_pipe();

    struct flml_error error;
    error.type = FLML_ERROR_NONE;
    int ret = flml_fd_nonblock(p->rdfd, &error);
    ck_assert_int_eq(ret, 0);
    ck_assert_int_eq(error.type, FLML_ERROR_NONE);

    errno = 0;
    char c;
    ssize_t r = read(p->rdfd, &c, 1);
    ck_assert_int_eq(r, -1);
    ck_assert(errno == EAGAIN || errno == EWOULDBLOCK);
}
END_TEST

void add_tests(void) {
    ADD_TEST(register_one);
    ADD_TEST(register_many);
    ADD_TEST(register_unregister_register);
    ADD_TEST(register_register_unregister_register);
    ADD_TEST(empty_poll);
    ADD_TEST(get_fd);
    ADD_TEST(default_ptr_is_null);
    ADD_TEST(set_ptr);
    ADD_TEST(set_ptr_twice);
    ADD_ABORT_TEST(unregister_error_aborts);
    ADD_ABORT_TEST(unregister_twice_aborts);
    ADD_ABORT_TEST(get_fd_with_invalid_key_aborts);
    ADD_ABORT_TEST(set_ptr_with_invalid_key_aborts);
    ADD_ABORT_TEST(get_ptr_with_invalid_key_aborts);
    ADD_TEST(register_many_fail);
    ADD_TEST(error);
    ADD_TEST(hangup);
    ADD_TEST(calls_read_cb);
    ADD_TEST(read_without_mode);
    ADD_TEST(calls_write_cb);
    ADD_TEST(write_without_mode);
    ADD_TEST(register_many_while_polling);
    ADD_TEST(unregister_while_polling);
    ADD_TEST(set_mode_while_polling);
    ADD_TEST(cb_only_called_once);
    ADD_TEST(nonblock);
}
