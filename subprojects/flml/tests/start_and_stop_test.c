/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ecb.h>

#include "mltest.h"

int set_up(void) {
    return 0;
}

int before_main_loop(ecb_unused struct flml *flml) {
    return 0;
}

int after_main_loop(ecb_unused struct flml *flml) {
    return 0;
}

void interact(ecb_unused pid_t target) {
    /* empty */
}
