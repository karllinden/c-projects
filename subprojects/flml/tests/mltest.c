/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <ecb.h>

#include <flml/flml.h>
#include <flml/signal.h>
#include <flml/timer.h>

#include "mltest.h"

#define READY_SIGNAL SIGUSR1
#define QUIT_SIGNAL SIGTERM

static volatile sig_atomic_t child_ready = 0;

static void ready_cb(ecb_unused struct flml *flml,
                     ecb_unused flml_timer_key key) {
    pid_t ppid = getppid();
    int ret = kill(ppid, READY_SIGNAL);
    if (ret) {
        perror("kill");
        exit(KILL_FAILED);
    }
}

static void quit_cb(struct flml *flml,
                    ecb_unused int signum,
                    ecb_unused void *ptr) {
    flml_quit(flml);
}

static int child_main(void) {
    const struct timespec zero = {0, 0};
    int exit_status = FLML_FAILED;

    struct flml_error error;
    error.type = FLML_ERROR_NONE;

    struct flml *flml = flml_new(&error);
    if (!flml) {
        goto error;
    }

    /*
     * Signal that the child is ready as soon as the main loop starts.
     * As soon as the child has signaled that it is ready the parent can
     * continue working.
     */
    flml_timer_key key = flml_timer_register(flml, &zero, ready_cb);
    if (flml_timer_key_is_error(key)) {
        error = *flml_get_last_error(flml);
        goto error;
    }

    flml_signal_catch(QUIT_SIGNAL, quit_cb, NULL);

    int ret = before_main_loop(flml);
    if (ret) {
        error = *flml_get_last_error(flml);
        goto error;
    }

    ret = flml_run(flml, FLML_HANDLE_SIGNALS);

    exit_status = after_main_loop(flml);
    if (ret || exit_status) {
        error = *flml_get_last_error(flml);
        goto error;
    }

    if (exit_status == FLML_FAILED) {
    error:
        flml_error_fprint(stderr, &error);
        fputc('\n', stderr);
    }

    flml_destroy(flml);
    return exit_status;
}

static void child_ready_cb(ecb_unused int signum) {
    child_ready = 1;
}

static int wait_for_child(pid_t cpid) {
    pid_t ret;
    int wstatus;

    do {
        ret = waitpid(cpid, &wstatus, 0);
    } while ((ret >= 0 || (ret < 0 && errno == EINTR)) && !WIFEXITED(wstatus)
             && !WIFSIGNALED(wstatus));
    if (ret < 0) {
        perror("waitpid");
        return WAITPID_FAILED;
    } else if (WIFEXITED(wstatus)) {
        return WEXITSTATUS(wstatus);
    } else {
        fprintf(stderr, "Child terminated by signal %d.\n", WTERMSIG(wstatus));
        return TERMINATED_BY_SIGNAL;
    }
}

static void wait_until_child_is_ready(void) {
    sigset_t newset;
    sigset_t oldset;

    /* Temporarily block the ready signal to prevent a race. */
    sigemptyset(&newset);
    sigaddset(&newset, READY_SIGNAL);
    sigprocmask(SIG_BLOCK, &newset, &oldset);

    /* As long as the ready signal has not been caught, wait for it. */
    while (!child_ready) {
        sigsuspend(&oldset);
    }

    /* Restore the old signal mask. */
    sigprocmask(SIG_SETMASK, &oldset, NULL);
}

static int parent_main(pid_t cpid) {
    int ret;

    wait_until_child_is_ready();

    interact(cpid);

    /* Request that the child quits. */
    ret = kill(cpid, QUIT_SIGNAL);
    if (ret) {
        perror("kill");
        return KILL_FAILED;
    }

    return wait_for_child(cpid);
}

static int handle_ready_signal(void) {
    struct sigaction sa;
    int ret;

    sa.sa_handler = child_ready_cb;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    ret = sigaction(READY_SIGNAL, &sa, NULL);
    if (ret) {
        perror("sigaction");
        return SIGACTION_FAILED;
    }

    return 0;
}

void _mltest_assert(int condition, const char *msg) {
    if (!condition) {
        fprintf(stderr, "mltest_assert failed: %s\n", msg);
        exit(ASSERT_FAILED);
    }
}

int main(void) {
    pid_t cpid;
    int ret;

    ret = set_up();
    if (ret) {
        return SET_UP_FAILED;
    }

    ret = handle_ready_signal();
    if (ret) {
        return ret;
    }

    cpid = fork();
    if (cpid < 0) {
        perror("fork");
        return FORK_FAILED;
    } else if (cpid == 0) {
        return child_main();
    } else {
        return parent_main(cpid);
    }
}
