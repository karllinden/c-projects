#ifndef RANDOMIZED_TEST_H
#define RANDOMIZED_TEST_H

#include <dima/dima.h>
#include <flml/flml.h>
#include <loranga/allocator.h>

struct randomized_test {
    dima_t *infallible_allocator;
    lra_generator_t *rng;
    lra_allocator_t *fallible_allocator;
    struct flml *flml;
};

void run_randomized_test(const struct randomized_test *test);

#endif // RANDOMIZED_TEST_H
