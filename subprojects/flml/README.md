# Fast Light Main Loop

Author: Karl Linden

Report bugs to: https://gitlab.com/karllinden/c-projects/issues

Homepage: https://gitlab.com/karllinden/c-projects

## Introduction

FLML (Fast Light Main Loop) is a fast lightweight poll based main loop with
support for convenient file descriptor management, timers and synchronous
receival of signals.

## Installation

Install flml from source:

```
meson build --buildtype=release
ninja -C build test
sudo ninja -C build install
```
