/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <dima/system.h>

#include <flml/flml.h>
#include <flml/signal.h>

#include "flml.h"
#include "sig.h"
#include "timespec.h"

static int flml_init(struct flml *flml, dima_t *dima) {
    flml->last_error.type = FLML_ERROR_NONE;
    flml->dima = dima;
    flml->ptr = NULL;
    flml->run = 0;

    int ret = flml_fds_init(flml);
    flml_timers_init(&flml->timers);

    return ret;
}

static void flml_deinit(struct flml *flml) {
    flml_fds_deinit(flml);
    flml_timers_deinit(flml, &flml->timers);
}

static int run_iteration(struct flml *flml) {
    struct timespec next = {-1, -1};
    flml_timers_trigger(flml, &next);

    int millis = (next.tv_sec >= 0) ? flml_timespec_to_millis(&next) : -1;

    int ret = flml_fds_poll(flml, millis);
    return ret < 0;
}

static int run(struct flml *flml) {
    int failed = 0;
    flml->run = 1;
    while (flml->run && !failed) {
        failed = run_iteration(flml);
    }
    flml->run = 0;
    return failed;
}

FLML_PUBLIC struct flml *flml_new(struct flml_error *error) {
    return flml_new_with_dima(dima_system_instance(), error);
}

FLML_PUBLIC struct flml *flml_new_with_dima(dima_t *dima,
                                            struct flml_error *error) {
    struct flml *flml = dima_alloc(dima, sizeof(*flml));
    if (flml == NULL) {
        flml_error_mem(error);
        return NULL;
    }

    int ret = flml_init(flml, dima);
    if (ret) {
        *error = flml->last_error;
        dima_free(dima, flml);
        return NULL;
    }

    return flml;
}

FLML_PUBLIC void flml_destroy(struct flml *flml) {
    if (flml) {
        flml_deinit(flml);
        flml_free(flml, flml);
    }
}

FLML_PUBLIC void flml_set_ptr(struct flml *flml, const void *ptr) {
    flml->ptr = (void *)ptr;
}

FLML_PUBLIC void *flml_get_ptr(const struct flml *flml) {
    return flml->ptr;
}

FLML_PUBLIC const struct flml_error *flml_get_last_error(
        const struct flml *flml) {
    return &flml->last_error;
}

FLML_PUBLIC int flml_run(struct flml *flml, int flags) {
    int handle_signals = flags & FLML_HANDLE_SIGNALS;
    if (handle_signals) {
        flml_signal_ignore(SIGPIPE);

        int ret = flml_set_up_self_pipe(flml);
        if (ret) {
            return 1;
        }
    }

    int failed = run(flml);

    if (handle_signals) {
        flml_tear_down_self_pipe(flml);
    }
    return failed;
}

FLML_PUBLIC void flml_quit(struct flml *flml) {
    flml->run = 0;
}
