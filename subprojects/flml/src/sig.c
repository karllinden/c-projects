/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <limits.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>

#include <panic.h>

#include <flml/signal.h>

#include "flml.h"
#include "sig.h"

/**
 * The number of signal handlers.
 */
#define N_HANDLERS 32

/**
 * A struct handler that represents an absent handler.
 */
#define NO_HANDLER \
    { NULL, NULL }

#define RD 0
#define WR 1

/**
 * Structure for holding a single signal handler.
 */
struct handler {
    /**
     * The callback to invoke when the signal has been caught.
     */
    flml_signal_cb *cb;

    /**
     * The pointer to pass to the callback.
     */
    void *ptr;
};

/**
 * The mutex that protects handlers (read+write), read_fd_key (read+write)
 * and write_fd (write).
 */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * The signal handlers.
 *
 * For each signum < N_HANDLERS, handlers[signum] holds the signal
 * handler for signum.
 */
static struct handler handlers[N_HANDLERS] = {
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 0-3 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 4-7 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 8-11 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 12-15 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 16-19 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 20-23 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER, /* 24-27 */
        NO_HANDLER, NO_HANDLER, NO_HANDLER, NO_HANDLER /* 28-31 */
};

/**
 * The flml_fd_key for the read end of the self-pipe.
 *
 * Used for receiving signal synchrously using the main loop.
 */
static flml_fd_key read_fd_key = FLML_FD_KEY_ERROR;

/**
 * The write end of the self-pipe.
 *
 * Used in the signal handler to write the number of the signal, so that
 * the correct callback can be invoked synchronously from the main loop.
 *
 * Since sig_atomic_t might be unsigned the special value that denotes
 * no file descriptor is 0.
 */
#define NO_WRITE_FD 0
static volatile sig_atomic_t write_fd = NO_WRITE_FD;

/**
 * The number of signals that have been missed due to write(2) failing.
 *
 * This variable is never used besides for the increment in
 * handle_signal. It only exists to remedy the warning when the return
 * value of write(2) is ignored.
 */
static volatile sig_atomic_t n_missed_signals = 0;

static int is_signal_number_valid(int signum) {
    return signum > 0 && signum < N_HANDLERS;
}

static void validate_signal_number(int signum) {
    if (!is_signal_number_valid(signum)) {
        panic("invalid signal number %d", signum);
    }
}

static struct handler *get_handler(int signum) {
    validate_signal_number(signum);
    return handlers + signum;
}

static void handle_signal(int signum) {
    int fd = (int)write_fd;

    if (is_signal_number_valid(signum) && fd != NO_WRITE_FD) {
        char c = (char)signum;
        ssize_t w = write(fd, &c, 1);
        if (w <= 0) {
            n_missed_signals++;
        }
    }
}

static void copy_handler(struct handler *dest, const struct handler *src) {
    memcpy(dest, src, sizeof(*dest));
}

static void flml_sigaction(int signum,
                           const struct handler *handler,
                           void (*signal_handler)(int)) {
    pthread_mutex_lock(&mutex);

    struct handler *global_handler = get_handler(signum);
    copy_handler(global_handler, handler);

    struct sigaction sa;
    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction(signum, &sa, NULL);

    pthread_mutex_unlock(&mutex);
}

static void invoke_handler(struct flml *flml,
                           int signum,
                           const struct handler *handler) {
    if (handler->cb != NULL) {
        handler->cb(flml, signum, handler->ptr);
    }
}

static int read_signal_number(struct flml *flml) {
    char signum;
    ssize_t r = read(flml_fd_get_fd(flml, read_fd_key), &signum, 1);
    return r > 0 ? signum : 0;
}

static void read_and_dispatch(struct flml *flml) {
    struct handler handler = NO_HANDLER;

    pthread_mutex_lock(&mutex);

    int signum = read_signal_number(flml);
    const struct handler *global_handler = get_handler(signum);
    copy_handler(&handler, global_handler);

    pthread_mutex_unlock(&mutex);

    invoke_handler(flml, signum, &handler);
}

static void read_fd_event_cb(struct flml *flml,
                             ecb_unused flml_fd_key key,
                             enum flml_fd_event event) {
    if (event == FLML_FD_EVENT_READ) {
        read_and_dispatch(flml);
    } else {
        /* This is very strange and should never happen... */
        fprintf(stderr, "flml: unexpected event %d on self-pipe\n", event);
        flml_tear_down_self_pipe(flml);
    }
}

static int is_self_pipe_setup(void) {
    int result = (write_fd != NO_WRITE_FD);
    assert(result == !flml_fd_key_is_error(read_fd_key));
    return result;
}

static void tear_down_self_pipe(struct flml *flml) {
    if (is_self_pipe_setup()) {
        close((int)write_fd);
        close(flml_fd_get_fd(flml, read_fd_key));

        flml_fd_unregister(flml, read_fd_key);

        write_fd = NO_WRITE_FD;
        read_fd_key = FLML_FD_KEY_ERROR;
    }
}

static int configure_self_pipe(int pipefd[2], struct flml_error *error) {
    int ret = flml_fd_nonblock(pipefd[0], error)
           || flml_fd_nonblock(pipefd[1], error);
    if (ret) {
        return 1;
    }

    if (pipefd[WR] < SIG_ATOMIC_MIN || pipefd[WR] > SIG_ATOMIC_MAX) {
        flml_error_signal(error);
        return 1;
    }

    return 0;
}

static int set_up_self_pipe_locked(struct flml *flml, int pipefd[2]) {
    if (is_self_pipe_setup()) {
        flml_error_signal(&flml->last_error);
        return 1;
    }

    read_fd_key = flml_fd_register(flml, pipefd[RD], read_fd_event_cb);
    if (flml_fd_key_is_error(read_fd_key)) {
        return 1;
    }

    flml_fd_set_mode(flml, read_fd_key, FLML_FD_MODE_READ);

    assert(pipefd[WR] != NO_WRITE_FD);
    write_fd = (sig_atomic_t)pipefd[WR];

    return 0;
}

static int set_up_self_pipe(struct flml *flml, int pipefd[2]) {
    int retval;

    pthread_mutex_lock(&mutex);
    retval = set_up_self_pipe_locked(flml, pipefd);
    pthread_mutex_unlock(&mutex);

    return retval;
}

int flml_set_up_self_pipe(struct flml *flml) {
    int pipefd[2];
    int ret;

    ret = pipe(pipefd);
    if (ret < 0) {
        flml_error_system(&flml->last_error, "pipe");
        return 1;
    } else if (configure_self_pipe(pipefd, &flml->last_error)
               || set_up_self_pipe(flml, pipefd)) {
        close(pipefd[0]);
        close(pipefd[1]);
        return 1;
    }

    return 0;
}

void flml_tear_down_self_pipe(struct flml *flml) {
    pthread_mutex_lock(&mutex);
    tear_down_self_pipe(flml);
    pthread_mutex_unlock(&mutex);
}

FLML_PUBLIC void flml_signal_default(int signum) {
    struct handler handler = NO_HANDLER;
    flml_sigaction(signum, &handler, SIG_DFL);
}

FLML_PUBLIC void flml_signal_catch(int signum,
                                   flml_signal_cb *cb,
                                   const void *ptr) {
    struct handler handler = {cb, (void *)ptr};
    flml_sigaction(signum, &handler, handle_signal);
}

FLML_PUBLIC void flml_signal_ignore(int signum) {
    struct handler handler = NO_HANDLER;
    flml_sigaction(signum, &handler, SIG_IGN);
}
