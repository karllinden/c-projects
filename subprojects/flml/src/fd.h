/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FD_H
#define FD_H

#include <poll.h>

#include <flml/fd.h>

#define N_BUFFERS 3

struct fd_priv;

struct fd_buffer {
    flml_fd_key *indices;
    struct fd_priv *privs;
    struct pollfd *pollfds;
    unsigned size;
    unsigned capacity;
};

struct fds {
    struct fd_buffer *current_buffer;
    struct fd_buffer *poll_buffer;
    struct fd_buffer buffers[N_BUFFERS];
};

int flml_fds_init(struct flml *flml);

void flml_fds_deinit(struct flml *flml);

/**
 * Poll the file descriptors and invoke appropriate callbacks.
 *
 * @param flml a pointer to the flml structure
 * @param timeout the timeout to pass to poll(2)
 * @return a positive number on success, 0 if the call times out or was
 *         interrupted, or a negative value on failure, in which case the last
 *         error is filled in appropriately
 */
int flml_fds_poll(struct flml *flml, int timeout);

#endif /* !FD_H */
