/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <limits.h>
#include <string.h>

#include "timespec.h"

#define MILLION (1000 * 1000)
#define BILLION (MILLION * 1000)

#define TIME_MAX LONG_MAX

static int timespec_is_valid(const struct timespec *ts) {
    return ts->tv_nsec >= 0 && ts->tv_nsec < BILLION;
}

void flml_timespec_copy(struct timespec *dest, const struct timespec *src) {
    memcpy(dest, src, sizeof(*dest));
}

int flml_timespec_is_non_negative(const struct timespec *ts) {
    return timespec_is_valid(ts) && ts->tv_sec >= 0;
}

int flml_timespec_add_non_negative(struct timespec *restrict augend,
                                   const struct timespec *addend) {
    assert(timespec_is_valid(augend));
    assert(flml_timespec_is_non_negative(addend));

    if (TIME_MAX - addend->tv_sec < augend->tv_sec) {
        return 1;
    }
    augend->tv_sec += addend->tv_sec;

    augend->tv_nsec += addend->tv_nsec;
    if (augend->tv_nsec >= BILLION) {
        augend->tv_nsec -= BILLION;
        if (augend->tv_sec == TIME_MAX) {
            return 1;
        }
        augend->tv_sec++;
    }

    return 0;
}

void flml_timespec_difference(struct timespec *restrict result,
                              const struct timespec *left,
                              const struct timespec *right) {
    result->tv_sec = left->tv_sec - right->tv_sec;
    if (left->tv_nsec >= right->tv_nsec) {
        result->tv_nsec = left->tv_nsec - right->tv_nsec;
    } else {
        result->tv_sec--;
        result->tv_nsec = left->tv_nsec + (BILLION - right->tv_nsec);
    }
}

int flml_timespec_compare(const struct timespec *ts1,
                          const struct timespec *ts2) {
#define CMP(a, b) ((a > b) - (a < b))
    return 2 * CMP(ts1->tv_sec, ts2->tv_sec) + CMP(ts1->tv_nsec, ts2->tv_nsec);
#undef CMP
}

int flml_timespec_to_millis(const struct timespec *ts) {
    int millis_from_sec;
    int millis_from_nsec;

    assert(flml_timespec_is_non_negative(ts));

    millis_from_sec
            = (ts->tv_sec <= INT_MAX / 1000) ? ts->tv_sec * 1000 : INT_MAX;

    millis_from_nsec = ts->tv_nsec / MILLION;

    /* Round up. */
    if (ts->tv_nsec % MILLION && millis_from_nsec < INT_MAX) {
        millis_from_nsec++;
    }

    return (millis_from_sec <= INT_MAX - millis_from_nsec)
                 ? millis_from_sec + millis_from_nsec
                 : INT_MAX;
}
