/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <panic.h>

#include <flml/fd.h>

#include "fd.h"
#include "flml.h"

/*
 * The initial capacity must be a power of 2, or the overflow checking won't
 * work.
 */
#define INITIAL_CAPACITY 8

#define get_fds(flml) (&flml->fds)

struct fd_priv {
    void *ptr;
    flml_fd_event_cb *event_cb;
    flml_fd_key key;
};

struct resolution {
    struct fd_buffer *buffer;
    flml_fd_key key;
    flml_fd_key index;
    struct fd_priv *priv;
    struct pollfd *pollfd;
};

struct invoke_data {
    struct flml *flml;
    struct pollfd *pollfd;
    flml_fd_event_cb *event_cb;
    flml_fd_key key;
};

static void deinit_buffer(struct flml *flml, struct fd_buffer *buffer) {
    flml_free(flml, buffer->indices);
    flml_free(flml, buffer->privs);
    flml_free(flml, buffer->pollfds);
    memset(buffer, 0, sizeof(*buffer));
}

static int is_buffer_unused(const struct fds *fds,
                            const struct fd_buffer *buffer) {
    return buffer != fds->current_buffer //
        && buffer != fds->poll_buffer;
}

static struct fd_buffer *find_unused_buffer(struct fds *fds) {
    for (int i = 0; i < N_BUFFERS; ++i) {
        struct fd_buffer *buffer = fds->buffers + i;
        if (is_buffer_unused(fds, buffer)) {
            return buffer;
        }
    }

    /*
     * There are three buffers, but only two pointers (current_buffer and
     * poll_buffer) that can use them, so at least one buffer must always be
     * unused.
     */
    panic("all buffers are used");
}

static void fd_priv_clear(struct fd_priv *priv, flml_fd_key key) {
    priv->ptr = NULL;
    priv->event_cb = NULL;
    priv->key = key;
}

static void pollfd_clear(struct pollfd *pollfd) {
    pollfd->fd = -1;
    pollfd->events = 0;
    pollfd->revents = 0;
}

static void init_indices(flml_fd_key *indices, unsigned capacity) {
    for (unsigned i = 0; i < capacity; ++i) {
        indices[i] = FLML_FD_KEY_ERROR;
    }
}

static flml_fd_key *create_indices(struct flml *flml, unsigned capacity) {
    flml_fd_key *result = flml_alloc_array(flml, capacity, sizeof(*result));
    if (result != NULL) {
        init_indices(result, capacity);
    }
    return result;
}

static void init_privs(struct fd_priv *privs, unsigned capacity) {
    for (unsigned i = 0; i < capacity; ++i) {
        fd_priv_clear(privs + i, i);
    }
}

static struct fd_priv *create_privs(struct flml *flml, unsigned capacity) {
    struct fd_priv *result = flml_alloc_array(flml, capacity, sizeof(*result));
    if (result != NULL) {
        init_privs(result, capacity);
    }
    return result;
}

static void init_pollfds(struct pollfd *pollfds, unsigned capacity) {
    for (unsigned i = 0; i < capacity; ++i) {
        pollfd_clear(pollfds + i);
    }
}

static struct pollfd *create_pollfds(struct flml *flml, unsigned capacity) {
    struct pollfd *result = flml_alloc_array(flml, capacity, sizeof(*result));
    if (result != NULL) {
        init_pollfds(result, capacity);
    }
    return result;
}

static int init_buffer(struct flml *flml,
                       struct fd_buffer *buffer,
                       unsigned capacity) {
    buffer->indices = create_indices(flml, capacity);
    buffer->privs = create_privs(flml, capacity);
    buffer->pollfds = create_pollfds(flml, capacity);
    if (buffer->indices == NULL //
        || buffer->privs == NULL //
        || buffer->pollfds == NULL) {
        deinit_buffer(flml, buffer);
        return 1;
    }

    buffer->size = 0;
    buffer->capacity = capacity;

    return 0;
}

static struct fd_buffer *create_buffer(struct flml *flml, unsigned capacity) {
    struct fds *fds = get_fds(flml);
    struct fd_buffer *buffer = find_unused_buffer(fds);
    int ret = init_buffer(flml, buffer, capacity);
    return ret == 0 ? buffer : NULL;
}

static void copy_buffer(struct fd_buffer *dest, const struct fd_buffer *src) {
    assert(dest->capacity >= src->capacity);
    memcpy(dest->indices, src->indices, src->capacity * sizeof(*src->indices));
    memcpy(dest->privs, src->privs, src->capacity * sizeof(*src->privs));
    memcpy(dest->pollfds, src->pollfds, src->capacity * sizeof(*src->pollfds));
    dest->size = src->size;
}

static void resolve_index(struct resolution *res) {
    if (res->key < res->buffer->capacity) {
        res->index = res->buffer->indices[res->key];
    } else {
        res->index = FLML_FD_KEY_ERROR;
    }
}

static int is_resolution_valid(const struct resolution *res) {
    return !flml_fd_key_is_error(res->index);
}

static void resolve_pointers(struct resolution *res) {
    if (is_resolution_valid(res)) {
        res->priv = res->buffer->privs + res->index;
        res->pollfd = res->buffer->pollfds + res->index;
    } else {
        res->priv = NULL;
        res->pollfd = NULL;
    }
}

static void resolve_key_in_buffer(struct fd_buffer *buffer,
                                  flml_fd_key key,
                                  struct resolution *res) {
    res->buffer = buffer;
    res->key = key;
    resolve_index(res);
    resolve_pointers(res);
}

static void resolve_last(struct fd_buffer *buffer, struct resolution *res) {
    assert(buffer->size > 0);

    res->buffer = buffer;
    res->index = buffer->size - 1;
    resolve_pointers(res);
    res->key = res->priv->key;

    assert(!flml_fd_key_is_error(res->key));
}

static void resolve_current_key(const struct flml *flml,
                                flml_fd_key key,
                                struct resolution *res) {
    const struct fds *fds = get_fds(flml);
    resolve_key_in_buffer(fds->current_buffer, key, res);
    if (!is_resolution_valid(res)) {
        panic("invalid flml_fd_key: %u", key);
    }
}

static void fd_priv_init(struct fd_priv *priv, flml_fd_event_cb *event_cb) {
    assert(priv->ptr == NULL);
    assert(priv->event_cb == NULL);
    priv->event_cb = event_cb;
}

static void pollfd_init(struct pollfd *pollfd, int fd) {
    assert(pollfd->fd == -1);
    assert(pollfd->events == 0);
    assert(pollfd->revents == 0);
    pollfd->fd = fd;
}

static int buffer_has_capacity(struct fd_buffer *buffer) {
    assert(buffer->size <= buffer->capacity);
    return buffer->size != buffer->capacity;
}

static int increase_current_buffer_capacity(struct flml *flml) {
    struct fds *fds = get_fds(flml);
    struct fd_buffer *old_buffer = fds->current_buffer;

    unsigned new_capacity = old_buffer->capacity * 2;
    if (new_capacity == 0) {
        flml_error_overflow(&flml->last_error);
        return 1;
    }

    struct fd_buffer *new_buffer = create_buffer(flml, new_capacity);
    if (new_buffer == NULL) {
        return 1;
    }

    copy_buffer(new_buffer, old_buffer);
    fds->current_buffer = new_buffer;

    if (old_buffer != fds->poll_buffer) {
        deinit_buffer(flml, old_buffer);
    }

    return 0;
}

static int ensure_current_buffer_capacity(struct flml *flml) {
    struct fds *fds = get_fds(flml);
    if (buffer_has_capacity(fds->current_buffer)) {
        return 0;
    } else {
        return increase_current_buffer_capacity(flml);
    }
}

static flml_fd_key register_fd(struct fd_buffer *buffer,
                               int fd,
                               flml_fd_event_cb *event_cb) {
    assert(buffer_has_capacity(buffer));

    buffer->size++;
    struct resolution res;
    resolve_last(buffer, &res);

    fd_priv_init(res.priv, event_cb);
    pollfd_init(res.pollfd, fd);

    flml_fd_key key = res.key;
    buffer->indices[key] = res.index;
    return key;
}

static void fd_priv_copy(struct fd_priv *dest, const struct fd_priv *src) {
    dest->ptr = src->ptr;
    dest->event_cb = src->event_cb;
    dest->key = src->key;
}

static void pollfd_copy(struct pollfd *dest, const struct pollfd *src) {
    memmove(dest, src, sizeof(*dest));
}

static void unregister_fd(const struct resolution *res,
                          ecb_unused const void *ptr) {
    struct fd_buffer *buffer = res->buffer;

    struct resolution last;
    resolve_last(buffer, &last);

    buffer->indices[last.key] = res->index;
    buffer->indices[res->key] = FLML_FD_KEY_ERROR;

    fd_priv_copy(res->priv, last.priv);
    fd_priv_clear(last.priv, res->key);

    pollfd_copy(res->pollfd, last.pollfd);
    pollfd_clear(last.pollfd);

    buffer->size--;
}

static void pollfd_set_flag(struct pollfd *pollfd, short flag, int set) {
    if (set) {
        pollfd->events |= flag;
    } else {
        pollfd->events &= ~flag;
        pollfd->revents &= ~flag;
    }
}

static void pollfd_set_mode(struct pollfd *pollfd, unsigned mode) {
    pollfd_set_flag(pollfd, POLLIN, mode & FLML_FD_MODE_READ);
    pollfd_set_flag(pollfd, POLLOUT, mode & FLML_FD_MODE_WRITE);
}

static void set_mode(const struct resolution *res, const void *ptr) {
    const unsigned *mode = ptr;
    pollfd_set_mode(res->pollfd, *mode);
}

static void test_and_invoke_event_callback(const struct invoke_data *data,
                                           short flag,
                                           enum flml_fd_event event) {
    if (data->pollfd->revents & flag) {
        data->pollfd->revents &= ~flag;
        data->event_cb(data->flml, data->key, event);
    }
}

static void invoke_event_callback(struct flml *flml,
                                  struct fd_priv *priv,
                                  struct pollfd *pollfd) {
    /*
     * The current priv and pollfd may be removed as a result of the callback,
     * so pollfd->revents may not be cached. However, if the current priv and
     * pollfd is removed, then pollfd->revents will for sure be 0, because
     * invoke_event_callbacks traverses the array from last to first.
     *
     * Because the compiler cannot deduce that priv->event_cb and priv->key will
     * not change as a result of the call through the function pointer, those
     * are cached to avoid fetching them again. The invocation data is stored on
     * the stack (and does not escape) so that the compiler can inline more
     * aggressively.
     */
    struct invoke_data data;
    data.flml = flml;
    data.pollfd = pollfd;
    data.event_cb = priv->event_cb;
    data.key = priv->key;

    assert(data.event_cb != NULL);

    test_and_invoke_event_callback(&data, POLLNVAL, FLML_FD_EVENT_CLOSED);
    test_and_invoke_event_callback(&data, POLLERR, FLML_FD_EVENT_ERROR);
    test_and_invoke_event_callback(&data, POLLHUP, FLML_FD_EVENT_HANGUP);
    test_and_invoke_event_callback(&data, POLLIN, FLML_FD_EVENT_READ);
    test_and_invoke_event_callback(&data, POLLOUT, FLML_FD_EVENT_WRITE);

    pollfd->revents = 0;
}

static void invoke_event_callbacks(struct flml *flml,
                                   struct fd_buffer *buffer) {
    /*
     * There are at least a few reasons for this strange way of looping.
     *
     * Firstly, this loop must have good performance, because it is called in
     * almost every iteration of the main loop. Because there is a call through
     * a function pointer, the compiler cannot know that buf->privs and
     * buf->pollfds are constant across the call through the function pointer.
     * Thus, pointers to the current element are stored and decremented to
     * prevent loading from the memory in buffer.
     *
     * Secondly, the iteration must be from last to first, because elements may
     * be removed during iteration, and when elements are removed the last
     * element is moved to an earlier position.
     */
    flml_fd_key index = buffer->size;
    struct fd_priv *priv = buffer->privs + index;
    struct pollfd *pollfd = buffer->pollfds + index;
    while (index) {
        index--;
        priv--;
        pollfd--;

        if (pollfd->revents) {
            invoke_event_callback(flml, priv, pollfd);
        }
    }
}

static int poll_buffer(struct flml *flml,
                       struct fd_buffer *buffer,
                       int timeout) {
    int ready = poll(buffer->pollfds, buffer->size, timeout);
    if (ready > 0) {
        invoke_event_callbacks(flml, buffer);
        return ready;
    } else if (ready == 0 || errno == EINTR) {
        /* Poll timed out or was interrupted. */
        return 0;
    } else {
        flml_error_system(&flml->last_error, "poll");
        return -1;
    }
}

int flml_fds_init(struct flml *flml) {
    struct fds *fds = get_fds(flml);

    memset(fds, 0, sizeof(*fds));
    fds->current_buffer = create_buffer(flml, INITIAL_CAPACITY);

    return fds->current_buffer == NULL;
}

void flml_fds_deinit(struct flml *flml) {
    struct fds *fds = get_fds(flml);

    /*
     * The flml structure should not be destroyed while polling so poll_buffer
     * should always be NULL here.
     */
    assert(fds->poll_buffer == NULL);

    deinit_buffer(flml, fds->current_buffer);
    fds->current_buffer = NULL;
}

int flml_fds_poll(struct flml *flml, int timeout) {
    struct fds *fds = get_fds(flml);
    assert(fds->poll_buffer == NULL);

    struct fd_buffer *buffer = fds->current_buffer;
    fds->poll_buffer = buffer;

    int retval = poll_buffer(flml, buffer, timeout);

    fds->poll_buffer = NULL;
    if (buffer != fds->current_buffer) {
        deinit_buffer(flml, buffer);
    }

    return retval;
}

typedef void for_each_buffer_fn(const struct resolution *res, const void *ptr);
static void for_each_buffer(struct flml *flml,
                            flml_fd_key key,
                            for_each_buffer_fn *fn,
                            const void *ptr) {
    struct resolution res;
    resolve_current_key(flml, key, &res);
    fn(&res, ptr);

    struct fds *fds = get_fds(flml);
    if (fds->poll_buffer != NULL && fds->poll_buffer != fds->current_buffer) {
        resolve_key_in_buffer(fds->poll_buffer, key, &res);
        if (is_resolution_valid(&res)) {
            fn(&res, ptr);
        }
    }
}

FLML_PUBLIC int flml_fd_nonblock(int fd, struct flml_error *error) {
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0 || fcntl(fd, F_SETFL, flags | O_NONBLOCK)) {
        flml_error_system(error, "fcntl");
        return 1;
    }

    return 0;
}

FLML_PUBLIC flml_fd_key flml_fd_register(struct flml *flml,
                                         int fd,
                                         flml_fd_event_cb *event_cb) {
    struct fds *fds = get_fds(flml);

    /* A NULL event_cb makes no sense. */
    assert(event_cb != NULL);

    int ret = ensure_current_buffer_capacity(flml);
    if (ret) {
        return FLML_FD_KEY_ERROR;
    }
    return register_fd(fds->current_buffer, fd, event_cb);
}

FLML_PUBLIC void flml_fd_unregister(struct flml *flml, flml_fd_key key) {
    for_each_buffer(flml, key, unregister_fd, NULL);
}

FLML_PUBLIC int flml_fd_get_fd(const struct flml *flml, flml_fd_key key) {
    struct resolution res;
    resolve_current_key(flml, key, &res);
    return res.pollfd->fd;
}

FLML_PUBLIC void flml_fd_set_ptr(struct flml *flml,
                                 flml_fd_key key,
                                 const void *ptr) {
    struct resolution res;
    resolve_current_key(flml, key, &res);
    res.priv->ptr = (void *)ptr;
}

FLML_PUBLIC void *flml_fd_get_ptr(const struct flml *flml, flml_fd_key key) {
    struct resolution res;
    resolve_current_key(flml, key, &res);
    return res.priv->ptr;
}

FLML_PUBLIC void flml_fd_set_mode(struct flml *flml,
                                  flml_fd_key key,
                                  unsigned mode) {
    for_each_buffer(flml, key, set_mode, &mode);
}
