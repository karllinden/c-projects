/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2019, 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FLML_TIMER_H_
#define _FLML_TIMER_H_

#include <limits.h>
#include <time.h>

#include <flml/error.h>
#include <flml/flml.h>

#define FLML_TIMER_KEY_ERROR UINT_MAX

/**
 * The type of the key that is associated to each timer in flml.
 *
 * Do not assume anything about this typedef. Use FLML_TIMER_KEY_ERROR,
 * flml_timer_key_is_error and the other flml_timer_* function for managing
 * objects of this type.
 */
typedef unsigned flml_timer_key;

/**
 * Timeout callback from the main loop.
 *
 * This is the type of the callback that is invoked whenever a timer expires.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the timer
 */
typedef void flml_timer_cb(struct flml *flml, flml_timer_key key);

/**
 * Register a new timer in the flml main loop.
 *
 * The callback given by cb will be issued after the time given by the expiry
 * has elapsed. The callback will only be invoked if the flml main loop is
 * running.
 *
 * By default the callback will only be invoked once. To invoke the timer
 * periodically with a fixed interval use flml_timer_set_interval.
 *
 * Although the timer can be specified with nanoseconds precision, flml
 * guarantees only milliseconds precision, but the timer may be subject to
 * jitter and delays.
 *
 * @param flml a pointer to the flml structure
 * @param expiry a pointer to the struct timespec that contains the time
 *               (relative to now) at which to invoke the callback
 * @param cb the callback to register
 * @return the key to the registered timer on success, or an flml_timer_key
 *         representing failure; see flml_timer_key_is_error
 */
flml_timer_key flml_timer_register(struct flml *flml,
                                   const struct timespec *expiry,
                                   flml_timer_cb *cb);

/**
 * Unregister the given timer.
 *
 * Unregistering a timer causes its callback to not be invoked when the timer
 * expires.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the timer to unregister, as returned by
 *            flml_timer_register
 */
void flml_timer_unregister(struct flml *flml, flml_timer_key key);

/**
 * Set the timer's user supplied pointer.
 *
 * The pointer can be accessed with flml_timer_get_ptr.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the timer, as returned by a successful call to
 *            flml_timer_register
 * @param ptr the user pointer to set
 */
void flml_timer_set_ptr(struct flml *flml, flml_timer_key key, const void *ptr);

/**
 * Get the pointer that was supplied to flml_timer_set_ptr.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the timer, as returned by a successful call to
 *            flml_timer_register
 * @return the pointer
 */
void *flml_timer_get_ptr(struct flml *flml, flml_timer_key key);

/**
 * Set the interval at which to periodically invoke the callback.
 *
 * @param flml a pointer to the flml structure
 * @param key the key of the timer, as returned by a successful call to
 *            flml_timer_register
 * @param interval the interval
 */
void flml_timer_set_interval(struct flml *flml,
                             flml_timer_key key,
                             const struct timespec *interval);

/**
 * Determine whether the given flml timer key represents an error.
 *
 * @param key the key of the timer, as returned by flml_timer_register
 * @return non-zero if the flml timer key represents an error, or zero otherwise
 */
static inline int flml_timer_key_is_error(flml_timer_key key) {
    return key == FLML_TIMER_KEY_ERROR;
}

#endif /* !_FLML_TIMER_H_ */
