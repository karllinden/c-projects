/*
 * This file is part of flml.
 *
 * Copyright (C) 2018-2023 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _FLML_FLML_H_
#define _FLML_FLML_H_

#include <dima/dima.h>

#include <flml/error.h>

/**
 * Flag for flml_run telling the main loop to handle signals.
 *
 * There can only be one flml main loop handling signals in the same
 * process at any given
 * time. If flml detects an attempt to handle signals from multiple
 * instances at the same time, flml reports FLML_ERROR_SIGNAL.
 */
#define FLML_HANDLE_SIGNALS 0x01

/**
 * Opaque forward definition of the fast light main loop structure.
 */
struct flml;

/**
 * Create a new empty flml structure.
 *
 * To populate the returned flml structure, use flml_fd_new and/or
 * flml_timer_register. After populating, the main loop can be run with
 * flml_run. When the structure is no longer needed, it must be destroyed with
 * flml_destroy.
 *
 * @param error a pointer to the error structure to fill in if an error
 *              occured
 * @return a pointer to the newly created flml structure on success or NULL on
 *         failure
 */
struct flml *flml_new(struct flml_error *error);

/**
 * Create a new empty flml structure with a custom memory allocator.
 *
 * This function behaves exactly as flml_new, except that the given DIMA is used
 * for allocating memory.
 *
 * The given DIMA must outlive the flml structure.
 */
struct flml *flml_new_with_dima(dima_t *dima, struct flml_error *error);

/**
 * Destroy an flml structure.
 *
 * If the passed flml pointer is NULL, this function does nothing.
 *
 * @param flml a pointer to the flml structure to destroy, or NULL
 */
void flml_destroy(struct flml *flml);

/**
 * Set the user supplied pointer.
 *
 * The pointer can be accessed with flml_get_ptr and is useful to pass user data
 * to the callbacks.
 *
 * @param flml a pointer to the flml structure
 * @param ptr the user pointer to set
 */
void flml_set_ptr(struct flml *flml, const void *ptr);

/**
 * Return the user pointer that was given to flml_set_ptr.
 *
 * If flml_set_ptr is not called prior to this function, then NULL is returned.
 *
 * @param flml a pointer to the flml structure
 * @return the pointer given to flml_set_ptr
 */
void *flml_get_ptr(const struct flml *flml);

/**
 * Return the last error that occurred in the given flml instance.
 *
 * If no error has occurred in the given instance, then the type of the returned
 * error is FLML_ERROR_NONE. This function never returns non-NULL.
 *
 * @param flml a pointer to the flml structure
 * @return the last error
 */
const struct flml_error *flml_get_last_error(const struct flml *flml);

/**
 * Run the fast light main loop.
 *
 * The main loop runs until flml_quit is called on the flml instance, or an
 * error occurs. This means that if there are no file descriptors, timers or
 * signals registered in the flml instance, this function will block
 * indefinitely.
 *
 * The main loop can behave in different ways depending on the flags given in
 * the flags parameter. Currently the only flag is FLML_HANDLE_SIGNALS.
 *
 * If FLML_HANDLE_SIGNALS is given in flags, then the main loop that is run
 * using that call to this function will handle signals in the following way:
 *  + Signal callbacks registered with flml_signal_catch will be invoked when
 *    the signal is delivered.
 *  + The SIGPIPE signal will be ignored. The disposition of the signal will not
 *    be restored when this function returns.
 * Only one flml main loop may handle signals at any given time, or an error
 * occurs.
 *
 * @param flml a pointer to the flml structure
 * @param flags the flags to run this main loop with; ORed together
 * @return zero on success, or non-zero otherwise in which case the
 *         last error is filled in appropriately; see flml_get_last_error
 */
int flml_run(struct flml *flml, int flags);

/**
 * Quit from the main loop running in the flml instance.
 *
 * @param flml a pointer to the flml structure
 */
void flml_quit(struct flml *flml);

#endif /* !_FLML_FLML_H_ */
