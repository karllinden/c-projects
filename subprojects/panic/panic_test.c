/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check-simpler.h>

#include <panic.h>

void set_up(void) {
    /* empty */
}

void tear_down(void) {
    /* empty */
}

START_TEST(panic_aborts) {
    int foo = 13;
    panic("foo should have been 0 here, but was %d.", foo);
}
END_TEST

START_TEST(panic_check_does_not_abort_if_condition_is_true) {
    panic_check(1);
}
END_TEST

START_TEST(panic_check_aborts_if_condition_is_false) {
    panic_check(0);
}
END_TEST

void add_tests(void) {
    ADD_ABORT_TEST(panic_aborts);
    ADD_TEST(panic_check_does_not_abort_if_condition_is_true);
    ADD_ABORT_TEST(panic_check_aborts_if_condition_is_false);
}
